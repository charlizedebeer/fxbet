.PHONY: test build images doc run

REPO = registry.gitlab.com/innostream/fxbet

test: build
	go test -failfast ./pkg/...

build:
	go generate ./... | grep -v "snake_case"
	go build -o cmd/fxbet cmd/main.go

images:
	podman login registry.gitlab.com
	podman build -t fxbet .
	podman push fxbet $(REPO)

doc: test
	swag init --parseDependency --dir cmd --output cmd/docs
	redoc-cli bundle cmd/docs/swagger.json -o cmd/docs/api.html

run: build test doc
	./cmd/fxbet -dev