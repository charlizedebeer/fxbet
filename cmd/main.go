package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"stream/fxbet/pkg/agreements"
	"stream/fxbet/pkg/email/mailjet"
	"stream/fxbet/pkg/games"
	"stream/fxbet/pkg/iam"
	"stream/fxbet/pkg/server"
	"stream/fxbet/pkg/server/http"
	"stream/fxbet/spatial/events/storage/file"
	"time"

	"github.com/gorilla/mux"
)

func listenForCtrlCAndCleanUp(cleanUp func()) {
	ctrlC := make(chan os.Signal, 1)
	signal.Notify(ctrlC, os.Interrupt)
	go func() {
		<-ctrlC
		cleanUp()
		os.Exit(0)
	}()
}

func startIAMService() (iam.Service, error) {
	storage, err := file.NewEventStorage("iam")
	if err != nil {
		return iam.Service{}, err
	}
	service := iam.New(storage)
	go service.Run()

	return service, nil
}

func startGamesService() (games.Service, error) {
	storage, err := file.NewEventStorage("games")
	if err != nil {
		return games.Service{}, err
	}
	service := games.New(storage)
	go service.Run()

	return service, nil
}

func startAgreementsService() (agreements.Service, error) {
	storage, err := file.NewEventStorage("agreements")
	if err != nil {
		return agreements.Service{}, err
	}
	service := agreements.New(storage)
	go service.Run()
	return service, nil
}

func startFXBetService() (server.Service, error) {
	storage, err := file.NewEventStorage("fxbet")
	if err != nil {
		log.Println(err)
	}
	service := server.New(storage)
	go service.Run()
	return service, nil
}

func main() {
	runInDeveloperMode := flag.Bool("dev", false, "Run server in local developer mode.")
	flag.Parse()

	iamService, err := startIAMService()
	if err != nil {
		log.Fatal(err)
	}
	iamService.AuthJwtSecret = "my_secret_key"
	iamService.LoginExpiresIn = 7 * 24 * time.Hour

	gameService, err := startGamesService()
	if err != nil {
		log.Fatal(err)
	}

	agreementsService, err := startAgreementsService()
	if err != nil {
		log.Fatal(err)
	}

	fxbetService, err := startFXBetService()
	if err != nil {
		log.Fatal(err)
	}
	fxbetService.IAM = iamService
	fxbetService.Games = gameService
	fxbetService.Agreements = agreementsService
	fxbetService.EmailSender = mailjet.Service{
		PublicAPIKey:  "fac3ed145319d5fbc5411638afa00dfd",
		PrivateAPIKey: "1606d6c9dc56be7bb2ab532c04dd724d",
		BearerToken:   "4a8fc5d9092d48e5b10fd7483bc60a73",
	}
	fxbetService.URLGenerator = func(p string) string {
		return fmt.Sprintf("http://localhost:1234/sign-up/verify/%s", p)
	}

	router := mux.NewRouter()
	server := http.Server{
		FXBet:        fxbetService,
		Router:       router,
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
		IdleTimeout:  120 * time.Second,
	}
	server.BindRoutes("/api", router)

	listenForCtrlCAndCleanUp(func() {
		fxbetService.Shutdown()
		iamService.Shutdown()
		agreementsService.Shutdown()
	})

	if *runInDeveloperMode {
		log.Printf("Running development server on port: %v", "8090")
		log.Fatal(server.ListenAndServe(":8090"))
		return
	}

	sslServer := http.SSLServer{
		Server:       server,
		CertsPath:    "/stream/certs",
		SupportEmail: "engineering@fxbet.co",
		HostNames:    []string{"fxbet.co", "www.fxbet.co"},
	}
	log.Printf("Running production server on ports 80 and 443")
	log.Fatal(sslServer.ListenAndServe())
}
