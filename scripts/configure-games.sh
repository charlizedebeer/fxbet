#!/bin/bash

# Specify a valid auth token for an operator here
AUTH_TOKEN="<SPECIFY_OPERATOR_AUTH_TOKEN_HERE>"

# This script POSTs to the FXBet Admin UI to configure a bunch of short and long games
# for the FXBet closed beta. It is NOT idempotent, i.e. don't run it multiple times,
# it will create multiple games! Use the admin API to delete existing games, or for
# a more brutal approach, just delete the Bookmaker binlog before restarting and running.

# ===== RISE / FALL MARKET GAMES ===========================
# CRYPTO
## Bitcoin Bonanza (short)
### BTC/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"BTC/EUR","tags":["crypto","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Bitcoin Bonanza", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/btc-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
## BTC/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"BTC/USD","tags":["crypto","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Bitcoin Bonanza", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/btc-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Bitcoin (long)
### BTC/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"BTC/EUR","tags":["crypto","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Bitcoin", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/btc-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
## BTC/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"BTC/USD","tags":["crypto","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Bitcoin", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/btc-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Up in the Ether (short)
### ETH/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"ETH/EUR","tags":["crypto","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Up in the Ether", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eth-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### ETH/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"ETH/USD","tags":["crypto","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Up in the Ether", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eth-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Ethereum (long)
### ETH/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"ETH/EUR","tags":["crypto","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Ethereum", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eth-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### ETH/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"ETH/USD","tags":["crypto","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Ethereum", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eth-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

# FOREX
## Yen Zen (short)
### JPY/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/EUR","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Yen Zen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/USD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Yen Zen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/GBP
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/GBP","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Yen Zen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/AUD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/AUD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Yen Zen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Japanese Yen (long)
### JPY/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/EUR","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Japanese Yen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/USD","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Japanese Yen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/GBP
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/GBP","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Japanese Yen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### JPY/AUD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"JPY/AUD","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Japanese Yen", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/jpy-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Dollar Scholar (short)
### USD/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"USD/EUR","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Dollar Scholar", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/usd-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## US Dollar (long)
### USD/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"USD/EUR","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"US Dollar", "description":null,"logoURL":null}}' http://localhost:8090/api/admin/game/create

## Euro Winner (short)
### EUR/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"EUR/USD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Euro Winner", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eur-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Euro (long)
### EUR/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"EUR/USD","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Euro", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/eur-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Pound Hound (short)
### GBP/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"GBP/USD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Pound Hound", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/gbp-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### GBP/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"GBP/EUR","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Pound Hound", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/gbp-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Pound sterling (long)
## TODO: These have no logo URL on purpose!
### GBP/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"GBP/USD","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Pound sterling", "description":null,"logoURL":null}}' http://localhost:8090/api/admin/game/create
### GBP/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"GBP/EUR","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Pound sterling", "description":null,"logoURL":null}}' http://localhost:8090/api/admin/game/create
### GBP/JPY
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"GBP/JPY","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Pound sterling", "description":null,"logoURL":null}}' http://localhost:8090/api/admin/game/create

## Chinese Yuan (long)
### CNY/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"CNY/EUR","tags":["forex","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Chinese Yuan", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/cny-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Hong Kong (short)
### HKD/EUR
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"HKD/EUR","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Hong Kong Kicks", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/hkd-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### HKD/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"HKD/USD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Hong Kong Kicks", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/hkd-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### HKD/GBP
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"HKD/GBP","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Hong Kong Kicks", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/hkd-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create
### HKD/AUD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"HKD/AUD","tags":["forex","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Hong Kong Kicks", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/hkd-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

# METALS
## Gold Digger (short)
### XAU/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"XAU/USD","tags":["metals","short"],"market":"rise","wagerTerms":[["tick", 5],["tick",10],["tick",20],["min",1]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[20, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Gold Digger", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/xau-fast/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Gold (long)
### XAU/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"XAU/USD","tags":["metals","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Gold", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/xau-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Silver (long)
### XAG/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"XAG/USD","tags":["metals","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Silver", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/xag-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create

## Platinum (long)
### XPT/USD
curl -H "Authorization: Bearer $AUTH_TOKEN" --request POST --data '{"feed":"XPT/USD","tags":["metals","long"],"market":"rise","wagerTerms":[["min", 1],["min",5]],"status":1,"minimumStake":[1, "EUR"],"maximumStake":[50, "EUR"],"minimumOdds":1.85,"maximumOdds":1.85,"oddsTolerance":0.01,"branding":{"title":"Platinum", "description":null,"logoURL":"https://fxbet-assets.fra1.cdn.digitaloceanspaces.com/games/xpt-slow/logo-med.jpg"}}' http://localhost:8090/api/admin/game/create


## Agreements
### Terms and conditions
curl -H "Authorization: Bearer $AUTH_TOKEN" -H "Content-Type: application/json" --request POST --data '{"title":"Terms and Conditions","body":"TODO add terms and conditions here", "required_for_user_action": "use.platform"}' http://localhost:8090/api/admin/terms
### Privacy
curl -H "Authorization: Bearer $AUTH_TOKEN" -H "Content-Type: application/json" --request POST --data '{"title":"Privacy Policy","body":"TODO add privacy policy here", "required_for_user_action": "use.platform"}' http://localhost:8090/api/admin/terms
