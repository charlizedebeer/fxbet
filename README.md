# FXBet

FXBet is an online gaming system where players bet on the outcome of short-term market movements.
This repository contains the Front-end (React) and back-end (Go) for the entire platform.

## Dependancies
- go
- flatbuffers
- swaggo
- redoc-cli

For `go` and `flatbuffers` best to use distro package management.

### Install swaggo

`go get -u github.com/swaggo/swag/cmd/swag`

### Install redoc-cli

`npm install -g redoc-cli`