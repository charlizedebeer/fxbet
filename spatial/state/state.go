// Package state provides types and utilities to build event-sourced services (with local, computed state)
// in a standard way.
package state

// ServiceState specifies the methods that are required on service state in order
// to perform event-sourcing in a standard manner. This allows us to provide standard
// utilities, such as to rehydrate state from an event store, etc.
type ServiceState interface {
	// Apply the given raw event (msgType, payload) to state. This is intended to be a synchronous
	// and 'pure' function - no I/O should be performed, and the same event applied to the same
	// state must always result in the same mutated state.
	// Note: The event payload is in raw, stored form (as bytes), which facilitates
	// rapidly hydrating state from persisted events - e.g. at service starup, as well as
	// enforcing consistency, such that the even being applied at run-time is in the same
	// form as that being applied later, e.g. when hydrating state.
	// Finally, it facilitates performance, by not forcing a full 'parse' or 'unpack', instead
	// allowing the implementation to read what it needs in order to update state.
	// Specifically, if flatbuffers are used, no intermediate allocations occur.
	ApplyEvent(evtType uint16, evtPayload []byte)
}
