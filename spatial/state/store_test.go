package state

import (
	"os"
	"stream/fxbet/spatial/events/storage"
	"stream/fxbet/spatial/events/storage/file"
	"stream/fxbet/spatial/events/storage/memory"
	"strings"
	"testing"

	flatbuffers "github.com/google/flatbuffers/go"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

type TestState struct {
	byID map[string]string
}

// InitState produces a new 'blank' state.
func InitState() *TestState {
	log.SetLevel(log.TraceLevel)
	return &TestState{
		byID: make(map[string]string),
	}
}

// ApplyEvent updates state given an event. The payload is already parsed as the correct type
// of flatbuffer, as per `decodeStoredEvent`.
func (s *TestState) ApplyEvent(msgType uint16, payload []byte) {
	cmd := strings.Split(string(payload), ":")
	switch cmd[0] {
	case "set":
		key := cmd[1]
		val := cmd[2]
		s.byID[key] = val
	}
}

func TestHydrate(t *testing.T) {
	state := InitState()
	events := memory.NewEventStorage("test")

	storei, err := NewStore(state, events)
	if err != nil {
		t.Fatal(err)
	}
	// Cast to internal struct for testing purposes
	store := storei.(*store)
	defer store.Close()

	// Write 2 stored events directly to file (not involving the store)
	evtData := [][][]byte{
		{[]byte("headers"), []byte("set:hello:world")},
		{[]byte("headers"), []byte("set:more:data")},
	}

	const msgType = uint16(1)
	b := flatbuffers.NewBuilder(100000)
	for _, ed := range evtData {

		// Create StoredEvent
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[0], // Would typically come from flatbuffers
			Payload: ed[1], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		storedEvt := storage.GetRootAsStoredEvent(b.FinishedBytes(), 0)
		err := events.Write(uint32(len(b.FinishedBytes())), storedEvt)
		if err != nil {
			t.Fatal(err)
		}
	}

	assert.Equal(t, int64(0), store.lastReadPos)
	assert.Equal(t, int64(0), store.lastWrotePos)

	err = store.Hydrate()
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "world", state.byID["hello"])
	assert.Equal(t, "data", state.byID["more"])

	assert.Equal(t, int64(188), store.lastReadPos)
	assert.Equal(t, int64(0), store.lastWrotePos) // Because we didn't dispatch anything through this store yet

	// Hydrate is idompotent, should be able to call it again
	err = store.Hydrate()
	if err != nil {
		t.Fatal(err)
	}

	// Write another event directly to the file
	se := storage.StoredEventT{
		MsgType: msgType,
		Headers: []byte("more headers"),
		Payload: []byte("set:foo:bar"),
	}
	b.Finish(se.Pack(b))
	storedEvt := storage.GetRootAsStoredEvent(b.FinishedBytes(), 0)
	err = events.Write(uint32(len(b.FinishedBytes())), storedEvt)
	if err != nil {
		t.Fatal(err)
	}

	err = store.Hydrate()
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "world", state.byID["hello"])
	assert.Equal(t, "data", state.byID["more"])
	assert.Equal(t, "bar", state.byID["foo"])
	assert.Equal(t, int64(364), store.lastReadPos)
	assert.Equal(t, int64(0), store.lastWrotePos) // Because we didn't dispatch anything through this store yet

	// Now dispatch an event through the store
	err = store.Dispatch(msgType, []byte("headers"), []byte("set:store:works"))
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "world", state.byID["hello"])
	assert.Equal(t, "data", state.byID["more"])
	assert.Equal(t, "bar", state.byID["foo"])
	assert.Equal(t, "works", state.byID["store"])
	assert.Equal(t, int64(364), store.lastReadPos)
	assert.Equal(t, int64(432), store.lastWrotePos) // Because we didn't dispatch anything through this store yet
}

// Two stores attached to a shared binlog file, with alternate Dispatch() and Hydrate(),
// as what is expected to happen in "binary star" HA service pairs.
func TestTwoStoresHAServicePair(t *testing.T) {
	// Store 1
	state1 := InitState()
	events1, err := file.NewEventStorage("store-test/ha-scenario")
	if err != nil {
		t.Fatal(err)
	}
	store1i, err := NewStore(state1, events1)
	// Cast to internal struct for testing purposes
	store1 := store1i.(*store)
	defer store1.Close()
	defer os.Remove(events1.WriteTo.Name()) // Clean up file for test purposes

	// Store 2
	state2 := InitState()
	events2, err := file.NewEventStorage("store-test/ha-scenario")
	if err != nil {
		t.Fatal(err)
	}
	store2i, err := NewStore(state2, events2)
	// Cast to internal struct for testing purposes
	store2 := store2i.(*store)
	defer store2.Close()
	defer os.Remove(events2.WriteTo.Name()) // Clean up file for test purposes

	// Hydrate both, check both states are empty
	err = store1.Hydrate()
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 0, len(state1.byID))
	assert.Equal(t, int64(0), store1.lastReadPos)
	assert.Equal(t, int64(0), store1.lastWrotePos)

	err = store2.Hydrate()
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 0, len(state2.byID))
	assert.Equal(t, int64(0), store2.lastReadPos)
	assert.Equal(t, int64(0), store2.lastWrotePos)

	assert.Equal(t, state1, state2)

	// Dispatch events in store 1, Hydrate store 2, check state (of both)
	const msgType = uint16(1)
	err = store1.Dispatch(msgType, []byte("headers"), []byte("set:dispatch1:store1"))
	if err != nil {
		t.Fatal(err)
	}

	// store 1 now has some state
	assert.Equal(t, "store1", state1.byID["dispatch1"])
	assert.Equal(t, int64(0), store1.lastReadPos)
	assert.Equal(t, int64(76), store1.lastWrotePos)

	// store 2 is still empty.
	assert.Equal(t, 0, len(state2.byID))

	// hydrate store 2
	err = store2.Hydrate()
	if err != nil {
		t.Fatal(err)
	}

	// store 2 should now have state
	assert.Equal(t, "store1", state2.byID["dispatch1"])
	assert.Equal(t, int64(76), store2.lastReadPos)
	assert.Equal(t, int64(0), store2.lastWrotePos)

	// and both states should now be equal
	assert.Equal(t, state1, state2)

	// Dispatch events in store 2, Hydrate store 1, check state (of both)
	err = store2.Dispatch(msgType, []byte("headers"), []byte("set:dispatch2:store2"))
	if err != nil {
		t.Fatal(err)
	}

	// store 2 is updated with event
	assert.Equal(t, "store1", state2.byID["dispatch1"])
	assert.Equal(t, "store2", state2.byID["dispatch2"])
	assert.Equal(t, int64(76), store2.lastReadPos)
	assert.Equal(t, int64(152), store2.lastWrotePos)

	// hydrate store 1
	err = store1.Hydrate()
	if err != nil {
		t.Fatal(err)
	}

	// store 1 should now be updated with the state
	assert.Equal(t, "store1", state1.byID["dispatch1"])
	assert.Equal(t, "store2", state1.byID["dispatch2"])
	assert.Equal(t, int64(152), store1.lastReadPos)
	assert.Equal(t, int64(76), store1.lastWrotePos)

	// and both states should now be equal
	assert.Equal(t, state1, state2)
	// TODO: THink of basic scenarios that can corrupt state of the files?
	// interleaved writes - the writes go through, but file positions are wrong so states are all messed up.
	// concurrent writes in go routines - writes happily, but again state is totally wrong.
}
