package state

import (
	"encoding/binary"
	"errors"
	"math"
	"stream/fxbet/spatial/events/storage"
	"time"

	flatbuffers "github.com/google/flatbuffers/go"
	log "github.com/sirupsen/logrus"
	"golang.org/x/text/message"
)

// Delim separates the routing frames (to the left) and the data frames (to the right)
// in a ZeroMQ multipart message.
var Delim = []byte("")

// MsgTypeToBytes converts a "human readable" message type (short integer) to the on-wire
// bytes representation.
func MsgTypeToBytes(msgType uint16) []byte {
	msgTypeBytes := make([]byte, 2)
	binary.LittleEndian.PutUint16(msgTypeBytes, msgType)
	return msgTypeBytes
}

// Packable describes any flatbuffers type generated with the object API. Flatbuffers
// are often used to represent events, for their compactness, type system, and speed.
type Packable interface {
	Pack(builder *flatbuffers.Builder) flatbuffers.UOffsetT
}

// Store represents the behaviour of an event-sourced, predictable container of state.
// Typically, a Store will contain:
// 1. A `ServiceState` which represents 'current' state, as an aggregate of dispatched events.
// 2. A `EventStorage` which durably and immutably stores dispatched events
// 3. A ZeroMQ `PUB` socket, which is used to broadcast dispatched events to observers (Optional)
//
// Important differences from typical Redux-like stores:
// 1. A Store manages resources (e.g. an underlying event storage, or network sockets) and
//    must be `.Close()`ed to allow underlying resources to complete their work.
// 2. Application state is mutable. The design goals of this store is very different than for that
//    powering an in-browser app; it was specifically designed to power single-threaded
//    servers (microservices).
// This store is not synchronized for concurrent access, it is meant to be used from a single thread
// at a time.
type Store interface {
	// GetState obtains a reference to service state. Unlike a Redux store, state is NOT immutable,
	// and hence this reference may continue to be independently used to read state, after events
	// have been dispatched.
	GetState() ServiceState

	// Dispatch FIRST instructs the `EventStorage` to store the event, AND THEN independently
	// applies the event to `ServiceState`, and sends the event on the `PUB` socket.
	// If an error was raised by either of the three steps, returns the error.
	// TODO: In order to power per-entity event iterators, i.e. "ledger statement", we need to
	// somehow associate an event with an entity, pre-storage. Do we pass this in? (probably not),
	// or do we have a separate concept of something that identifies the entity / ies affected by
	// an iterator?
	Dispatch(msgType uint16, headers, payload []byte) error

	// MustDispatch is a convenience wrapper around `Dispatch` which, if an error
	// occurs, fails with log.Fatalf (crashing the process).
	MustDispatch(msgType uint16, headers, payload []byte)

	// MustDispatchPackable is a convenience for `MustDispatch` with exactly the same semantics,
	// where the payload is represented as a packable struct. This avoids callers from
	// having to pack the payload themselves.
	MustDispatchPackable(msgType uint16, payload Packable)

	// Close any underlying resources, such as open file handles or database
	// connections. May only be called once, and only returns when it is safe
	// to dispose of the store and terminate the program, i.e. after any pending writes
	// of events have completed. Would typically be used as:
	// defer store.Close()
	Close() error

	// Hydrate forces the store to update State, from the last persisted events
	// that were read from the EventStorage, to the latest available events.
	// Idempotent: Updates state to be as up-to-date as possible given the
	// stored events.
	Hydrate() error

	// GetSubscribeEndpoint returns the endpoint URL of the ZeroMQ PUB socket over which dispatched
	// events are broadcast. A ZMQ SUB socket may be attached to this URL to observe future events.
	// Note: If such an endpoint is not available, i.e. there is no active PUB socket over whic
	// events are published, empty string may be returned.
	GetSubscribeEndpoint() string

	// TODO: Currently assumes that business logic don't need raw events, only aggregate state. For
	// some use-cases, i.e. "get ledger statement" this won't be true, in which case we can
	// expose iterators to read events:
	//   1. All events, in chronological order
	//   2. Events pertaining to a particular entity, in reverse-chrono order (i.e. ledger transactions)
}

// Represents a store. Create an instance using `NewStore`.
type store struct {
	// Aggregate state, as a reduction of events
	state ServiceState
	// Durable storage of events - written during Dispatch, read during Hydrate
	events storage.EventStorage
	// Bookkeeping for idempotent Hydrate()
	lastReadPos  int64
	lastWrotePos int64
	// Tracks how many times Hydrate() was called
	hydrateCount uint64
	// Used to print messages with localised formatting
	messagePrinter *message.Printer
}

// NewStore produces a new instance of a store, using the given service state, event storage,
// and ZMQ PUB socket.
// When Dispatch() is called, the event will first be stored using the given EventStorage.StoreEvent(),
// then the given state is updated via ServiceState.ApplyEvent(), and finally, if a PUB socket is provided,
// broadcase to observers by being published over the socket.
// The PUB socket may be nil, in which case dispatched events will not be
// externally observable.
func NewStore(state ServiceState, events storage.EventStorage) (Store, error) {
	if state == nil {
		return nil, errors.New("no ServiceState provided")
	}
	if events == nil {
		return nil, errors.New("no EventStorage provided")
	}
	return &store{
		state:          state,
		events:         events,
		messagePrinter: message.NewPrinter(message.MatchLanguage("en")),
	}, nil
}

func (s *store) GetState() ServiceState {
	return s.state
}

func (s *store) Close() error {
	return s.events.Close()
}

func (s *store) GetSubscribeEndpoint() string {
	return ""
}

func (s *store) Dispatch(msgType uint16, headers, payload []byte) error {
	b := flatbuffers.NewBuilder(1024) // TODO: Make initial size configurable? How does engineer know what to set to??
	//bytes := messaging.Build(&storage.StoredEventT{
	e := &storage.StoredEventT{
		MsgType: msgType,
		Headers: headers,
		Payload: payload,
	}
	b.Finish(e.Pack(b))
	bytes := b.FinishedBytes()
	// TODO: Might make sense to revise EventStorage API to just take []byte
	// instead of StoredEvent, since it doesn't do anything with the struct.
	se := storage.GetRootAsStoredEvent(bytes, 0)
	err := s.events.Write(uint32(len(bytes)), se)
	if err != nil {
		return err
	}
	// Update writePos from event storage
	s.lastWrotePos = s.events.GetWritePos()
	s.state.ApplyEvent(msgType, payload)
	return nil
}

func (s *store) MustDispatch(msgType uint16, headers, payload []byte) {
	err := s.Dispatch(msgType, headers, payload)
	if err != nil {
		log.Fatal(err)
	}
}

func (s *store) MustDispatchPackable(msgType uint16, payload Packable) {
	b := flatbuffers.NewBuilder(128)
	b.Finish(payload.Pack(b))
	s.MustDispatch(msgType, nil, b.FinishedBytes())
}

func (s *store) Hydrate() error {
	s.hydrateCount++
	start := time.Now()
	// Determine where to read from, i.e. max(readPos, writePos)
	max := int64(math.Max(float64(s.lastReadPos), float64(s.lastWrotePos)))
	iterator, err := s.events.ReadFrom(max)
	if err != nil {
		return err
	}

	// Log - debug on first time, trace thereafter
	if s.hydrateCount == 1 {
		log.Debugf("Hydrating events from position %d: %s", max, s.events.String())
	} else if log.GetLevel() == log.TraceLevel {
		log.Debugf("Hydrating events from position %d: %s", max, s.events.String())
	}

	// Get all events from this pos, apply to state
	var evtsRead uint64
	for iterator.Next() {
		storedEvent, err := iterator.Value()
		if err != nil {
			return err
		}

		s.state.ApplyEvent(storedEvent.MsgType(), storedEvent.PayloadBytes())
		evtsRead++
	}

	// Update readPos from iterator - note iterator gives us read pos from start position
	// so we need to add it the start position.
	s.lastReadPos = max + iterator.GetReadPos()

	elapsed := time.Since(start)

	// Log - debug on first time, trace thereafter
	if s.hydrateCount == 1 {
		msg := s.messagePrinter.Sprintf("Hydrated %d events from %d bytes in %f sec: %s", evtsRead, s.lastReadPos, elapsed.Seconds(), s.events.String())
		log.Debugf(msg)
	} else if log.GetLevel() == log.TraceLevel {
		msg := s.messagePrinter.Sprintf("Hydrated %d events from %d bytes in %f sec: %s", evtsRead, s.lastReadPos, elapsed.Seconds(), s.events.String())
		log.Tracef(msg)
	}

	return nil
}
