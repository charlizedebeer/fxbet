package atomic

// Atomic provides fields and methods to run atomic operations
type Atomic struct {
	atomically chan func()
	done       chan struct{}
	shutdown   chan struct{}
}

// New returns a atomic queue ready for operations.
func New() Atomic {
	return Atomic{
		atomically: make(chan func()),
		done:       make(chan struct{}),
		shutdown:   make(chan struct{}),
	}
}

// Atomically run operations and wait for a return
func (a Atomic) Atomically(f func()) {
	a.atomically <- f
	<-a.done
}

// Run starts the main loop to run atomic operations
func (a Atomic) Run() {
	for {
		select {
		case f := <-a.atomically:
			f()
			a.done <- struct{}{}
		case <-a.shutdown:
			return
		}
	}
}

// Shutdown stops the main loop
func (a Atomic) Shutdown() {
	a.shutdown <- struct{}{}
}
