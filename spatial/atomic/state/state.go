package state

import (
	"fmt"
	"stream/fxbet/spatial/atomic"
	"stream/fxbet/spatial/events/storage"
	"stream/fxbet/spatial/state"

	flatbuffers "github.com/google/flatbuffers/go"
)

// Atomic provides methods to atomically update state using event sourcing.
type Atomic struct {
	atomic.Atomic
	store state.Store
}

// New initializes and returns the event sourced state service
func New(s state.ServiceState, evtStore storage.EventStorage) Atomic {
	store, _ := state.NewStore(s, evtStore)
	return Atomic{
		Atomic: atomic.New(),
		store:  store,
	}
}

// Run first hydrates the store and then starts the atomic loop
func (s Atomic) Run() {
	err := s.store.Hydrate()
	if err != nil {
		panic(fmt.Sprintf("could not hydrate store, %v", err))
	}

	s.Atomic.Run()
}

// Dispatch a state changing event atomically.
func (s Atomic) Dispatch(evtType uint16, payload state.Packable) error {
	var err error
	s.Atomically(func() {
		b := flatbuffers.NewBuilder(128)
		b.Finish(payload.Pack(b))
		err = s.store.Dispatch(evtType, nil, b.FinishedBytes())
	})
	return err
}
