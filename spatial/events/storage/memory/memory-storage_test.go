// Unit tests for the file-based event store ("binlog")
package memory

import (
	"encoding/binary"
	"stream/fxbet/spatial/events/storage"
	"testing"

	flatbuffers "github.com/google/flatbuffers/go"
	"github.com/stretchr/testify/assert"
)

// MsgTypeToBytes converts a "human readable" message type (short integer) to the on-wire
// bytes representation.
func MsgTypeToBytes(msgType uint16) []byte {
	msgTypeBytes := make([]byte, 2)
	binary.LittleEndian.PutUint16(msgTypeBytes, msgType)
	return msgTypeBytes
}

// Simple unit test to prove that it fundamentally works
func TestReadWriteSimple(t *testing.T) {
	// Create event store
	// Note: It takes file name as input, not file handle, in order to
	// be able to control file handles, automatically re-open on failure, etc.
	st := NewEventStorage("test")
	// Important to always close the store, which properly flushes and releases
	defer st.Close()

	var msgType1 uint16 = 41
	var msgType2 uint16 = 42

	// Write 2 stored events
	evtData := [][][]byte{
		{MsgTypeToBytes(msgType1), []byte("hello"), []byte("world")},
		{MsgTypeToBytes(msgType2), []byte("more"), []byte("data")},
	}

	for _, ed := range evtData {
		b := flatbuffers.NewBuilder(100000)

		// Create StoredEvent
		msgType := binary.LittleEndian.Uint16(ed[0])
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[1], // Would typically come from flatbuffers
			Payload: ed[2], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		storedEvt := storage.GetRootAsStoredEvent(b.FinishedBytes(), 0)
		err := st.Write(uint32(len(b.FinishedBytes())), storedEvt)
		if err != nil {
			t.Fatal(err)
		}

	}

	// Read them all!
	allEvents, err := st.ReadAll()
	if err != nil {
		t.Fatal(err)
	}

	i := 0
	for allEvents.Next() {
		evt, err := allEvents.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[i]
		i++

		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
	}
	assert.Equalf(t, len(evtData), i, "Expected %d events to be read from %s", len(evtData), st.String())
}

func TestReadFromSimple(t *testing.T) {
	// Create event store
	// Note: It takes file name as input, not file handle, in order to
	// be able to control file handles, automatically re-open on failure, etc.
	st := NewEventStorage("test")
	// Important to always close the store, which properly flushes and releases
	defer st.Close()

	var msgType1 uint16 = 41
	var msgType2 uint16 = 42

	// Write 2 stored events
	evtData := [][][]byte{
		{MsgTypeToBytes(msgType1), []byte("hello"), []byte("world")},
		{MsgTypeToBytes(msgType2), []byte("more"), []byte("data")},
	}

	var offset int64 = 0
	for idx, ed := range evtData {
		b := flatbuffers.NewBuilder(100000)

		// Create StoredEvent
		msgType := binary.LittleEndian.Uint16(ed[0])
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[1], // Would typically come from flatbuffers
			Payload: ed[2], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		finishedBytes := b.FinishedBytes()
		storedEvt := storage.GetRootAsStoredEvent(finishedBytes, 0)
		err := st.Write(uint32(len(finishedBytes)), storedEvt)
		if err != nil {
			t.Fatal(err)
		}

		if idx == 0 {
			offset = int64(len(finishedBytes)) + 4
		}
	}

	// Read them all!
	allEvents, err := st.ReadFrom(0)
	if err != nil {
		t.Fatal(err)
	}

	i := 0
	for allEvents.Next() {
		evt, err := allEvents.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[i]
		i++

		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
	}
	assert.Equalf(t, len(evtData), i, "Expected %d events to be read from %s", len(evtData), st.String())

	i = 0
	lastEventIterator, err := st.ReadFrom(offset)
	if err != nil {
		t.Fatal(err)
	}

	for lastEventIterator.Next() {
		evt, err := lastEventIterator.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[1]
		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
		i++
	}
	assert.Equalf(t, 1, i, "Expected 1 event to be read from (pos) %s", st.String())
}
