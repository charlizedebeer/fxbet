// Package file contains an implementation of `EventStorage` that appends to, and reads
// from, a local file, or "binlog". Because "size prefixed flatbuffers" are not currently
// available in Go, the file format is not simply a sequence of flatbuffers. Instead, each
// entry is prefixed with its size, as a uint32. The format is thus:
//
// [size][StoredEvent][size][StoredEvent][size][StoredEvent][size][StoredEvent]...
//
// Note: For reliability, currently performs a .Flush() after every entry is written,
// which is quite slow, but provides certain guarantees that are desirable for most
// event-sourced service implementations that use this event store.
package memory

import (
	"encoding/binary"
	"errors"
	"strconv"
	"stream/fxbet/spatial/events/storage"
)

// EventStorage is an implementation that maintains an in-memory handle to the binlog.
type EventStorage struct {
	writeTo []byte
	name    string
}

// NewEventStorage creates a new in-memory `EventStorage`
// TODO: Remove Error result (can never fail) - will make test code smaller etc
func NewEventStorage(serviceName string) *EventStorage {
	return &EventStorage{
		writeTo: make([]byte, 0),
		name:    serviceName,
	}
}

// String representation for humans, useful in log files etc.
func (s *EventStorage) String() string {
	displayName := s.name
	if displayName == "" {
		displayName = "(unnamed)"
	}
	return "In-memory event log: " + displayName
}

// Store atomically and synchronously adds an entry to the binlog.
func (s *EventStorage) Write(size uint32, e *storage.StoredEvent) error {
	// Write size bytes
	entry := make([]byte, 4)
	binary.LittleEndian.PutUint32(entry, size)
	// Add flatbuffer bytes
	entry = append(entry, e.Table().Bytes...)
	s.writeTo = append(s.writeTo, entry...)

	return nil
}

func (s *EventStorage) ReadAll() (storage.EventIterator, error) {
	return s.ReadFrom(0)
}

func (s *EventStorage) ReadFrom(pos int64) (storage.EventIterator, error) {
	return &readAllIterator{
		readFrom: s.writeTo[pos:],
		srcName:  s.String(),
		startIdx: 0,
	}, nil
}

func (s *EventStorage) Close() error {
	return nil
}

func (s *EventStorage) GetWritePos() int64 {
	return int64(len(s.writeTo))
}

// Encapsulated implementation of `EventIterator`
type readAllIterator struct {
	readFrom []byte
	nextEvt  *storage.StoredEvent
	nextErr  error
	srcName  string
	count    int
	startIdx int
}

func (i *readAllIterator) Next() bool {
	if i.startIdx >= len(i.readFrom) {
		return false
	}

	// Read size
	szb := make([]byte, 4)
	idx := i.startIdx
	szb = i.readFrom[idx:(idx + len(szb))]

	// Could not read size, corrupted file??
	if len(szb) != 4 {
		i.nextErr = errors.New("Corrupted, read incomplete size of next entry")
		i.nextEvt = nil
		return true
	}

	// Read `size` bytes, decode as StoredEvent (flatbuffer)
	size := binary.LittleEndian.Uint32(szb)
	buf := make([]byte, size)
	buf = i.readFrom[idx+4 : idx+4+len(buf)]

	// Did we read what we expected to?
	if uint32(len(buf)) != size {
		i.nextErr = errors.New(i.srcName + " is corrupted, event truncated unexpectedly (expected " +
			strconv.Itoa(int(size)) + "b, got " + strconv.Itoa(len(buf)) +
			"b, count=" + strconv.Itoa(i.count) + ")")
		i.nextEvt = nil
		return true
	}

	// Store next, parsed value
	i.nextEvt = storage.GetRootAsStoredEvent(buf, 0)
	i.nextErr = nil

	// Increment global counter
	i.count++
	i.startIdx = idx + 4 + len(buf)
	return true
}

func (i *readAllIterator) GetReadPos() int64 {
	return int64(i.startIdx)
}

func (i *readAllIterator) Value() (*storage.StoredEvent, error) {
	if i.nextEvt != nil {
		return i.nextEvt, nil
	}
	if i.nextErr != nil {
		return nil, i.nextErr
	}
	// Either Next() was never called, or code bug in Next()
	panic("EventStorage iterator Next() must be called before Value()")
}
