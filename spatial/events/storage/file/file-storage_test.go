// Unit tests for the file-based event store ("binlog")
package file

import (
	"crypto/md5"
	"encoding/binary"
	"math/rand"
	"os"
	"stream/fxbet/spatial/events/storage"
	"testing"
	"time"

	flatbuffers "github.com/google/flatbuffers/go"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

// MsgTypeToBytes converts a "human readable" message type (short integer) to the on-wire
// bytes representation.
func MsgTypeToBytes(msgType uint16) []byte {
	msgTypeBytes := make([]byte, 2)
	binary.LittleEndian.PutUint16(msgTypeBytes, msgType)
	return msgTypeBytes
}

func TestMain(m *testing.M) {
	log.SetLevel(log.TraceLevel)
	os.Exit(m.Run())
}

// Simple unit test to prove that it fundamentally works
func TestReadWriteSimple(t *testing.T) {
	// Create event store
	// Note: It takes file name as input, not file handle, in order to
	// be able to control file handles, automatically re-open on failure, etc.
	st, err := NewEventStorage("events-test/plain", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	// Important to always close the store, which properly flushes and releases
	defer st.Close()
	defer os.Remove(st.WriteTo.Name()) // Clean up file for test purposes

	var msgType1 uint16 = 41
	var msgType2 uint16 = 42

	// Write 2 stored events
	evtData := [][][]byte{
		{MsgTypeToBytes(msgType1), []byte("hello"), []byte("world")},
		{MsgTypeToBytes(msgType2), []byte("more"), []byte("data")},
	}

	for _, ed := range evtData {
		b := flatbuffers.NewBuilder(100000)

		// Create StoredEvent
		msgType := binary.LittleEndian.Uint16(ed[0])
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[1], // Would typically come from flatbuffers
			Payload: ed[2], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		storedEvt := storage.GetRootAsStoredEvent(b.FinishedBytes(), 0)
		err := st.Write(uint32(len(b.FinishedBytes())), storedEvt)
		if err != nil {
			t.Fatal(err)
		}

	}

	// Read them all!
	allEvents, err := st.ReadAll()
	if err != nil {
		t.Fatal(err)
	}

	i := 0
	for allEvents.Next() {
		evt, err := allEvents.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[i]
		i++

		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
	}
	assert.Equalf(t, len(evtData), i, "Expected %d events to be read from %s", len(evtData), st.String())
}

// Tests that we can perform a streaming read while the file is being written to, in lockstep (i.e. file
// never appears corrupted)
func TestReadWriteInterleaved(t *testing.T) {
	t.SkipNow()
	// Create event store
	// Note: It takes file name as input, not file handle, in order to
	// be able to control file handles, automatically re-open on failure, etc.
	st1, err := NewEventStorage("events-test/interleaved", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	st2, err := NewEventStorage("events-test/interleaved", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	// Important to always close the store, which properly flushes and releases
	defer st1.Close()
	defer st2.Close()
	defer os.Remove(st1.WriteTo.Name()) // Clean up file for test purposes

	var msgType1 uint16 = 41

	// Write 2 stored events
	evtData := [][][]byte{
		{MsgTypeToBytes(msgType1), []byte("hello"), []byte("world")},
		{MsgTypeToBytes(msgType1), []byte("more"), []byte("data")},
	}

	// Setup 'read' iterator from second store
	evts, err := st2.ReadAll()
	if err != nil {
		t.Fatal(err)
	}

	for _, ed := range evtData {
		b := flatbuffers.NewBuilder(100000)

		// Create StoredEvent
		msgType := binary.LittleEndian.Uint16(ed[0])
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[1], // Would typically come from flatbuffers
			Payload: ed[2], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		storedEvt := storage.GetRootAsStoredEvent(b.FinishedBytes(), 0)

		// Write via first store
		err := st1.Write(uint32(len(b.FinishedBytes())), storedEvt)
		if err != nil {
			t.Fatal(err)
		}

		// Read from second, already-connected iterator
		assert.True(t, evts.Next())
		evt, err := evts.Value()
		if err != nil {
			t.Fatal(err)
		}

		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, ed[1], evt.HeadersBytes())
		assert.Equal(t, ed[2], evt.PayloadBytes())
		log.Debugln("Read next event")

		// Check there are no more events at the moment (until we write more via other store)
		assert.False(t, evts.Next())
	}

	// Check there are no more events (all done)
	assert.False(t, evts.Next())
}

func TestReadWriteConcurrentLots(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test in short mode.")
	}

	// Note: As a non-short test, we also don't write to the usual TMPFS (which is backed by RAM)
	// Instead, writing to the normal, on-disk location in order to experience realistic I/O speed
	st1, err := NewEventStorage("events-test/concurrent")
	if err != nil {
		t.Fatal(err)
	}
	st2, err := NewEventStorage("events-test/concurrent")
	if err != nil {
		t.Fatal(err)
	}
	defer st1.Close()
	defer st2.Close()
	defer os.Remove(st1.WriteTo.Name()) // Clean up file for test purposes

	evtCount := 100000
	go WriteRandEvents(t, evtCount, st1)
	done := make(chan bool)
	go ReadRandEvents(t, evtCount, st2, done)
	isDone := <-done
	log.Infof("Concurrent test done: %v", isDone)
}

// To the given event store, write count events where the payload is a random 1K bytes,
// and the header is the md5sum
func WriteRandEvents(t *testing.T, count int, st *EventStorage) {
	log.Debugf("Writing %d event(s)", count)
	evts := make([]*storage.StoredEventT, count)
	// Prepare data for rapid writing later
	for i := 0; i < count; i++ {
		var msgType uint16 = 42
		payload := make([]byte, 1024)
		rand.Read(payload)
		headersA := md5.Sum(payload)
		headers := headersA[:]

		evts[i] = &storage.StoredEventT{
			MsgType: msgType,
			Headers: headers,
			Payload: payload,
		}
	}

	// Write to event store
	for _, evt := range evts {
		b := flatbuffers.NewBuilder(10000)
		b.Finish(evt.Pack(b))
		bytes := b.FinishedBytes()
		storedEvt := storage.GetRootAsStoredEvent(bytes, 0)
		err := st.Write(uint32(len(bytes)), storedEvt)
		if err != nil {
			t.Fatal(err)
		}
	}
	log.Debugf("Wrote %d event(s)", count)
}

// From the given event store, read count events where the payload is a random,
// and the header is the md5sum. Checks the sums, and that the expected number of events are read
func ReadRandEvents(t *testing.T, count int, st *EventStorage, done chan bool) {
	log.Debugf("Reading %d event(s)\n", count)
	readCount := 0
	evts, err := st.ReadAll()
	if err != nil {
		t.Fatal(err)
	}

	for readCount < count {
		if evts.Next() {
			evt, err := evts.Value()
			if err != nil {
				t.Fatal(err)
			}
			readCount++

			// Check that event is not corrupted
			assert.Equal(t, uint16(42), evt.MsgType())

			expectedSum := evt.HeadersBytes()
			sumA := md5.Sum(evt.PayloadBytes())
			sum := sumA[:]
			assert.Equal(t, expectedSum, sum)
		} else {
			log.Debugf("Waiting for more data: Have %d of expected %d\n", readCount, count)
			time.Sleep(1 * time.Millisecond)
		}

	}
	log.Debugf("Read %d event(s)\n", readCount)
	assert.Equal(t, count, readCount)
	done <- true
}

func TestReadFromSimple(t *testing.T) {
	// Create event store
	// Note: It takes file name as input, not file handle, in order to
	// be able to control file handles, automatically re-open on failure, etc.
	st, err := NewEventStorage("events-test/plain", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	// Important to always close the store, which properly flushes and releases
	defer st.Close()
	defer os.Remove(st.WriteTo.Name()) // Clean up file for test purposes

	var msgType1 uint16 = 41
	var msgType2 uint16 = 42

	// Write 2 stored events
	evtData := [][][]byte{
		{MsgTypeToBytes(msgType1), []byte("hello"), []byte("world")},
		{MsgTypeToBytes(msgType2), []byte("more"), []byte("data")},
	}

	var offset int64 = 0
	for idx, ed := range evtData {
		b := flatbuffers.NewBuilder(100000)

		// Create StoredEvent
		msgType := binary.LittleEndian.Uint16(ed[0])
		se := storage.StoredEventT{
			MsgType: msgType,
			Headers: ed[1], // Would typically come from flatbuffers
			Payload: ed[2], // Would typically come from flatbuffers
		}
		b.Finish(se.Pack(b))
		finishedBytes := b.FinishedBytes()
		if idx == 0 {
			offset = int64(len(finishedBytes)) + 4
		}
		storedEvt := storage.GetRootAsStoredEvent(finishedBytes, 0)
		err := st.Write(uint32(len(finishedBytes)), storedEvt)
		if err != nil {
			t.Fatal(err)
		}
	}

	allEvents, err := st.ReadFrom(0)
	if err != nil {
		t.Fatal(err)
	}

	i := 0
	for allEvents.Next() {
		evt, err := allEvents.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[i]
		i++

		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
	}
	assert.Equalf(t, len(evtData), i, "Expected %d events to be read from %s", len(evtData), st.String())

	i = 0
	lastEventIterator, err := st.ReadFrom(offset)
	if err != nil {
		t.Fatal(err)
	}

	for lastEventIterator.Next() {
		evt, err := lastEventIterator.Value()
		if err != nil {
			t.Fatal(err)
		}
		exp := evtData[1]
		msgType := binary.LittleEndian.Uint16(exp[0])
		// Compare equality
		assert.Equal(t, msgType, evt.MsgType())
		assert.Equal(t, exp[1], evt.HeadersBytes())
		assert.Equal(t, exp[2], evt.PayloadBytes())
		i++
	}
	assert.Equalf(t, 1, i, "Expected 1 event to be read from (pos) %s", st.String())
}

func TestReadFrom(t *testing.T) {
	st, err := NewEventStorage("events-test/plain", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	defer st.Close()
	defer os.Remove(st.WriteTo.Name()) // Clean up file for test purposes

	count := 10
	log.Debugf("Writing %d event(s)", count)

	var offsets = make([]int64, count)
	// Prepare data for rapid writing later
	for i := 0; i < count; i++ {
		var msgType uint16 = 42
		payload := make([]byte, 1024)
		rand.Read(payload)
		headersA := md5.Sum(payload)
		headers := headersA[:]

		evt := &storage.StoredEventT{
			MsgType: msgType,
			Headers: headers,
			Payload: payload,
		}

		builder := flatbuffers.NewBuilder(10000)
		builder.Finish(evt.Pack(builder))
		bytes := builder.FinishedBytes()
		storedEvt := storage.GetRootAsStoredEvent(bytes, 0)
		err := st.Write(uint32(len(bytes)), storedEvt)
		if err != nil {
			t.Fatal(err)
		}

		evtOffset := int64(len(bytes) + 4)
		var prev int64 = 0
		if i != 0 {
			prev = offsets[i-1]
		}
		offsets[i] = evtOffset + prev // we add the previous offset to keep up with position in file.
	}
	log.Debugf("Wrote %d event(s)", count)

	// for each off set
	for idx, offset := range offsets {
		log.Debugf("Reading from offset %d", offset)
		iterator, err := st.ReadFrom(offset)
		if err != nil {
			t.Fatal(err)
		}

		i := 0
		for iterator.Next() {
			value, err := iterator.Value()
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, uint16(42), value.MsgType())
			expectedSum := value.HeadersBytes()
			sumA := md5.Sum(value.PayloadBytes())
			sum := sumA[:]
			assert.Equal(t, expectedSum, sum)
			i++
		}

		assert.Equal(t, count-(idx+1), i) // total events - idx + 1 gives us the expected evt count.
	}
}

func TestReadFromBadOffset(t *testing.T) {
	st, err := NewEventStorage("events-test/plain", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	defer st.Close()
	defer os.Remove(st.WriteTo.Name()) // Clean up file for test purposes

	var msgType uint16 = 42
	payload := make([]byte, 1024)
	rand.Read(payload)
	headersA := md5.Sum(payload)
	headers := headersA[:]

	evt := &storage.StoredEventT{
		MsgType: msgType,
		Headers: headers,
		Payload: payload,
	}

	builder := flatbuffers.NewBuilder(10000)
	builder.Finish(evt.Pack(builder))
	bytes := builder.FinishedBytes()
	storedEvt := storage.GetRootAsStoredEvent(bytes, 0)
	err = st.Write(uint32(len(bytes)), storedEvt)
	if err != nil {
		t.Fatal(err)
	}

	const offset int64 = 125
	log.Debugf("Reading from offset %d", offset)
	iterator, err := st.ReadFrom(offset)
	if err != nil {
		t.Fatal(err)
	}

	for iterator.Next() {
		value, err := iterator.Value()
		// we expect this to fail
		assert.NotNil(t, err)
		assert.Nil(t, value)
	}
}

func TestReadFromLargeOffset(t *testing.T) {
	st, err := NewEventStorage("events-test/plain", os.TempDir())
	if err != nil {
		t.Fatal(err)
	}
	defer st.Close()
	defer os.Remove(st.WriteTo.Name()) // Clean up file for test purposes

	var msgType uint16 = 42
	payload := make([]byte, 1024)
	rand.Read(payload)
	headersA := md5.Sum(payload)
	headers := headersA[:]

	evt := &storage.StoredEventT{
		MsgType: msgType,
		Headers: headers,
		Payload: payload,
	}

	builder := flatbuffers.NewBuilder(10000)
	builder.Finish(evt.Pack(builder))
	bytes := builder.FinishedBytes()
	storedEvt := storage.GetRootAsStoredEvent(bytes, 0)
	err = st.Write(uint32(len(bytes)), storedEvt)
	if err != nil {
		t.Fatal(err)
	}

	offset := int64(len(bytes)+4) * 2 // offset 2x bigger than file size
	log.Debugf("Reading from offset %d", offset)
	iterator, err := st.ReadFrom(offset)
	if err != nil {
		t.Fatal(err)
	}

	// as offset was larger than file we won't have a next.
	assert.False(t, iterator.Next())
}
