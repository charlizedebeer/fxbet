// Package file contains an implementation of `EventStorage` that appends to, and reads
// from, a local file, or "binlog". Because "size prefixed flatbuffers" are not currently
// available in Go, the file format is not simply a sequence of flatbuffers. Instead, each
// entry is prefixed with its size, as a uint32. The format is thus:
//
// [size][StoredEvent][size][StoredEvent][size][StoredEvent][size][StoredEvent]...
//
// Note: For reliability, currently performs a .Flush() after every entry is written,
// which is quite slow, but provides certain guarantees that are desirable for most
// event-sourced service implementations that use this event store.
package file

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"os"
	"stream/fxbet/spatial/events/storage"
	"strings"

	log "github.com/sirupsen/logrus"
)

// MaxSupportedEvtSize is the maximum possible size we'll even try to read or write.
const MaxSupportedEvtSize = 100_000_000

// MaxRecommendedEvtSize is the maximum size that events shoul typically adhere to (by convention).
const MaxRecommendedEvtSize = 4_096

// EventStorage is an implementation that maintains a single append-only file handle
// to the binlog. Every time `.ReadAll()` is called, an additional, read-only file
// handle is opened which drives the state of the iteration behaviour.
type EventStorage struct {
	// The handle to the binlog file
	// TODO: Rather expose file(s) ... split binlog in future... via Method() ?
	WriteTo *os.File
	// Read handle to the bin log file.
	ReadFromFile *os.File
	// If set to true, will be very durable (fsync on every write) but incredibly slow
	SyncEveryWrite bool
	// TODO: Track all open read file handles, such that we can close them all
	// on `.Close()`
	// If any events were written to this store using `Write`, this tracks the
	// last known byte position, i.e. the cursor in the file that will next be
	// written to. Note: If another independent EventStorage writes to this same
	// file, this will not be up to date!
	lastWritePos int64
}

// StorageDirForService determines which directory SHOULD be used
// to host the data (binlog) for the given service name. Note: It merely indicates
// the location, and does not e.g. create it if it does not already exist.
// Note that it accepts an optional second `baseDir` parameter, which may
// be used to override the default (~/.local/share).
func StorageDirForService(serviceName string, baseDir ...string) (string, error) {
	// baseDir override provided
	if baseDir != nil && len(baseDir) == 1 {
		storageDir := baseDir[0]
		if !strings.HasSuffix(storageDir, "/") {
			storageDir += "/"
		}
		return storageDir + serviceName, nil
	}
	// defaults
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", errors.New("could not open user home dir")
	}
	return homeDir + "/.local/share/" + serviceName, nil
}

// NewEventStorage creates a new `EventStorage` attached to the given file path.
// It immediately opens an append-only file handle to the primary storage file, and keeps
// it open until the event store is disposed of with `.Close()`.
// A base directory may be specified, otherwise a default location, e.g. ~/.local/share/
// is used. In both cases, service name is used as a subdirectory to avoid name collisions between
// different services.
func NewEventStorage(serviceName string, baseDir ...string) (*EventStorage, error) {
	// Where to store
	binlogDirectory, err := StorageDirForService(serviceName, baseDir...)
	if err != nil {
		return nil, err
	}
	log.WithField("dir", binlogDirectory).Debugln("Init binlog")

	// Create if doesn't already exist
	_, err = os.Stat(binlogDirectory)
	if os.IsNotExist(err) {
		err := os.MkdirAll(binlogDirectory, 0755)
		if err != nil {
			return nil, errors.New("unable to create directory")
		}
	}

	// Note: We open in O_APPEND mode to guarantee that multiple processes writing to the same file
	// don't overwrite one another's data, assuming the chunks being written are under the fs page size
	// (usually 4k on Linux) - see: https://pubs.opengroup.org/onlinepubs/009695399/functions/pwrite.html
	WriteTo, err := os.OpenFile(binlogDirectory+"/event-log.bin", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	readFrom, err := os.Open(WriteTo.Name())
	if err != nil {
		return nil, err
	}

	store := &EventStorage{
		WriteTo:      WriteTo,
		ReadFromFile: readFrom,
	}
	return store, nil
}

// String representation for humans, useful in log files etc.
func (s *EventStorage) String() string {
	// Is this a slow-ass binlog?
	str := ""
	if s.SyncEveryWrite {
		str = "*SYNC* "
	}
	return str + "binlog file " + s.WriteTo.Name()
}

// Store atomically and synchronously adds an entry to the binlog.
func (s *EventStorage) Write(size uint32, e *storage.StoredEvent) error {
	// Write size bytes
	entry := make([]byte, 4)
	binary.LittleEndian.PutUint32(entry, size)
	// Add flatbuffer bytes
	entry = append(entry, e.Table().Bytes...)
	_, err := s.WriteTo.Write(entry)
	// Note: Because we're writing the entire entry as one buffer, to a file in O_APPEND mode,
	// we assume that, even if multiple event stores are attached to the same file, they will not overwrite
	// one another's entries, or see partial data.
	if len(entry) > MaxRecommendedEvtSize {
		log.WithField("file", s.WriteTo.Name()).Warnf("writing of large events are discouraged: %d bytes", len(entry))
	}
	if err != nil {
		return err
	}
	// Enforce actual flushing to durable storage
	if s.SyncEveryWrite {
		err = s.WriteTo.Sync()
		if err != nil {
			return err
		}
	}
	// Store 'last written' position
	// TODO: Is this a performance bottleneck? Is there a better way?
	wpos, _ := s.WriteTo.Seek(0, io.SeekCurrent)
	s.lastWritePos = wpos
	return nil
}

func (s *EventStorage) ReadAll() (storage.EventIterator, error) {
	return s.ReadFrom(0)
}

func (s *EventStorage) ReadFrom(pos int64) (storage.EventIterator, error) {
	// start reading from pos, relative to origin of file
	ret, err := s.ReadFromFile.Seek(pos, io.SeekStart)
	if err != nil {
		return nil, err
	}

	if ret != pos {
		err := fmt.Errorf("Failed to seek position %d in file (achieved position %d)", pos, ret)
		return nil, err
	}

	// Set up a scanner, with correct tokenizer for our stored events,
	// and nice big fat buffer for fast reading
	scanner := bufio.NewScanner(s.ReadFromFile)
	scanner.Split(StoredEventTokenizer)
	scanner.Buffer(make([]byte, MaxSupportedEvtSize+4), MaxSupportedEvtSize+4)

	return &readAllIterator{
		scanner:  scanner,
		readFrom: s.ReadFromFile,
		srcName:  s.String(),
	}, nil
}

func (s *EventStorage) Close() error {
	err := s.WriteTo.Close()
	if err != nil {
		return err
	}
	return nil
}

func (s *EventStorage) GetWritePos() int64 {
	return s.lastWritePos
}

// Encapsulated implementation of `EventIterator`
type readAllIterator struct {
	readFrom io.Reader
	nextEvt  *storage.StoredEvent
	nextErr  error
	srcName  string
	count    int
	pos      int64
	scanner  *bufio.Scanner
}

func (i *readAllIterator) Next() bool {
	i.scanner.Scan()
	buf := i.scanner.Bytes()
	readErr := i.scanner.Err()
	/*
		log.WithFields(log.Fields{
			"bufIsNil": buf == nil,
			"bufSize":  len(buf),
			"hasError": readErr != nil,
		}).Trace("Read next event")
	*/

	// No data, but no error (done)
	if buf == nil && readErr == nil {
		i.nextErr = nil
		i.nextEvt = nil
		return false
	}

	// No data, but non-EOF error occurred
	if buf == nil && readErr != nil {
		i.nextErr = readErr
		i.nextEvt = nil
		return true // Next call to Value() will return the error
	}

	// We got data
	i.pos += int64(len(buf) + 4) // TODO: This is a hack. How to communicate true bytes read?

	// Store next, parsed value
	i.nextEvt = storage.GetRootAsStoredEvent(buf, 0)
	i.nextErr = nil

	// Increment counters
	i.count++

	return true
}

func (i *readAllIterator) Value() (*storage.StoredEvent, error) {
	if i.nextEvt != nil {
		return i.nextEvt, nil
	}
	if i.nextErr != nil {
		return nil, i.nextErr
	}
	// Either Next() was never called, or code bug in Next()
	panic("EventStorage iterator Next() must be called before Value()")
}

func (i *readAllIterator) GetReadPos() int64 {
	return i.pos
}

// StoredEventTokenizer is an implementation of `bufio.SplitFunc` that yields the raw bytes underlying
// a `StoredEvent` when scanning a buffered input stream. Note: This is for the simple, unidirectional
// stored event format of:
// [sizeA][    dataA    ][sizeB][    dataB    ].
func StoredEventTokenizer(data []byte, atEOF bool) (advance int, token []byte, err error) {

	/*
		log.WithFields(log.Fields{
			"dataSize": len(data),
			"atEOF":    atEOF,
		}).Trace("Parse next event (token)")
	*/

	// No data, and at end of file
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	// < 4 bytes, but more data is available - ask for more data
	if !atEOF && len(data) < 4 {
		return 0, nil, nil // More data pls
	}
	/// <4 bytes, no more data is available - data is corrupt
	if atEOF && len(data) < 4 {
		return 0, nil, errors.New("Stored event data is corrupt: Need at least 4 bytes (size prefix)")
	}

	// Obtain size from first 4 bytes
	szb := data[0:4]
	size := binary.LittleEndian.Uint32(szb)
	packetSize := int(4 + size) // Total size of event entry in file

	// Sanity check on size - otherwise, probably reading a file with wrong format or corrupted
	if size > MaxSupportedEvtSize {
		return 0, nil, fmt.Errorf("Bad size header for stored event: %d bytes (stream may be corrupted)", size)
	}

	// If there is enough data in the buffer to return the full event data - do so
	if len(data) >= packetSize {
		// This is the raw bytes underlying a `StoredEvent` (without size header)
		return packetSize, data[4:packetSize], nil
	} else {
		if !atEOF {
			// More data is available - ask for it
			return 0, nil, nil
		} else {
			// Last even unexpectedly truncated
			return 0, nil, fmt.Errorf("File unexpectedly truncated: Event should be %d bytes, but only %d bytes available", size, len(data)-4)
		}
	}
}
