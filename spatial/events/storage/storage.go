// Package store specifies a simple but plug-able events store
// for use in event-sourced services.
package storage

// EventStorage describes the functionality of a durable event store
// that powers services built in an event-sourcing style.
type EventStorage interface {
	// ReadAll provides an iterator that yields all
	// of the events in this store, in chronological order (oldest to newest)
	// Same as ReadFrom(0)
	ReadAll() (EventIterator, error)

	// ReadFrom provides an iterator that yields all events from
	// the requested position in the events stream on.
	// WARNING: It is the caller's responsibility to ensure that exactly
	// the correct offset is provided, such as to read non-corrupted event data.
	// Most usages of this event storage, using e.g. Flatbuffers, will not protect
	// against, or even detect, an incorrect offset. GLHF.
	ReadFrom(pos int64) (EventIterator, error)

	// TODO: ReadAllForEntity provides an iterator that yields all
	// events that effected state changes in the given entity.
	// ReadAllForEntity(entityType string, entityID []byte) (EventIterator, error)

	// Write the given event synchronously and atomically
	// For flatbuffer reasons, we have to pass the size in to cater for all usage scenarios
	Write(size uint32, event *StoredEvent) error

	// TODO: Pass in the entity type, ID, to allow for sensible storage / sharding etc
	// Note: For file-based impl, need some assumed logic to convert 16-byte ID to 1 UUID,
	// 32-byte ID to 2 concatenated UUIDS, for anything else perhaps SHA1/MD5 hash it.
	// Store(size uint32, event *StoredEvent, entityType string, entityID []byte) error

	// TODO: Other ways of reading, e.g. latest to oldest, "after" a certain time, etc...
	// ...

	// Human-friendly string representation. Should clearly identify the
	// source and fundamental configuration of the event store implementation.
	String() string

	// Close any underlying resources, such as open file handles or database
	// connections. Would typically be used as:
	// defer EventStorage.Close()
	Close() error

	// GetWritePos indicates the position, in bytes, that this store
	// will next write an event to.
	GetWritePos() int64
}

// EventIterator allows a consumer of events to read them
// in a streaming manner.
type EventIterator interface {
	Next() bool
	Value() (*StoredEvent, error)
	// GetReadPos indicates the position, in bytes, within the
	// event stream that this iterator will read next, if `Next()` is
	// called and there are more bytes to read.
	GetReadPos() int64
}
