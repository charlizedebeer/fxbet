// Package transform contains utilities to easily build programs that transform
// events, by piping events from one `EventStorage` to another through a transformation
// function that supports 1..n transformation.
package transform

import (
	"errors"
	"stream/fxbet/spatial/events/storage"
	"time"

	flatbuffers "github.com/google/flatbuffers/go"
	log "github.com/sirupsen/logrus"
)

// EvtTransformFn describes the type of a function that transforms a single source
// event, into zero or more destination events.
type EvtTransformFn func(evt *storage.StoredEventT) ([]*storage.StoredEventT, error)

// TransformEvents transforms all the events from the given source events storage, to the given destination, using the
// given transformation function. It supports 1..n transformation, in that the transformation function can return zero, one, or
// more output events for the given input event. This is a blocking operation, and may take a long time to run for large
// event stores, where I/O is usually the limitation.
func TransformEvents(src storage.EventStorage, dest storage.EventStorage, transform EvtTransformFn) (Result, error) {
	result := Result{}
	var outputSize int64 = 0
	if src == nil {
		return result, errors.New("src EventStorage is nil")
	}
	if dest == nil {
		return result, errors.New("dest EventStorage is nil")
	}
	if transform == nil {
		return result, errors.New("transform function is nil")
	}

	log.Infof("Starting event transformation from %s to %s", src.String(), dest.String())

	// Read through all source events
	start := time.Now()
	all, err := src.ReadAll()
	if err != nil {
		return result, err
	}
	for all.Next() {
		se, err := all.Value()
		if err != nil {
			return result, err
		}
		result.InputEvtCount++
		// TODO: How to track input size?
		// Transform
		transformed, err := transform(se.UnPack())
		if transformed != nil {
			for _, evt := range transformed {
				// Pack each resulting evt to bytes, write to dest
				b := flatbuffers.NewBuilder(1024)
				b.Finish(evt.Pack(b))
				bytes := b.FinishedBytes()
				se := storage.GetRootAsStoredEvent(bytes, 0)
				err := dest.Write(uint32(len(bytes)), se)
				outputSize += int64(len(bytes))
				if err != nil {
					return result, err
				}
				result.OutputEvtCount++
			}
		}

	}
	result.Duration = time.Since(start)
	// TODO: Calc size deltas

	return result, nil
}

type Result struct {
	// InputEvtCount counts how many events were read from the source storage
	InputEvtCount int64
	// OutputEvtCount counts how many events were written to the destination storage
	OutputEvtCount int64
	// TransformedCount counts how many of the input events results in different output events,
	// as an indication of "how many were transformed". The check is usually done simply by comparing
	// input and output values, e.g. at a pointer level, or a similar inexpensive mechanism so as not
	// to slow down the transformation.
	TransformedCount int64
	// A positive (growth) or negative (shrinkage) number of bytes that the output events
	// differ in size (in total) from the input events.
	SizeDeltaInBytes int64
	// How long the transformation was running for.
	Duration time.Duration
}

// Identity is a `EvtTransformFn` that passes events through without any modification.
func Identity(evt *storage.StoredEventT) ([]*storage.StoredEventT, error) {
	return []*storage.StoredEventT{evt}, nil
}
