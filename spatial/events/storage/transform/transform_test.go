package transform

import (
	"stream/fxbet/spatial/events/storage"
	mem "stream/fxbet/spatial/events/storage/memory"
	"stream/fxbet/spatial/state"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

// EvtUserMsg is a fictitious event type for "chat message received"
const EvtUserMsg uint16 = 111

// EvtSystemMsg is a fictitious event type for "system message received"
const EvtSystemMsg uint16 = 112

// BlankBadWords is an event transformation that, for "chat" events, blanks
// out any bad words. For unit testing simplicity, we're not using Flatbuffers
// to encode the events, but simple strings.
func BlankBadWords(evt *storage.StoredEventT) ([]*storage.StoredEventT, error) {
	// Only for user messages
	if evt.MsgType == EvtUserMsg {
		newContent := strings.ReplaceAll(string(evt.Payload), "Windows", "*******")
		return []*storage.StoredEventT{{
			MsgType: evt.MsgType,
			Headers: evt.Headers,
			Payload: []byte(newContent),
		}}, nil
	}
	// Otherwise, pass-through untouched
	return []*storage.StoredEventT{evt}, nil
}

// ChatState is a toy state (for use in a `Store`) that accumulates chat
// events in a single string. Totally pointless, but illustrative.
type ChatState struct {
	msgLog string
}

func (c *ChatState) ApplyEvent(evtType uint16, evtPayload []byte) {
	switch evtType {
	case EvtUserMsg:
		text := "User: " + string(evtPayload)
		c.msgLog += "\n" + text
	case EvtSystemMsg:
		text := "** SYSTEM MESSAGE: " + string(evtPayload)
		c.msgLog += "\n" + text
	default:
		log.Fatalf("Unsupported event type: %v", evtType)
	}
}

// getStorageWithChatLog creates and returns a new `EventStorage` which has been
// loaded
func getStorageWithChatLog(t *testing.T) storage.EventStorage {
	log.SetLevel(log.DebugLevel)
	// We create a store like we typically would for services
	es := mem.NewEventStorage("test/chat")
	chatState := &ChatState{}
	store, err := state.NewStore(chatState, es)
	if err != nil {
		t.Fatalf("Failed to set up test store (chat log): %+v", err)
	}

	// Throw some events into the store
	store.MustDispatch(EvtSystemMsg, nil, []byte("Welcome to a useless chat system"))
	store.MustDispatch(EvtUserMsg, nil, []byte("Bob: What are you using?"))
	store.MustDispatch(EvtUserMsg, nil, []byte("Ted: This would work so well in Windows!"))
	store.MustDispatch(EvtSystemMsg, nil, []byte("Going down for maintenance, please close all Windows."))

	// This is the accumulated state, FYI
	expected := `
** SYSTEM MESSAGE: Welcome to a useless chat system
User: Bob: What are you using?
User: Ted: This would work so well in Windows!
** SYSTEM MESSAGE: Going down for maintenance, please close all Windows.`
	assert.Equal(t, expected, chatState.msgLog)

	return es
}

// Test_TransformEvents_blankBadWords_oneMatch shows how to run stored events through a simple transformation
// function, which in this case transforms only one of the events.
func Test_TransformEvents_blankBadWords_oneMatch(t *testing.T) {
	// Obtain an event store with some chat events in it
	eventsSrc := getStorageWithChatLog(t)
	assert.NotNil(t, eventsSrc)

	// We wish to go back and rewrite history, and blank out bad words. We
	// need to pick a destination event storage that's different to the source!
	eventsDest := mem.NewEventStorage("test/chat/v2")

	result, err := TransformEvents(eventsSrc, eventsDest, BlankBadWords)
	assert.NotNil(t, result)
	assert.Nil(t, err)

	// After transformation, simulate the launching of a new service that attaches
	// a store (state) to the transformed events, check output
	chatState := &ChatState{}
	store, err := state.NewStore(chatState, eventsDest)
	assert.Nil(t, err)
	err = store.Hydrate()
	assert.Nil(t, err)

	expected := `
** SYSTEM MESSAGE: Welcome to a useless chat system
User: Bob: What are you using?
User: Ted: This would work so well in *******!
** SYSTEM MESSAGE: Going down for maintenance, please close all Windows.`
	assert.Equal(t, expected, chatState.msgLog)
}
