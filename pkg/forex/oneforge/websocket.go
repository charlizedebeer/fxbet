package oneforge

import (
	"encoding/json"
	"fmt"
	"stream/fxbet/pkg/forex"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

// WebSocket manages a connection to 1forge for currency feed updates
type WebSocket struct {
	apiKey    string
	sendTicks chan forex.Tick
	shutdown  chan struct{}
}

// New returns a WebSocket
func New(apiKey string, sendUpdates chan forex.Tick) WebSocket {
	return WebSocket{
		apiKey:    apiKey,
		sendTicks: sendUpdates,
		shutdown:  make(chan struct{}),
	}
}

// SubscribeTo connects to 1forge and subscribes for updates. Each call to SubscribeTo will
// make a new connection with it's own subscriptions and is concurrent safe.
func (w WebSocket) SubscribeTo(pairs []forex.CurrencyPair, debounce time.Duration) {
	lastSent := make(map[string]time.Time)

	backOff := 1 * time.Second
	for {
		ws, err := Connect("wss://sockets.1forge.com/socket", w.apiKey)
		if err != nil {
			time.Sleep(backOff)
			backOff *= 2
			continue
		}
		defer ws.Close()
		backOff = 1 * time.Second

		err = SubscribeTo(ws, pairs)
		if err != nil {
			continue
		}

		for {
			select {
			case <-w.shutdown:
				return
			default:
				// proceed to ReadTick
			}

			tick, err := ReadTick(ws)
			if err != nil {
				break
			}
			if time.Since(lastSent[tick.Symbol]) >= debounce && isSubscribedTo(pairs, tick.Symbol) {
				lastSent[tick.Symbol] = time.Now()
				w.sendTicks <- tick
			}
		}

		ws.Close()
	}
}

// Shutdown stops and closes the websocket
func (w WebSocket) Shutdown() {
	w.shutdown <- struct{}{}
}

func isSubscribedTo(pairs []forex.CurrencyPair, tickSymbol string) bool {
	for _, ccy := range pairs {
		if string(ccy) == tickSymbol {
			return true
		}
	}
	return false
}

const (
	msgLogin              string = "login"
	msgMessage            string = "message"
	msgForceClose         string = "force_close"
	msgHeart              string = "heart"
	msgPostLoginSuccess   string = "post_login_success"
	msgUpdate             string = "update"
	msgSubscribeTo        string = "subscribe_to"
	msgUnsubscribeFrom    string = "unsubscribe_from"
	msgSubscribeToAll     string = "subscribe_to_all"
	msgUnsubscribeFromAll string = "unsubscribe_from_all"
)

// Connect will open a connection, login with credentials and wait for a login success response.
func Connect(url string, apiKey string) (*websocket.Conn, error) {
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		return nil, err
	}

	err = conn.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%s|%s", msgLogin, apiKey)))
	if err != nil {
		return nil, err
	}

	for {
		eventType, _, err := readMessage(conn)
		if err != nil {
			return nil, err
		}
		if eventType == msgPostLoginSuccess {
			break
		}
	}

	return conn, nil
}

// SubscribeTo will send a subscription message for each currency pair.
func SubscribeTo(conn *websocket.Conn, pairs []forex.CurrencyPair) error {
	for _, ccyPair := range pairs {
		err := conn.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%s|%s", msgSubscribeTo, ccyPair)))
		if err != nil {
			return err
		}
	}
	return nil
}

// ReadTick will read messages until it gets an error or a tick.
func ReadTick(conn *websocket.Conn) (forex.Tick, error) {
	var tick forex.Tick
	for {
		eventType, data, err := readMessage(conn)
		if err != nil {
			return tick, fmt.Errorf("read error: %v", err)
		}
		if eventType == msgForceClose {
			return tick, fmt.Errorf("forced to close connection")
		}
		if eventType == msgHeart {
			err = conn.WriteMessage(websocket.TextMessage, []byte("beat"))
			if err != nil {
				return tick, fmt.Errorf("heart beat error: %v", err)
			}
		}
		if eventType == msgUpdate {
			err := json.Unmarshal([]byte(data), &tick)
			if err != nil {
				return tick, err
			}
			return tick, nil
		}
	}
}

func readMessage(conn *websocket.Conn) (string, string, error) {
	_, msg, err := conn.ReadMessage()
	if err != nil {
		return "", "", err
	}

	strMsg := string(msg)

	var eventType string
	var data string
	index := strings.Index(strMsg, "|")
	if index != -1 {
		eventType = strMsg[0:index]
		data = strMsg[index+1:]
	} else {
		eventType = strMsg
	}

	return eventType, data, nil
}
