package oneforge

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"stream/fxbet/pkg/forex"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
)

var upgrader = websocket.Upgrader{}

func Test_OneForge_WebSocket(t *testing.T) {
	pairs := []forex.CurrencyPair{
		"NOK/EUR", // 0.099432815,
		"INR/EUR", // 0.011290367,
		"JPY/EUR", // 0.0076563706,
		"CAD/EUR", // 0.66658038,
		"BRL/EUR", // 0.15068892,
		"PEN/EUR", // 0.23115696,
		"CLP/EUR", // 0.0011764862,
		"USD/EUR", // 0.84059258,
		"NZD/EUR", // 0.59100975,
	}

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		c, err := upgrader.Upgrade(w, r, nil)
		assert.Nil(t, err)
		defer c.Close()

		_, msg, err := c.ReadMessage()
		assert.Nil(t, err)
		assert.Equal(t, "login|API-KEY", string(msg))

		c.WriteMessage(websocket.TextMessage, []byte(msgPostLoginSuccess))
		for _, pair := range pairs {
			_, msg, err = c.ReadMessage()
			assert.Nil(t, err)
			assert.Equal(t, fmt.Sprintf("subscribe_to|%s", pair), string(msg))
		}

		tickBytes, err := json.Marshal(forex.Tick{
			Timestamp: time.Now().Unix(),
			Symbol:    "BTC/USD",
			Bid:       123.123,
			Ask:       123.123,
			Mid:       123.123,
		})
		assert.Nil(t, err)
		payload := append([]byte("update|"), tickBytes...)
		c.WriteMessage(websocket.TextMessage, payload)
	}))
	defer s.Close()

	url := "ws" + strings.TrimPrefix(s.URL, "http")
	ws, err := Connect(url, "API-KEY")
	assert.Nil(t, err)

	err = SubscribeTo(ws, pairs)
	assert.Nil(t, err)

	tick, err := ReadTick(ws)
	assert.Nil(t, err)
	assert.Equal(t, "BTC/USD", tick.Symbol)
}
