package forex

// Tick describes a currency pair tick update.
type Tick struct {
	Timestamp int64   `json:"t"`
	Symbol    string  `json:"s"`
	Bid       float64 `json:"b"`
	Ask       float64 `json:"a"`
	Mid       float64 `json:"p"`
}

// CurrencyPair as two currencies joined with a `/` such as BTC/USD
type CurrencyPair string
