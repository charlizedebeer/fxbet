package currency

// Currency respresents an amount of a currency.
type Currency struct {
	Amount float64 `json:"amount" example:"100"`
	CCY    string  `json:"ccy" example:"EUR"`
}

// New ...
func New(ccy string, amount float64) Currency {
	return Currency{amount, ccy}
}

// AmountBankers returns the amount as int with 6 decimal places
func (c Currency) AmountBankers() int64 {
	return int64(c.Amount*1_000_000 + 0.5)
}

// FromBankers takes a banker rounded value and converts it to float64
func FromBankers(amount int64) float64 {
	return float64(amount) / 1_000_000
}
