package agreements

import (
	"fmt"
	"stream/fxbet/spatial/atomic/state"
	"stream/fxbet/spatial/events/storage"
)

//go:generate flatc --go --gen-onefile --gen-object-api --go-namespace agreements events.fbs

// Service ...
type Service struct {
	state.Atomic
}

// New instantiates a new wallet API
func New(evtStore storage.EventStorage) Service {
	s := Service{}
	s.Atomic = state.New(s, evtStore)
	return s
}

// IsAllAgreed returns true if all the terms have been agreed to, false otherwise
func (s Service) IsAllAgreed(actionName string, termIDs []string) bool {
	for _, term := range s.TermsByActionName(actionName) {
		agreedTo := false
		for _, id := range termIDs {
			if term.ID == id {
				agreedTo = true
				break
			}
		}
		if !agreedTo {
			return false
		}
	}
	return true
}

// UserAcceptsTerms makes the terms as accepted by the user.
func (s Service) UserAcceptsTerms(userID string, termIDs []string) error {
	for _, id := range termIDs {
		term, ok := s.TermByID(id)
		if !ok {
			return fmt.Errorf("term (%v) is not found", id)
		}
		err := s.UserAcceptsTerm(userID, term.ID)
		if err != nil {
			return err
		}
	}
	return nil
}

// ApplyEvent updates state according to the event. This is already called atomically
func (s Service) ApplyEvent(evtType uint16, payload []byte) {
}

// TermsByActionName returns all the terms required for the given action
func (s Service) TermsByActionName(actionName string) []Term {
	return []Term{}
}

// TermByID returns the term and if it exists or not
func (s Service) TermByID(termID string) (Term, bool) {
	return Term{}, false
}

// UserAcceptsTerm updates state to show the user accepted the term
func (s Service) UserAcceptsTerm(userID string, termID string) error {
	return fmt.Errorf("not implemented")
}
