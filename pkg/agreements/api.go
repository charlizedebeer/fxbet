package agreements

// Term contains just the ID and title of the terms
type Term struct {
	ID    string `json:"id"`
	Title string `json:"title" description:"The title of the previously-defined terms"`
}
