package email

// Email ...
type Email struct {
	Subject     string
	ToName      string
	ToAddress   string
	FromName    string
	FromAddress string
}

// Sender describes the types of emails that can be sent
type Sender interface {
	SendVerifyAddress(Email, map[string]string) error
}
