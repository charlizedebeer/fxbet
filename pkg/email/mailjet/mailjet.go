package mailjet

import (
	"fmt"
	"net/url"
	"stream/fxbet/pkg/email"

	"github.com/mailjet/mailjet-apiv3-go"
)

// Service ...
type Service struct {
	PublicAPIKey  string
	PrivateAPIKey string
	BearerToken   string
}

// SendVerifyAddress send an email that submits a form back to the link provided.
func (s Service) SendVerifyAddress(e email.Email, data map[string]string) error {
	mailJetClient := mailjet.NewMailjetClient(s.PublicAPIKey, s.PrivateAPIKey)

	parsedURI, err := url.Parse(fmt.Sprintf("mailto:%s", e.ToAddress))
	if err != nil {
		return err
	}

	// TODO this is annoying.  Why cant I cast templateData.(map[string]interface{})?
	m := make(map[string]interface{})
	for key, val := range data {
		m[key] = val
	}

	// Create the MailJet SendMail request
	messagesInfo := []mailjet.InfoMessagesV31{
		{
			From: &mailjet.RecipientV31{
				Email: e.FromAddress,
				Name:  e.FromName,
			},
			To: &mailjet.RecipientsV31{
				mailjet.RecipientV31{
					Email: parsedURI.RequestURI(),
				},
			},
			TemplateID:       1794085,
			TemplateLanguage: true,
			Subject:          e.Subject,
			Variables:        m,
		},
	}

	// Send the message
	messages := mailjet.MessagesV31{Info: messagesInfo}
	_, err = mailJetClient.SendMailV31(&messages)
	if err != nil {
		return fmt.Errorf("Could not send the mail, %v", err)
	}

	return nil
}
