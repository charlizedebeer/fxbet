package server

import (
	"stream/fxbet/pkg/agreements"
	"stream/fxbet/pkg/games"
	"stream/fxbet/pkg/iam"
)

// URLGenerator is used to get a full url from a path
type URLGenerator func(string) string

// Endpoints
const (
	// User and registration
	POSTRegisterUser         = "/user/registrations"
	POSTCompleteRegistration = "/user/verifications"
	POSTAuthenticateUser     = "/user/auth"
	// Games
	GETAllGames             = "/games"
	POSTCreateGame          = "/games"
	GETAllGamesWithBranding = "/games/with-branding"
	GETGame                 = "/game/{id}"
	GETGameBranding         = "/game/{id}/branding"
	PUTGameStatus           = "/game/{id}/enabled"
	PUTMarketStatus         = "/game/{id}/open"
	PUTUpdateGame           = "/game/{id}"
	PUTUpdateBranding       = "/game/{id}/branding"
	// Agreements
	GETTerm          = "/terms/{id}"
	GETTermForAction = "/terms/by-action/{action}"
	GETAcceptedTerms = "/user/{user_id}/terms/accept"
	POSTAcceptTerms  = "/user/{user_id}/terms/accept"
)

// RegisterUserRequest is the input to the RegisterUser
type RegisterUserRequest struct {
	Name            iam.Name        `json:"name,required" description:"The players name"`
	Email           iam.Email       `json:"email,required" description:"Email used to identify the user" example:"john@email.com"`
	Country         iam.Country     `json:"country,required" description:"Country of residence for the user" example:"United Kingdom"`
	DateOfBirth     iam.DateOfBirth `json:"dateOfBirth,required" description:"Users date of birth in YYYY-MM-DD format" example:"2006-01-02"`
	Gender          iam.Gender      `json:"gender,omitempty" description:"Users gender" enums:"male,female,other"`
	AcceptedTermIDs []string        `json:"accepted_term_ids,omitempty" description:"Terms that the user accepted"`
}

// RegisterUserResponse is the success response from RegisterUser
type RegisterUserResponse struct {
	SignUpID string `json:"sign_up_id" description:"SignUpID used to identify the sign-up"`
}

// CompleteRegistrationRequest is the input to CompleteRegistration
type CompleteRegistrationRequest struct {
	Token    string `json:"token" description:"Token used to verify user"`
	Password string `json:"password" description:"Password used to secure user's account" example:"not1234"`
}

// CompleteRegistrationResponse is the success response from CompleteRegistration
type CompleteRegistrationResponse struct {
	Token    string `json:"token" description:"An authentication token"`
	PlayerID string `json:"player_id" description:"PlayerID used to identify the user"`
}

// AuthenticateUserRequest is the input to AuthenticateUser
type AuthenticateUserRequest struct {
	IDInStrategy string            `json:"id_in_strategy" example:"joe@bloggs.com"`
	Strategy     iam.LoginStrategy `json:"strategy" example:"1"`
	Password     string            `json:"password" description:"Password used to secure account" example:"mypasswordkeepsmesafe"`
	IPAddress    string            `json:"ip_address" swaggerignore:"true"`
}

// AuthenticateUserResponse is the success response from AuthenticateUser
type AuthenticateUserResponse struct {
	AuthToken    string            `json:"token" example:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAi..."`
	ContextRoles []iam.ContextRole `json:"roles"`
}

// AllGamesWithBrandingRequest is the input to a Games request
type AllGamesWithBrandingRequest struct {
	Tag string `json:"tag" description:"Filter games by tag"`
}

// GameWithBranding describes a game with it's branding information
type GameWithBranding struct {
	games.GameEntity
	Branding games.Branding `json:"branding"`
}

// AllGamesWithBrandingResponse is the success response from a Game request
type AllGamesWithBrandingResponse struct {
	Games []GameWithBranding `json:"games" description:"Game with branding info"`
}

// AllGamesRequest is the input to a Games request
type AllGamesRequest struct {
	Tag string `json:"tag" description:"Filter games by tag"`
}

// AllGamesResponse is the success response from a Game request
type AllGamesResponse struct {
	Games []games.GameEntity `json:"games" description:"Game with branding info"`
}

// CreateGameRequest is the input to creating a new game
type CreateGameRequest struct {
	Game     games.Game      `json:"game" description:"Game information"`
	Branding *games.Branding `json:"branding" description:"Optional branding information"`
}

// CreateGameResponse is the success response from creating a game
type CreateGameResponse struct {
	Game     games.GameEntity `json:"game" description:"Created game information"`
	Branding *games.Branding  `json:"branding" description:"Optional created game's branding"`
}

// GameStatusRequest is the input to changing a game's status
type GameStatusRequest struct {
	id string
	// * 0 = Disabled.
	// * 1 = Enabled.
	Status bool
}

// ParseVars parses path parameters
func (r *GameStatusRequest) ParseVars(vars map[string]string) {
	r.id = vars["id"]
}

// MarketStatusResponse is the success response from changing a game status
type MarketStatusResponse struct {
}

// MarketStatusRequest is the input to changing a game's status
type MarketStatusRequest struct {
	id string
	// * 0 = Closed.
	// * 1 = Opened.
	Status bool
}

// ParseVars parses path parameters
func (r *MarketStatusRequest) ParseVars(vars map[string]string) {
	r.id = vars["id"]
}

// GameStatusResponse is the success response from changing a game status
type GameStatusResponse struct {
}

// GameRequest is the input to fetching game information.
type GameRequest struct {
	id string
}

// GameResponse is the success response from fetching game information
type GameResponse struct {
	Game games.GameEntity `json:"game" description:"Game requested"`
}

// ParseVars parses path parameters
func (r *GameRequest) ParseVars(vars map[string]string) {
	r.id = vars["id"]
}

// UpdateGameRequest is the input to update a game's
type UpdateGameRequest struct {
	id   string
	Game games.Game `json:"game" description:"Game details to update/replace"`
}

// ParseVars parses path parameters
func (r *UpdateGameRequest) ParseVars(vars map[string]string) {
	r.id = vars["id"]
}

// UpdateGameResponse is the success response from fetching a game's branding
type UpdateGameResponse struct {
	Game games.GameEntity `json:"game" description:"Updated"`
}

// GameBrandingRequest is the input to fetching a game's branding
type GameBrandingRequest struct {
	gameID string
}

// ParseVars parses path parameters
func (r *GameBrandingRequest) ParseVars(vars map[string]string) {
	r.gameID = vars["id"]
}

// GameBrandingResponse is the success response from fetching a game's branding
type GameBrandingResponse struct {
	Branding games.Branding `json:"branding" description:"Game's branding information"`
}

// UpdateBrandingRequest is the input to fetching a game's branding
type UpdateBrandingRequest struct {
	gameID   string
	Bradning games.Branding `json:"branding" description:"Branding information"`
}

// ParseVars parses path parameters
func (r *UpdateBrandingRequest) ParseVars(vars map[string]string) {
	r.gameID = vars["id"]
}

// UpdateBrandingResponse is the success response from fetching a game's branding
type UpdateBrandingResponse struct {
	Branding games.Branding `json:"branding" description:"Updated branding information"`
}

// TermRequest is the input to query a term by ID
type TermRequest struct {
	id string
}

// ParseVars parses path parameters
func (r *TermRequest) ParseVars(vars map[string]string) {
	r.id = vars["id"]
}

// TermResponse is the success response to querying a term by ID
type TermResponse struct {
	Term agreements.Term `json:"term" description:"Term requested"`
}

// TermForActionRequest is the input to query terms for a given action
type TermForActionRequest struct {
	action string
}

// ParseVars parses path parameters
func (r *TermForActionRequest) ParseVars(vars map[string]string) {
	r.action = vars["action"]
}

// TermForActionResponse is the success output for querying terms for a given action
type TermForActionResponse struct {
	Terms []agreements.Term `json:"terms" description:"Terms required for the given action"`
}

// AcceptedTermsRequest is the input to query a user's accepted terms
type AcceptedTermsRequest struct {
	userID string
}

// ParseVars parses path parameters
func (r *AcceptedTermsRequest) ParseVars(vars map[string]string) {
	r.userID = vars["userID"]
}

// AcceptedTermsResponse is the success output for querying a user's accepted terms
type AcceptedTermsResponse struct {
	Terms []agreements.Term `json:"terms" description:"User's accepted terms"`
}

// AcceptTermsRequest is the input to accept terms for a given user
type AcceptTermsRequest struct {
	userID string
	Term   agreements.Term `json:"term" description:"Term to be accepted"`
}

// ParseVars parses path parameters
func (r *AcceptTermsRequest) ParseVars(vars map[string]string) {
	r.userID = vars["userID"]
}

// AcceptTermsResponse is the success output for accepting terms for a given user.
type AcceptTermsResponse struct {
	Terms []agreements.Term `json:"terms" description:"Terms accepted"`
}
