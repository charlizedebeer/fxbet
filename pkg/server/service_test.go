package server

import (
	"fmt"
	"reflect"
	"stream/fxbet/pkg/agreements"
	"stream/fxbet/pkg/email"
	"stream/fxbet/pkg/errors"
	"stream/fxbet/pkg/forex/currency"
	"stream/fxbet/pkg/games"
	"stream/fxbet/pkg/iam"
	"stream/fxbet/spatial/events/storage/memory"
	"testing"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func newTestService(t *testing.T) Service {
	iamService := iam.New(memory.NewEventStorage("iam-test"))
	go iamService.Run()
	loadIAM(t, iamService)

	agreementsService := agreements.New(memory.NewEventStorage("agreements-test"))
	go agreementsService.Run()
	loadAgreements(t, agreementsService)

	gamesService := games.New(memory.NewEventStorage("games-test"))
	go gamesService.Run()

	fxbetService := New(memory.NewEventStorage("fxbet-test"))
	fxbetService.IAM = iamService
	fxbetService.Agreements = agreementsService
	fxbetService.EmailSender = mockEmailSender{}
	fxbetService.Games = gamesService
	fxbetService.URLGenerator = func(p string) string {
		return fmt.Sprintf("http://localhost:1234/sign-up/verify/%s", p)
	}
	go fxbetService.Run()

	return fxbetService
}

type mockEmailSender struct{}

func (m mockEmailSender) SendVerifyAddress(email.Email, map[string]string) error {
	return nil
}

func loadIAM(t *testing.T, i iam.Service) {
	player, err := i.CreateUser(iam.LoginStrategyPassword, "player@fxbet.co", "P@ssw0rd")
	if err != nil {
		t.Fatal(err)
	}
	role := iam.ContextRole{Role: RolePlayer}
	err = i.AddRole(player.ID, role)
	if err != nil {
		t.Fatal(err)
	}
}

func loadAgreements(t *testing.T, i agreements.Service) {

}

func loadGames(t *testing.T, s games.Service) []games.GameEntity {
	var gg []games.GameEntity
	g1, err := s.CreateGame(games.Game{
		Feed:   "EUR/USD",
		Tags:   []string{"fast"},
		Market: games.MarketTypeRises,
		Status: games.GameStatusEnabled,
		MinimumStake: currency.Currency{
			Amount: 5.0,
			CCY:    "EUR",
		},
		MaximumStake: currency.Currency{
			Amount: 100.0,
			CCY:    "EUR",
		},
		MinimumOdds:   0.0,
		MaximumOdds:   1.0,
		OddsTolerance: 1.0,
		Rating:        3,
	})
	if err != nil {
		t.Fatal(err)
	}
	g2, err := s.CreateGame(games.Game{
		Feed:   "BTC/USD",
		Tags:   []string{"slow"},
		Market: games.MarketTypeRises,
		Status: games.GameStatusEnabled,
		MinimumStake: currency.Currency{
			Amount: 5.0,
			CCY:    "EUR",
		},
		MaximumStake: currency.Currency{
			Amount: 100.0,
			CCY:    "EUR",
		},
		MinimumOdds:   0.0,
		MaximumOdds:   1.0,
		OddsTolerance: 1.0,
		Rating:        2,
	})
	if err != nil {
		t.Fatal(err)
	}
	g3, err := s.CreateGame(games.Game{
		Feed:   "EUR/ZAR",
		Tags:   []string{},
		Market: games.MarketTypeRises,
		Status: games.GameStatusEnabled,
		MinimumStake: currency.Currency{
			Amount: 5.0,
			CCY:    "EUR",
		},
		MaximumStake: currency.Currency{
			Amount: 100.0,
			CCY:    "EUR",
		},
		MinimumOdds:   0.0,
		MaximumOdds:   1.0,
		OddsTolerance: 1.0,
		Rating:        1,
	})
	if err != nil {
		t.Fatal(err)
	}
	if g, ok := s.Game(g1); ok {
		gg = append(gg, g)
	}
	if g, ok := s.Game(g2); ok {
		gg = append(gg, g)
	}
	if g, ok := s.Game(g3); ok {
		gg = append(gg, g)
	}
	return gg
}

func loadGamesWithBranding(t *testing.T, s games.Service) []GameWithBranding {
	var gg []GameWithBranding

	return gg
}

func loadFXBetRegistration(t *testing.T, f Service) string {
	res, err := f.RegisterUser(RegisterUserRequest{
		Name: iam.Name{
			FirstName: "Player",
			LastName:  "One",
		},
		Email:       "player1@fxbet.co",
		Country:     "ZA",
		DateOfBirth: iam.DateOfBirth(time.Date(1980, time.January, 1, 0, 0, 0, 0, time.UTC)),
		Gender:      "male",
	})
	if err != nil {
		t.Fatal(err)
	}
	var jwtClaims jwt.MapClaims = map[string]interface{}{
		"sub": res.SignUpID,
	}
	tokenString, err := f.IAM.SignedToken(jwtClaims)
	if err != nil {
		t.Fatal(err)
	}
	return tokenString
}

func Test_AuthenticateUser(t *testing.T) {
	s := newTestService(t)
	defer s.IAM.Shutdown()
	defer s.Agreements.Shutdown()
	defer s.Shutdown()

	tests := []struct {
		name        string
		args        AuthenticateUserRequest
		want        AuthenticateUserResponse
		wantErr     bool
		wantErrCode errors.Code
	}{
		{"player known login", AuthenticateUserRequest{
			IDInStrategy: "player@fxbet.co",
			Strategy:     iam.LoginStrategyPassword,
			Password:     "P@ssw0rd",
			IPAddress:    "127.0.0.1",
		}, AuthenticateUserResponse{
			ContextRoles: []iam.ContextRole{{Role: RolePlayer}},
		}, false, 0},
		{"player incorrect password", AuthenticateUserRequest{
			IDInStrategy: "player@fxbet.co",
			Strategy:     iam.LoginStrategyPassword,
			Password:     "P@ssw0rd123",
			IPAddress:    "127.0.0.1",
		}, AuthenticateUserResponse{}, true, CredentialsNotFoundRefusal},
		{"player incorrect username", AuthenticateUserRequest{
			IDInStrategy: "unknown@fxbet.co",
			Strategy:     iam.LoginStrategyPassword,
			Password:     "P@ssw0rd",
			IPAddress:    "127.0.0.1",
		}, AuthenticateUserResponse{}, true, CredentialsNotFoundRefusal},
		{"invalid login strategy", AuthenticateUserRequest{
			IDInStrategy: "player@fxbet.co",
			Strategy:     iam.LoginStrategy(0),
			Password:     "P@ssw0rd",
			IPAddress:    "127.0.0.1",
		}, AuthenticateUserResponse{}, true, CredentialsNotFoundRefusal},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.AuthenticateUser(tt.args)
			if errorCode, ok := err.(errors.Coded); tt.wantErr && (!ok || errorCode != tt.wantErrCode) {
				t.Errorf("Service.AuthenticateUser() error = %v, wantErrCode %v", err, tt.wantErrCode)
				return
			}
			if got.AuthToken == "" && !tt.wantErr {
				t.Errorf("Service.AuthenticateUser().AuthToken was blank")
			}
			if !reflect.DeepEqual(got.ContextRoles, tt.want.ContextRoles) {
				t.Errorf("Service.AuthenticateUser().ContextRoles = %v, want %v", got.ContextRoles, tt.want.ContextRoles)
			}
		})
	}
}

func TestService_RegisterUser(t *testing.T) {
	s := newTestService(t)
	defer s.IAM.Shutdown()
	defer s.Agreements.Shutdown()
	defer s.Shutdown()
	tests := []struct {
		name        string
		args        RegisterUserRequest
		want        RegisterUserResponse
		wantErr     bool
		wantErrCode errors.Code
	}{
		{"valid", RegisterUserRequest{
			Name: iam.Name{
				FirstName: "First",
				LastName:  "Last",
			},
			Email:       "register@fxbet.co",
			Country:     "ZA",
			DateOfBirth: iam.DateOfBirth(time.Date(1980, time.January, 1, 0, 0, 0, 0, time.UTC)),
			Gender:      "male",
		}, RegisterUserResponse{}, false, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.RegisterUser(tt.args)
			if errorCode, ok := err.(errors.Coded); tt.wantErr && (!ok || errorCode != tt.wantErrCode) {
				t.Errorf("Service.RegisterUser() error = %v, wantErrCode %v", err, tt.wantErrCode)
				return
			}
			if got.SignUpID == "" {
				t.Errorf("Service.RegisterUser().SignUpID should not be blank")
			}
		})
	}
}

func TestService_CompleteRegistration(t *testing.T) {
	s := newTestService(t)
	defer s.IAM.Shutdown()
	defer s.Agreements.Shutdown()
	defer s.Shutdown()
	token := loadFXBetRegistration(t, s)
	unknownSignUpToken, err := s.IAM.SignedToken(map[string]interface{}{
		"sub": "asdasd",
	})
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name        string
		args        CompleteRegistrationRequest
		want        CompleteRegistrationResponse
		wantErr     bool
		wantErrCode errors.Code
	}{
		{"valid", CompleteRegistrationRequest{
			Token:    token,
			Password: "P@ssw0rd",
		}, CompleteRegistrationResponse{}, false, 0},
		{"invalid token", CompleteRegistrationRequest{
			Token:    "asdasdasdasd",
			Password: "P@ssw0rd",
		}, CompleteRegistrationResponse{}, true, InvalidSignUpToken},
		{"signup not found", CompleteRegistrationRequest{
			Token:    unknownSignUpToken,
			Password: "P@ssw0rd",
		}, CompleteRegistrationResponse{}, true, SignUpNotFoundRefusal},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.CompleteRegistration(tt.args)
			if errorCode, ok := err.(errors.Coded); tt.wantErr && (!ok || errorCode != tt.wantErrCode) {
				t.Errorf("Service.CompleteRegistration() error = %v, wantErrCode %v", err, tt.wantErrCode)
				return
			}
			if !tt.wantErr && got.PlayerID == "" {
				t.Errorf("Service.CompleteRegistration().PlayerID should not be blank")
			}
			if !tt.wantErr && got.Token == "" {
				t.Errorf("Service.CompleteRegistration().Token should not be blank")
			}
		})
	}
}

func TestService_AllGames(t *testing.T) {
	s := newTestService(t)
	defer s.IAM.Shutdown()
	defer s.Agreements.Shutdown()
	defer s.Shutdown()
	testGames := loadGames(t, s.Games)

	tests := []struct {
		name        string
		args        AllGamesRequest
		want        AllGamesResponse
		wantErr     bool
		wantErrCode errors.Code
	}{
		{"all games", AllGamesRequest{}, AllGamesResponse{
			Games: testGames,
		}, false, 0},
		{"fast games", AllGamesRequest{"fast"}, AllGamesResponse{
			Games: testGames,
		}, false, 0},
		{"slow games", AllGamesRequest{"slow"}, AllGamesResponse{
			Games: testGames,
		}, false, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.AllGames(tt.args)
			if errorCode, ok := err.(errors.Coded); tt.wantErr && (!ok || errorCode != tt.wantErrCode) {
				t.Errorf("Service.AllGames() error = %v, wantErrCode %v", err, tt.wantErrCode)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.AllGames() =\n     %v,\nwant %v", got, tt.want)
			}
		})
	}
}

func TestService_AllGamesWithBranding(t *testing.T) {
	s := newTestService(t)
	defer s.IAM.Shutdown()
	defer s.Agreements.Shutdown()
	defer s.Shutdown()
	testGames := loadGamesWithBranding(t, s.Games)

	tests := []struct {
		name        string
		args        AllGamesWithBrandingRequest
		want        AllGamesWithBrandingResponse
		wantErr     bool
		wantErrCode errors.Code
	}{
		{"all games", AllGamesWithBrandingRequest{}, AllGamesWithBrandingResponse{
			Games: testGames,
		}, false, 0},
		{"fast games", AllGamesWithBrandingRequest{"fast"}, AllGamesWithBrandingResponse{
			Games: testGames,
		}, false, 0},
		{"slow games", AllGamesWithBrandingRequest{"slow"}, AllGamesWithBrandingResponse{
			Games: testGames,
		}, false, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.AllGamesWithBranding(tt.args)
			if errorCode, ok := err.(errors.Coded); tt.wantErr && (!ok || errorCode != tt.wantErrCode) {
				t.Errorf("Service.AllGamesWithBranding() error = %v, wantErrCode %v", err, tt.wantErrCode)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.AllGamesWithBranding() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_CreateGame(t *testing.T) {
	// t.FailNow()
}

func TestService_Game(t *testing.T) {
	// t.FailNow()
}

func TestService_GameBranding(t *testing.T) {
	// t.FailNow()
}

func TestService_UpdateGame(t *testing.T) {
	// t.FailNow()
}

func TestService_GameStatus(t *testing.T) {
	// t.FailNow()
}

func TestService_MarketStatus(t *testing.T) {
	// t.FailNow()
}

func TestService_UpdateBranding(t *testing.T) {
	// t.FailNow()
}

func TestService_Term(t *testing.T) {
	// t.FailNow()
}

func TestService_TermForAction(t *testing.T) {
	// t.FailNow()
}

func TestService_AcceptedTerms(t *testing.T) {
	// t.FailNow()
}

func TestService_TAcceptTerms(t *testing.T) {
	// t.FailNow()
}
