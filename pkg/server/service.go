package server

//go:generate flatc --go --gen-onefile --gen-object-api --go-namespace server events.fbs

import (
	"fmt"
	"stream/fxbet/pkg/agreements"
	"stream/fxbet/pkg/email"
	"stream/fxbet/pkg/errors"
	"stream/fxbet/pkg/games"
	"stream/fxbet/pkg/iam"
	"stream/fxbet/pkg/ids"
	"stream/fxbet/spatial/atomic/state"
	"stream/fxbet/spatial/events/storage"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Service for FXBet
type Service struct {
	state.Atomic
	IAM          iam.Service
	Agreements   agreements.Service
	EmailSender  email.Sender
	Games        games.Service
	URLGenerator URLGenerator
	signUpByID   map[string]*RegisterUserRequest
}

// New instantiates a new wallet API
func New(evtStore storage.EventStorage) Service {
	s := Service{
		signUpByID: make(map[string]*RegisterUserRequest),
	}
	s.Atomic = state.New(s, evtStore)
	return s
}

// CreateSignUpForm creates a new signup that should be completed after verifying email address
func (s Service) CreateSignUpForm(req RegisterUserRequest) (string, error) {
	id := ids.NewID()
	err := s.Dispatch(uint16(EvtTypeEvtSignUpCreated), &EvtSignUpCreatedT{
		ID:          id,
		FirstName:   req.Name.FirstName,
		MiddleNames: req.Name.MiddleNames,
		LastName:    req.Name.LastName,
		Prefix:      req.Name.Prefix,
		Suffix:      req.Name.Suffix,
		Email:       req.Email.String(),
		Country:     string(req.Country),
		DateOfBirth: time.Time(req.DateOfBirth).UTC().UnixNano(),
		// Gender:          EnumValuesGender[string(req.Gender)],
		AcceptedTermIDs: []string{},
	})
	if err != nil {
		return "", err
	}
	return id, nil
}

// ApplyEvent updates state according to the event. This is already called atomically.
func (s Service) ApplyEvent(evtType uint16, payload []byte) {
	switch EvtType(evtType) {
	case EvtTypeEvtSignUpCreated:
		evt := GetRootAsEvtSignUpCreated(payload, 0)
		id := string(evt.ID())
		if id == "" {
			return
		}
		acceptedTerms := make([]string, 0, evt.AcceptedTermIDsLength())
		for i := 0; i < evt.AcceptedTermIDsLength(); i++ {
			acceptedTerms[i] = string(evt.AcceptedTermIDs(i))
		}
		s.signUpByID[id] = &RegisterUserRequest{
			Name: iam.Name{
				FirstName:   string(evt.FirstName()),
				MiddleNames: string(evt.MiddleNames()),
				LastName:    string(evt.LastName()),
				Prefix:      string(evt.Prefix()),
				Suffix:      string(evt.Suffix()),
			},
			Email:       iam.Email(string(evt.Email())),
			Country:     iam.Country(string(evt.Country())),
			DateOfBirth: iam.DateOfBirth(time.Unix(0, evt.DateOfBirth())),
			// Gender:          iam.GenderFromEnum(evt.Gender()),
			AcceptedTermIDs: acceptedTerms,
		}
	}
}

// SignUpFormByID returns the signup form filled in the start of signup
func (s Service) SignUpFormByID(id string) (RegisterUserRequest, bool) {
	var form RegisterUserRequest
	var ok bool
	s.Atomically(func() {
		if ptr, found := s.signUpByID[id]; found && ptr != nil {
			form = *ptr
			ok = true
		}
	})
	return form, ok
}

// RegisterUserRefusal is the errors that can be returned from RegisterUser
const (
	InvalidContactRefusal              errors.Code = 1000
	InvalidDateOfBirthRefusal          errors.Code = 1001
	PlayerNotOver18Refusal             errors.Code = 1002
	InvalidCountryOfResidenceRefusal   errors.Code = 1003
	UnsupportedCountryRefusal          errors.Code = 1004
	InsufficientInformationRefusal     errors.Code = 1005
	DuplicateSignupRefusal             errors.Code = 1006
	NotAllRequiredTermsAcceptedRefusal errors.Code = 1007
)

// RegisterUser stores registration information but does not create any users until the user
// verifies their email and chooses a password.
// @Summary Start Registration Process
func (s Service) RegisterUser(req RegisterUserRequest) (RegisterUserResponse, error) {
	if iam.Age(req.DateOfBirth) < 18 {
		return RegisterUserResponse{}, PlayerNotOver18Refusal
	}

	if s.IAM.UserExists(iam.LoginStrategyPassword, req.Email.String()) {
		return RegisterUserResponse{}, DuplicateSignupRefusal
	}

	if !s.Agreements.IsAllAgreed("use.platform", req.AcceptedTermIDs) {
		return RegisterUserResponse{}, NotAllRequiredTermsAcceptedRefusal
	}

	signUpID, err := s.CreateSignUpForm(req)
	if err != nil {
		return RegisterUserResponse{}, err
	}

	var jwtClaims jwt.MapClaims = map[string]interface{}{
		"sub": signUpID,
	}
	tokenString, err := s.IAM.SignedToken(jwtClaims)
	if err != nil {
		return RegisterUserResponse{}, err
	}
	err = s.EmailSender.SendVerifyAddress(email.Email{
		ToAddress:   req.Email.String(),
		FromName:    "FXBet",
		FromAddress: "tech@fxbet.co",
		Subject:     "Please confirm your email address",
	}, map[string]string{
		"token-url": s.URLGenerator(tokenString),
	})
	if err != nil {
		return RegisterUserResponse{}, err
	}

	return RegisterUserResponse{SignUpID: signUpID}, nil
}

// CompleteRegistrationRefusal is the errors that can be returned from CompleteRegistration
const (
	SignUpNotFoundRefusal errors.Code = 1000
	InvalidSignUpToken    errors.Code = 1001
)

// CompleteRegistration looks up a previously started registration and then creates the user, assigns role
// and enables the user.
// @Summary Complete a Registration
func (s Service) CompleteRegistration(req CompleteRegistrationRequest) (CompleteRegistrationResponse, error) {
	if !s.IAM.IsTokenValid(req.Token) {
		return CompleteRegistrationResponse{}, InvalidSignUpToken
	}
	signUpID, ok := s.IAM.TokenSubject(req.Token)
	if !ok {
		return CompleteRegistrationResponse{}, SignUpNotFoundRefusal
	}
	signUpForm, ok := s.SignUpFormByID(signUpID)
	if !ok {
		return CompleteRegistrationResponse{}, SignUpNotFoundRefusal
	}

	user, err := s.IAM.CreateUser(iam.LoginStrategyPassword, signUpForm.Email.String(), req.Password)
	if err != nil {
		return CompleteRegistrationResponse{}, err
	}
	err = s.IAM.AddUserRoles(user.ID, iam.ContextRole{
		Role: RolePlayer,
	})
	if err != nil {
		return CompleteRegistrationResponse{}, err
	}
	err = s.IAM.SetStatus(user.ID, true)
	if err != nil {
		return CompleteRegistrationResponse{}, err
	}

	token, err := s.IAM.AuthToken(user.ID)
	if err != nil {
		return CompleteRegistrationResponse{}, err
	}

	err = s.Agreements.UserAcceptsTerms(user.ID, signUpForm.AcceptedTermIDs)
	if err != nil {
		return CompleteRegistrationResponse{}, err
	}

	return CompleteRegistrationResponse{
		Token:    token,
		PlayerID: user.ID,
	}, nil
}

// AuthenticateUserRefusal is the errors that can be returned from AuthenticateUser
const (
	CredentialsNotFoundRefusal errors.Code = 1000
)

// AuthenticateUser looks for a user with the given credentials and returns an authentication token to be used in requests.
// @Summary Authenticate User
func (s Service) AuthenticateUser(req AuthenticateUserRequest) (AuthenticateUserResponse, error) {
	user, ok := s.IAM.UserFromCredentials(req.Strategy, req.IDInStrategy, req.Password)
	if !ok {
		return AuthenticateUserResponse{}, CredentialsNotFoundRefusal
	}

	roles := s.IAM.UserRoles(user.ID)

	token, err := s.IAM.AuthToken(user.ID)
	if err != nil {
		return AuthenticateUserResponse{}, err
	}

	return AuthenticateUserResponse{
		AuthToken:    token,
		ContextRoles: roles,
	}, nil
}

// AllGamesWithBranding returns all the configured games with branding.
// @Summary All games with branding
func (s Service) AllGamesWithBranding(req AllGamesWithBrandingRequest) (AllGamesWithBrandingResponse, error) {
	var gg []GameWithBranding
	for _, g := range s.Games.Games() {
		game := GameWithBranding{GameEntity: g}
		branding, ok := s.Games.Branding(g.ID)
		if ok {
			game.Branding = branding
		}
	}
	return AllGamesWithBrandingResponse{
		Games: gg,
	}, nil
}

// AllGames returns all the configured games.
// @Summary All games
func (s Service) AllGames(req AllGamesRequest) (AllGamesResponse, error) {
	var gg []games.GameEntity
	for _, g := range s.Games.Games() {
		gg = append(gg, g)
	}
	return AllGamesResponse{
		Games: gg,
	}, nil
}

// GameStatus sets the status of a game
// @Summary Set game's status
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) GameStatus(req GameStatusRequest) (GameStatusResponse, error) {
	var err error
	if req.Status {
		err = s.Games.Enable(req.id)
	} else {
		err = s.Games.Disable(req.id)
	}
	return GameStatusResponse{}, err
}

// MarketStatus sets the market status of a game
// @Summary Set game's status
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) MarketStatus(req MarketStatusRequest) (MarketStatusResponse, error) {
	var err error
	if req.Status {
		err = s.Games.Open(req.id)
	} else {
		err = s.Games.Close(req.id)
	}
	return MarketStatusResponse{}, err
}

// Game returns the game entity requested.
// @Summary Game details
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) Game(req GameRequest) (GameResponse, error) {
	return GameResponse{}, fmt.Errorf("not implemented")
}

// GameBranding returns just the game's branding informaton for the requested gameID
// @Summary Game's Branding
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) GameBranding(req GameBrandingRequest) (GameBrandingResponse, error) {
	return GameBrandingResponse{}, fmt.Errorf("not implemented")
}

// CreateGame creates a new game with optional branding information
// @Summary Create Game
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) CreateGame(req CreateGameRequest) (CreateGameResponse, error) {
	return CreateGameResponse{}, fmt.Errorf("not implemented")
}

// UpdateGame replaces a game with the new details
// @Summary Update Game
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) UpdateGame(req UpdateGameRequest) (UpdateGameResponse, error) {
	return UpdateGameResponse{}, fmt.Errorf("not implemented")
}

// UpdateBranding replaces a game branding with the new details
// @Summary Update Game's Branding
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) UpdateBranding(req UpdateBrandingRequest) (UpdateBrandingResponse, error) {
	return UpdateBrandingResponse{}, fmt.Errorf("not implemented")
}

// Term queries a term by a given ID
// @Summary Query Term
// @Param id path string true "Game ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) Term(req TermRequest) (TermResponse, error) {
	return TermResponse{}, fmt.Errorf("not implemented")
}

// TermForAction returns all the terms required for a given action
// @Summary Terms for Action
// @Param action path string true "Action" "register"
func (s Service) TermForAction(req TermForActionRequest) (TermForActionResponse, error) {
	return TermForActionResponse{}, fmt.Errorf("not implemented")
}

// AcceptedTerms returns all the terms required for a given action
// @Summary Accepted Terms
// @Param user_ID path string true "User ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) AcceptedTerms(req AcceptedTermsRequest) (AcceptedTermsResponse, error) {
	return AcceptedTermsResponse{}, fmt.Errorf("not implemented")
}

// AcceptTerms returns all the terms required for a given action
// @Summary Accept Terms
// @Param user_ID path string true "User ID" "01ENA97TRFH9CHYD3M6Q4CWGHY"
func (s Service) AcceptTerms(req AcceptTermsRequest) (AcceptTermsResponse, error) {
	return AcceptTermsResponse{}, fmt.Errorf("not implemented")
}
