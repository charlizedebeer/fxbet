//go:generate go run internal/httpgen.go

package http

import (
	"crypto/tls"
	"net/http"
	"os"
	"path/filepath"
	"stream/fxbet/pkg/server"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/acme/autocert"
)

// Server runs a normal Server server without any SSL.
type Server struct {
	FXBet        server.Service
	Router       *mux.Router
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	IdleTimeout  time.Duration
}

// SSLServer runs a SSLServer only server where all HTTP traffic will be forwarded to HTTPS.
// Certificates are handled by LetsEncrypt.
type SSLServer struct {
	Server
	CertsPath    string
	SupportEmail string
	HostNames    []string
}

// ListenAndServe starts listening for connections
func (s SSLServer) ListenAndServe() error {
	// Good practise, enforce HTTP timeouts
	if s.ReadTimeout == 0 {
		s.ReadTimeout = 60 * time.Second
	}
	if s.WriteTimeout == 0 {
		s.WriteTimeout = 60 * time.Second
	}
	if s.IdleTimeout == 0 {
		s.IdleTimeout = 120 * time.Second
	}
	if s.CertsPath == "" {
		s.CertsPath = "/certs"
	}

	certManager := autocert.Manager{
		Prompt:      autocert.AcceptTOS,
		Cache:       autocert.DirCache(s.CertsPath),
		HostPolicy:  autocert.HostWhitelist(s.HostNames...),
		RenewBefore: 0, // If zero, they're renewed 30 days before expiration.
		Email:       s.SupportEmail,
	}

	// Setup and run HTTPS server
	srv := &http.Server{
		Handler:      s.Router,
		Addr:         ":443",
		ReadTimeout:  s.ReadTimeout,
		WriteTimeout: s.WriteTimeout,
		IdleTimeout:  s.IdleTimeout,
		TLSConfig: &tls.Config{
			GetCertificate: certManager.GetCertificate,
			// Causes servers to use Go's default ciphersuite preferences,
			// which are tuned to avoid attacks. Does nothing on clients.
			PreferServerCipherSuites: true,
			// Only use curves which have assembly implementations
			CurvePreferences: []tls.CurveID{
				tls.CurveP256,
				tls.X25519,
			},
			MinVersion: tls.VersionTLS12,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,

				// Best disabled, as they don't provide Forward Secrecy,
				// but might be necessary for some clients
				// tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				// tls.TLS_RSA_WITH_AES_128_GCM_SHA256,
			},
		},
	}

	go http.ListenAndServe(":80", certManager.HTTPHandler(nil))

	return srv.ListenAndServeTLS("", "")
}

// ListenAndServe starts listening for connections
func (s Server) ListenAndServe(port string) error {
	// Good practise, enforce HTTP timeouts
	if s.ReadTimeout == 0 {
		s.ReadTimeout = 60 * time.Second
	}
	if s.WriteTimeout == 0 {
		s.WriteTimeout = 60 * time.Second
	}
	if s.IdleTimeout == 0 {
		s.IdleTimeout = 120 * time.Second
	}

	// Setup and run HTTPS server
	srv := &http.Server{
		Handler:      s.Router,
		Addr:         port,
		ReadTimeout:  s.ReadTimeout,
		WriteTimeout: s.WriteTimeout,
		IdleTimeout:  s.IdleTimeout}

	return srv.ListenAndServe()
}

// SPAHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type SPAHandler struct {
	StaticPath string
	IndexPath  string
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h SPAHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.StaticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.StaticPath, h.IndexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.StaticPath)).ServeHTTP(w, r)
}
