// Generated do not edit

package http

import (
	"encoding/json"
	"log"
	"net/http"
	"stream/fxbet/pkg/errors"
	"stream/fxbet/pkg/server"

	"github.com/gorilla/mux"
)

// BindRoutes attaches all handlers to a subrouter to better control middleware.
func (s Server) BindRoutes(path string, router *mux.Router) {
	r := router.PathPrefix(path).Subrouter()
	r.Use(corsAndPreflightMiddleWare)

	r.HandleFunc("/help", apiDocs).Methods(http.MethodGet, http.MethodOptions)
	r.HandleFunc("/ping", s.pong).Methods(http.MethodGet, http.MethodOptions)

	r.HandleFunc("/user/{user_id}/terms/accept", s.acceptTermsHandler).Methods("OPTIONS", "POST")
	r.HandleFunc("/user/{user_id}/terms/accept", s.acceptedTermsHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/games", s.allGamesHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/games/with-branding", s.allGamesWithBrandingHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/user/auth", s.authenticateUserHandler).Methods("OPTIONS", "POST")
	r.HandleFunc("/user/verifications", s.completeRegistrationHandler).Methods("OPTIONS", "POST")
	r.HandleFunc("/games", s.createGameHandler).Methods("OPTIONS", "POST")
	r.HandleFunc("/game/{id}", s.gameHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/game/{id}/branding", s.gameBrandingHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/game/{id}/enabled", s.gameStatusHandler).Methods("OPTIONS", "PUT")
	r.HandleFunc("/game/{id}/open", s.marketStatusHandler).Methods("OPTIONS", "PUT")
	r.HandleFunc("/user/registrations", s.registerUserHandler).Methods("OPTIONS", "POST")
	r.HandleFunc("/terms/{id}", s.termHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/terms/by-action/{action}", s.termForActionHandler).Methods("OPTIONS", "GET")
	r.HandleFunc("/game/{id}/branding", s.updateBrandingHandler).Methods("OPTIONS", "PUT")
	r.HandleFunc("/game/{id}", s.updateGameHandler).Methods("OPTIONS", "PUT")
}

func corsAndPreflightMiddleWare(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if origins, ok := r.Header["Origin"]; ok {
			if len(origins) > 0 {
				w.Header().Set("Access-Control-Allow-Origin", origins[0])
			}
		}
		w.Header().Set("Access-Control-Allow-Methods", "POST,GET,PUT,OPTIONS,DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Accept-Language, Authorization")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (s Server) pong(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("PONG"))
}

func apiDocs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "cmd/docs/api.html")
}

// @Tags User
// @Param request body server.AcceptTermsRequest true "AcceptTermsRequest is the input to accept terms for a given user"
// @Success 200 object server.AcceptTermsResponse "AcceptTermsResponse is the success output for accepting terms for a given user."
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/user/{user_id}/terms/accept [POST]
// @Produce json
func (s Server) acceptTermsHandler(w http.ResponseWriter, r *http.Request) {
	var req server.AcceptTermsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.AcceptTerms(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags User
// @Param request body server.AcceptedTermsRequest true "AcceptedTermsRequest is the input to query a user's accepted terms"
// @Success 200 object server.AcceptedTermsResponse "AcceptedTermsResponse is the success output for querying a user's accepted terms"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/user/{user_id}/terms/accept [GET]
// @Produce json
func (s Server) acceptedTermsHandler(w http.ResponseWriter, r *http.Request) {
	var req server.AcceptedTermsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.AcceptedTerms(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Games
// @Param request body server.AllGamesRequest true "AllGamesRequest is the input to a Games request"
// @Success 200 object server.AllGamesResponse "AllGamesResponse is the success response from a Game request"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/games [GET]
// @Produce json
func (s Server) allGamesHandler(w http.ResponseWriter, r *http.Request) {
	var req server.AllGamesRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.AllGames(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Games
// @Param request body server.AllGamesWithBrandingRequest true "AllGamesWithBrandingRequest is the input to a Games request"
// @Success 200 object server.AllGamesWithBrandingResponse "AllGamesWithBrandingResponse is the success response from a Game request"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/games/with-branding [GET]
// @Produce json
func (s Server) allGamesWithBrandingHandler(w http.ResponseWriter, r *http.Request) {
	var req server.AllGamesWithBrandingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.AllGamesWithBranding(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags User
// @Param request body server.AuthenticateUserRequest true "AuthenticateUserRequest is the input to AuthenticateUser"
// @Success 200 object server.AuthenticateUserResponse "AuthenticateUserResponse is the success response from AuthenticateUser"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/user/auth [POST]
// @Produce json
func (s Server) authenticateUserHandler(w http.ResponseWriter, r *http.Request) {
	var req server.AuthenticateUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.AuthenticateUser(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags User
// @Param request body server.CompleteRegistrationRequest true "CompleteRegistrationRequest is the input to CompleteRegistration"
// @Success 200 object server.CompleteRegistrationResponse "CompleteRegistrationResponse is the success response from CompleteRegistration"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/user/verifications [POST]
// @Produce json
func (s Server) completeRegistrationHandler(w http.ResponseWriter, r *http.Request) {
	var req server.CompleteRegistrationRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.CompleteRegistration(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Games
// @Param request body server.CreateGameRequest true "CreateGameRequest is the input to creating a new game"
// @Success 200 object server.CreateGameResponse "CreateGameResponse is the success response from creating a game"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/games [POST]
// @Produce json
func (s Server) createGameHandler(w http.ResponseWriter, r *http.Request) {
	var req server.CreateGameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.CreateGame(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.GameRequest true "GameRequest is the input to fetching game information."
// @Success 200 object server.GameResponse "GameResponse is the success response from fetching game information"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id} [GET]
// @Produce json
func (s Server) gameHandler(w http.ResponseWriter, r *http.Request) {
	var req server.GameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.Game(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.GameBrandingRequest true "GameBrandingRequest is the input to fetching a game's branding"
// @Success 200 object server.GameBrandingResponse "GameBrandingResponse is the success response from fetching a game's branding"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id}/branding [GET]
// @Produce json
func (s Server) gameBrandingHandler(w http.ResponseWriter, r *http.Request) {
	var req server.GameBrandingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.GameBranding(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.GameStatusRequest true "GameStatusRequest is the input to changing a game's status"
// @Success 200 object server.GameStatusResponse "GameStatusResponse is the success response from changing a game status"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id}/enabled [PUT]
// @Produce json
func (s Server) gameStatusHandler(w http.ResponseWriter, r *http.Request) {
	var req server.GameStatusRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.GameStatus(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.MarketStatusRequest true "MarketStatusRequest is the input to changing a game's status"
// @Success 200 object server.MarketStatusResponse "MarketStatusResponse is the success response from changing a game status"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id}/open [PUT]
// @Produce json
func (s Server) marketStatusHandler(w http.ResponseWriter, r *http.Request) {
	var req server.MarketStatusRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.MarketStatus(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags User
// @Param request body server.RegisterUserRequest true "RegisterUserRequest is the input to the RegisterUser"
// @Success 200 object server.RegisterUserResponse "RegisterUserResponse is the success response from RegisterUser"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/user/registrations [POST]
// @Produce json
func (s Server) registerUserHandler(w http.ResponseWriter, r *http.Request) {
	var req server.RegisterUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	res, err := s.FXBet.RegisterUser(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Terms
// @Param request body server.TermRequest true "TermRequest is the input to query a term by ID"
// @Success 200 object server.TermResponse "TermResponse is the success response to querying a term by ID"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/terms/{id} [GET]
// @Produce json
func (s Server) termHandler(w http.ResponseWriter, r *http.Request) {
	var req server.TermRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.Term(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Terms
// @Param request body server.TermForActionRequest true "TermForActionRequest is the input to query terms for a given action"
// @Success 200 object server.TermForActionResponse "TermForActionResponse is the success output for querying terms for a given action"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/terms/by-action/{action} [GET]
// @Produce json
func (s Server) termForActionHandler(w http.ResponseWriter, r *http.Request) {
	var req server.TermForActionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.TermForAction(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.UpdateBrandingRequest true "UpdateBrandingRequest is the input to fetching a game's branding"
// @Success 200 object server.UpdateBrandingResponse "UpdateBrandingResponse is the success response from fetching a game's branding"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id}/branding [PUT]
// @Produce json
func (s Server) updateBrandingHandler(w http.ResponseWriter, r *http.Request) {
	var req server.UpdateBrandingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.UpdateBranding(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}

// @Tags Game
// @Param request body server.UpdateGameRequest true "UpdateGameRequest is the input to update a game's"
// @Success 200 object server.UpdateGameResponse "UpdateGameResponse is the success response from fetching a game's branding"
// @Failure 400 object errors.Detailed "Request was refused with error code"
// @Failure 500 object string "Unexpected server or infrastructure failure"
// @Router /api/game/{id} [PUT]
// @Produce json
func (s Server) updateGameHandler(w http.ResponseWriter, r *http.Request) {
	var req server.UpdateGameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: err.Error()})
		return
	}
	req.ParseVars(mux.Vars(r))
	res, err := s.FXBet.UpdateGame(req)
	if errorCode, ok := err.(errors.Coded); ok {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errorCode)
		return
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		json.NewEncoder(w).Encode(errors.Detailed{Code: 0, Detail: "Unexpected Error"})
		return
	}
	json.NewEncoder(w).Encode(res)
}
