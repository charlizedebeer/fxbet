package main

import (
	"bytes"
	"fmt"
	"go/ast"
	goparser "go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strings"
	"text/template"
	"unicode"

	"golang.org/x/tools/imports"
)

type endpoint struct {
	Name            string
	Description     string
	Summary         string
	PathParam       string
	Path            string
	Method          string
	RequestComment  string
	ResponseComment string
}

func (e endpoint) HandlerName() string {
	a := []rune(e.Name)
	a[0] = unicode.ToLower(a[0])
	return string(a) + "Handler"
}

func (e endpoint) Tag() string {
	ss := strings.Split(e.Path, "/")
	if len(ss) < 2 {
		return ""
	}
	return strings.Title(ss[1])
}

func main() {
	p := parser{
		endpoints: make(map[string]endpoint),
	}

	fs := token.NewFileSet()
	pp, err := goparser.ParseDir(fs, "../", nil, goparser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}
	for _, astPackage := range pp {
		for _, f := range astPackage.Files {
			ast.Walk(&p, f)
		}
	}

	fmt.Println("Generating http handlers:")
	names := []string{}
	for name := range p.endpoints {
		names = append(names, name)
	}
	p.Endpoints = make([]endpoint, 0, len(names))
	sort.Strings(names)
	for _, n := range names {
		p.Endpoints = append(p.Endpoints, p.endpoints[n])
		fmt.Printf("%v: %v [%v]\n", p.endpoints[n].Name, p.endpoints[n].Path, p.endpoints[n].Method)
	}
	err = p.generateHandlers("http-handlers-generated.go")
	if err != nil {
		fmt.Println(err)
	}
}

func getEndPointName(s string) string {
	n := strings.Replace(s, "POST", "", 1)
	n = strings.Replace(n, "GET", "", 1)
	return n
}

func getEndPointMethod(s string) string {
	if strings.HasPrefix(s, "POST") {
		return http.MethodPost
	}
	if strings.HasPrefix(s, "GET") {
		return http.MethodGet
	}
	return ""
}

func getStructName(s string) (string, string) {
	ss := strings.Split(s, " ")
	if len(ss) < 1 {
		return s, ""
	}
	if strings.HasSuffix(ss[0], "Request") {
		return strings.Replace(ss[0], "Request", "", 1), "Request"
	}
	if strings.HasSuffix(ss[0], "Response") {
		return strings.Replace(ss[0], "Response", "", 1), "Response"
	}
	return s, ""
}

type parser struct {
	endpoints map[string]endpoint
	Endpoints []endpoint
}

func (p *parser) Visit(n ast.Node) ast.Visitor {
	if n == nil {
		return nil
	}

	switch d := n.(type) {
	case *ast.ValueSpec:
		if len(d.Values) <= 0 || len(d.Names) <= 0 {
			return p
		}
		basicLit, ok := d.Values[0].(*ast.BasicLit)
		if !ok {
			return p
		}
		if !strings.HasPrefix(d.Names[0].Name, "POST") && !strings.HasPrefix(d.Names[0].Name, "GET") {
			return p
		}
		e := endpoint{
			Name:   getEndPointName(d.Names[0].Name),
			Path:   strings.Trim(basicLit.Value, `" `),
			Method: getEndPointMethod(d.Names[0].Name),
		}
		p.endpoints[e.Name] = e
	case *ast.CommentGroup:
		comment := strings.TrimRight(d.Text(), "\n\r")
		fn, r := getStructName(comment)
		endpoint, ok := p.endpoints[fn]
		if ok {
			if r == "Request" {
				endpoint.RequestComment = comment
			}
			if r == "Response" {
				endpoint.ResponseComment = comment
			}
			p.endpoints[fn] = endpoint
		}

	case *ast.FuncDecl:
		p.checkFuncIsParseVars(d)
		p.checkEndpointIsDocumented(d)
	}

	return p
}

func (p *endpointParser) checkFuncIsParseVars(fd *ast.FuncDecl) {
	if fd.Name.Name != "ParseVars" {
		return
	}
	if fd.Recv.List == nil && len(fd.Recv.List) <= 0 {
		return
	}
	var fn string
	switch recvType := fd.Recv.List[0].Type.(type) {
	case *ast.StarExpr:
		if ident, ok := recvType.X.(*ast.Ident); ok {
			fn = getFuncNameFromRequestReponse(ident.Name)
		}
	default:
		fmt.Println("ParseVars error: unexpected type")
	}
	endpoint, ok := p.parsedEndpoints[fn]
	if !ok {
		fmt.Printf("ParseVars error: could not find server function %v\n", fn)
		return
	}
	fmt.Printf("%v needs vars\n", fn)
	endpoint.NeedVars = true
	p.parsedEndpoints[fn] = endpoint
}

func (p *endpointParser) checkEndpointIsDocumented(fd *ast.FuncDecl) {
	endpoint, ok := p.parsedEndpoints[fd.Name.Name]
	if !ok {
		return
	}
	var description string
	var summary string
	var param string
	if fd.Doc != nil && fd.Doc.List != nil {
		for _, c := range fd.Doc.List {
			comment := strings.TrimSpace(strings.Replace(c.Text, "//", "", 1))
			if strings.HasPrefix(comment, "@Summary") {
				summary = "// " + comment
				continue
			}
			if strings.HasPrefix(comment, "@Param") {
				param = "// " + comment
				continue
			}
			description += fmt.Sprintf("// @Description %s\n", comment)
		}
	}
	endpoint.Description = strings.TrimRight(description, "\n")
	endpoint.Summary = summary
	endpoint.PathParam = param
	p.parsedEndpoints[fd.Name.Name] = endpoint
}

func (p *parser) generateHandlers(fn string) error {
	handlerTemplate, err := ioutil.ReadFile("internal/http-handler.go.tmpl")
	if err != nil {
		return err
	}
	tmpl := template.Must(template.New("server").Parse(string(handlerTemplate)))
	buf := bytes.NewBuffer([]byte{})
	err = tmpl.Execute(buf, p)
	if err != nil {
		return err
	}
	res, err := imports.Process(fn, buf.Bytes(), nil)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(fn, res, 0666)
}
