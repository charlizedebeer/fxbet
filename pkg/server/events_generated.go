// Code generated by the FlatBuffers compiler. DO NOT EDIT.

package server

import (
	"strconv"

	flatbuffers "github.com/google/flatbuffers/go"
)

type EvtType uint16

const (
	EvtTypeEvtSignUpCreated EvtType = 1000
)

var EnumNamesEvtType = map[EvtType]string{
	EvtTypeEvtSignUpCreated: "EvtSignUpCreated",
}

var EnumValuesEvtType = map[string]EvtType{
	"EvtSignUpCreated": EvtTypeEvtSignUpCreated,
}

func (v EvtType) String() string {
	if s, ok := EnumNamesEvtType[v]; ok {
		return s
	}
	return "EvtType(" + strconv.FormatInt(int64(v), 10) + ")"
}

type Gender byte

const (
	GenderGenderMale   Gender = 0
	GenderGenderFemale Gender = 1
	GenderGenderOther  Gender = 2
)

var EnumNamesGender = map[Gender]string{
	GenderGenderMale:   "GenderMale",
	GenderGenderFemale: "GenderFemale",
	GenderGenderOther:  "GenderOther",
}

var EnumValuesGender = map[string]Gender{
	"GenderMale":   GenderGenderMale,
	"GenderFemale": GenderGenderFemale,
	"GenderOther":  GenderGenderOther,
}

func (v Gender) String() string {
	if s, ok := EnumNamesGender[v]; ok {
		return s
	}
	return "Gender(" + strconv.FormatInt(int64(v), 10) + ")"
}

type EvtSignUpCreatedT struct {
	ID string
	FirstName string
	MiddleNames string
	LastName string
	Prefix string
	Suffix string
	Email string
	Country string
	DateOfBirth int64
	Gender Gender
	AcceptedTermIDs []string
}

func (t *EvtSignUpCreatedT) Pack(builder *flatbuffers.Builder) flatbuffers.UOffsetT {
	if t == nil { return 0 }
	IDOffset := builder.CreateString(t.ID)
	FirstNameOffset := builder.CreateString(t.FirstName)
	MiddleNamesOffset := builder.CreateString(t.MiddleNames)
	LastNameOffset := builder.CreateString(t.LastName)
	PrefixOffset := builder.CreateString(t.Prefix)
	SuffixOffset := builder.CreateString(t.Suffix)
	EmailOffset := builder.CreateString(t.Email)
	CountryOffset := builder.CreateString(t.Country)
	AcceptedTermIDsOffset := flatbuffers.UOffsetT(0)
	if t.AcceptedTermIDs != nil {
		AcceptedTermIDsLength := len(t.AcceptedTermIDs)
		AcceptedTermIDsOffsets := make([]flatbuffers.UOffsetT, AcceptedTermIDsLength)
		for j := 0; j < AcceptedTermIDsLength; j++ {
			AcceptedTermIDsOffsets[j] = builder.CreateString(t.AcceptedTermIDs[j])
		}
		EvtSignUpCreatedStartAcceptedTermIDsVector(builder, AcceptedTermIDsLength)
		for j := AcceptedTermIDsLength - 1; j >= 0; j-- {
			builder.PrependUOffsetT(AcceptedTermIDsOffsets[j])
		}
		AcceptedTermIDsOffset = builder.EndVector(AcceptedTermIDsLength)
	}
	EvtSignUpCreatedStart(builder)
	EvtSignUpCreatedAddID(builder, IDOffset)
	EvtSignUpCreatedAddFirstName(builder, FirstNameOffset)
	EvtSignUpCreatedAddMiddleNames(builder, MiddleNamesOffset)
	EvtSignUpCreatedAddLastName(builder, LastNameOffset)
	EvtSignUpCreatedAddPrefix(builder, PrefixOffset)
	EvtSignUpCreatedAddSuffix(builder, SuffixOffset)
	EvtSignUpCreatedAddEmail(builder, EmailOffset)
	EvtSignUpCreatedAddCountry(builder, CountryOffset)
	EvtSignUpCreatedAddDateOfBirth(builder, t.DateOfBirth)
	EvtSignUpCreatedAddGender(builder, t.Gender)
	EvtSignUpCreatedAddAcceptedTermIDs(builder, AcceptedTermIDsOffset)
	return EvtSignUpCreatedEnd(builder)
}

func (rcv *EvtSignUpCreated) UnPackTo(t *EvtSignUpCreatedT) {
	t.ID = string(rcv.ID())
	t.FirstName = string(rcv.FirstName())
	t.MiddleNames = string(rcv.MiddleNames())
	t.LastName = string(rcv.LastName())
	t.Prefix = string(rcv.Prefix())
	t.Suffix = string(rcv.Suffix())
	t.Email = string(rcv.Email())
	t.Country = string(rcv.Country())
	t.DateOfBirth = rcv.DateOfBirth()
	t.Gender = rcv.Gender()
	AcceptedTermIDsLength := rcv.AcceptedTermIDsLength()
	t.AcceptedTermIDs = make([]string, AcceptedTermIDsLength)
	for j := 0; j < AcceptedTermIDsLength; j++ {
		t.AcceptedTermIDs[j] = string(rcv.AcceptedTermIDs(j))
	}
}

func (rcv *EvtSignUpCreated) UnPack() *EvtSignUpCreatedT {
	if rcv == nil { return nil }
	t := &EvtSignUpCreatedT{}
	rcv.UnPackTo(t)
	return t
}

type EvtSignUpCreated struct {
	_tab flatbuffers.Table
}

func GetRootAsEvtSignUpCreated(buf []byte, offset flatbuffers.UOffsetT) *EvtSignUpCreated {
	n := flatbuffers.GetUOffsetT(buf[offset:])
	x := &EvtSignUpCreated{}
	x.Init(buf, n+offset)
	return x
}

func GetSizePrefixedRootAsEvtSignUpCreated(buf []byte, offset flatbuffers.UOffsetT) *EvtSignUpCreated {
	n := flatbuffers.GetUOffsetT(buf[offset+flatbuffers.SizeUint32:])
	x := &EvtSignUpCreated{}
	x.Init(buf, n+offset+flatbuffers.SizeUint32)
	return x
}

func (rcv *EvtSignUpCreated) Init(buf []byte, i flatbuffers.UOffsetT) {
	rcv._tab.Bytes = buf
	rcv._tab.Pos = i
}

func (rcv *EvtSignUpCreated) Table() flatbuffers.Table {
	return rcv._tab
}

func (rcv *EvtSignUpCreated) ID() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(4))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) FirstName() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(6))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) MiddleNames() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(8))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) LastName() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(10))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) Prefix() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(12))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) Suffix() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(14))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) Email() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(16))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) Country() []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(18))
	if o != 0 {
		return rcv._tab.ByteVector(o + rcv._tab.Pos)
	}
	return nil
}

func (rcv *EvtSignUpCreated) DateOfBirth() int64 {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(20))
	if o != 0 {
		return rcv._tab.GetInt64(o + rcv._tab.Pos)
	}
	return 0
}

func (rcv *EvtSignUpCreated) MutateDateOfBirth(n int64) bool {
	return rcv._tab.MutateInt64Slot(20, n)
}

func (rcv *EvtSignUpCreated) Gender() Gender {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(22))
	if o != 0 {
		return Gender(rcv._tab.GetByte(o + rcv._tab.Pos))
	}
	return 0
}

func (rcv *EvtSignUpCreated) MutateGender(n Gender) bool {
	return rcv._tab.MutateByteSlot(22, byte(n))
}

func (rcv *EvtSignUpCreated) AcceptedTermIDs(j int) []byte {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(24))
	if o != 0 {
		a := rcv._tab.Vector(o)
		return rcv._tab.ByteVector(a + flatbuffers.UOffsetT(j*4))
	}
	return nil
}

func (rcv *EvtSignUpCreated) AcceptedTermIDsLength() int {
	o := flatbuffers.UOffsetT(rcv._tab.Offset(24))
	if o != 0 {
		return rcv._tab.VectorLen(o)
	}
	return 0
}

func EvtSignUpCreatedStart(builder *flatbuffers.Builder) {
	builder.StartObject(11)
}
func EvtSignUpCreatedAddID(builder *flatbuffers.Builder, ID flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(0, flatbuffers.UOffsetT(ID), 0)
}
func EvtSignUpCreatedAddFirstName(builder *flatbuffers.Builder, FirstName flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(1, flatbuffers.UOffsetT(FirstName), 0)
}
func EvtSignUpCreatedAddMiddleNames(builder *flatbuffers.Builder, MiddleNames flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(2, flatbuffers.UOffsetT(MiddleNames), 0)
}
func EvtSignUpCreatedAddLastName(builder *flatbuffers.Builder, LastName flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(3, flatbuffers.UOffsetT(LastName), 0)
}
func EvtSignUpCreatedAddPrefix(builder *flatbuffers.Builder, Prefix flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(4, flatbuffers.UOffsetT(Prefix), 0)
}
func EvtSignUpCreatedAddSuffix(builder *flatbuffers.Builder, Suffix flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(5, flatbuffers.UOffsetT(Suffix), 0)
}
func EvtSignUpCreatedAddEmail(builder *flatbuffers.Builder, Email flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(6, flatbuffers.UOffsetT(Email), 0)
}
func EvtSignUpCreatedAddCountry(builder *flatbuffers.Builder, Country flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(7, flatbuffers.UOffsetT(Country), 0)
}
func EvtSignUpCreatedAddDateOfBirth(builder *flatbuffers.Builder, DateOfBirth int64) {
	builder.PrependInt64Slot(8, DateOfBirth, 0)
}
func EvtSignUpCreatedAddGender(builder *flatbuffers.Builder, Gender Gender) {
	builder.PrependByteSlot(9, byte(Gender), 0)
}
func EvtSignUpCreatedAddAcceptedTermIDs(builder *flatbuffers.Builder, AcceptedTermIDs flatbuffers.UOffsetT) {
	builder.PrependUOffsetTSlot(10, flatbuffers.UOffsetT(AcceptedTermIDs), 0)
}
func EvtSignUpCreatedStartAcceptedTermIDsVector(builder *flatbuffers.Builder, numElems int) flatbuffers.UOffsetT {
	return builder.StartVector(4, numElems, 4)
}
func EvtSignUpCreatedEnd(builder *flatbuffers.Builder) flatbuffers.UOffsetT {
	return builder.EndObject()
}
