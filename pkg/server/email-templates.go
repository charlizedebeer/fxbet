package server

// NotificationTemplate contains the required information to generate a notification to a user
type NotificationTemplate struct {
	ID string
	// THe address e.g. the from email or the from sms number
	FromAddress string
	// A human readable name for the sender e.g. Instanto
	FromName string
	// Optional subject of the notification e.g. email subject.  sms will not have a subject.
	Subject string
	// The body of the template with data variables specified in accordance with https://golang.org/pkg/text/template/
	Body string
	// The provider of the service to send the notification e.g. MailJet
	Provider string
	// Optionally the providers templateID.  Note for SMS MailJet does not do templatinbg so we create the body with our temnplating
	ExternalTemplateID interface{}
	// The Scheme e.g. mailto
	Scheme string
	// Specified the data fields required by the template
	// TODO improve to add validation to determine if data is required and well formed. We could create a template Data struct and use golang validation
	Data []string
}
