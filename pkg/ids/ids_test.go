package ids

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewID(t *testing.T) {
	a := NewID()
	b := NewID()
	assert.NotEqual(t, "", a)
	assert.NotEqual(t, "", b)
	assert.NotEqual(t, a, b)
}

func TestIsValidULID(t *testing.T) {
	a := NewID()
	b := "MADEUPID"
	assert.True(t, IsValidULID(a))
	assert.False(t, IsValidULID(b))
}

func TestShortIDs(t *testing.T) {
	a := NewID()
	s, err := ToShortID(a)
	b, err := FromShortID(s)

	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, a, b)
	assert.True(t, len(s) < len(a))
}

func BenchmarkShortIDs(b *testing.B) {
	minLen, maxLen := 0, 0
	for n := 0; n < b.N; n++ {
		in := NewID()
		short, err := ToShortID(in)
		out, err := FromShortID(short)

		// TODO: This messes up the benchmark time probably, but is useful quality check
		if err != nil {
			b.Fatal(err)
		}
		if in != out {
			b.Fatalf("ID from short ID does not match original: in=%s, short=%s, out=%s\n", in, short, out)
		}
		if len(short) >= len(in) {
			b.Fatalf("Short ID not shorter than input: in=%s, short=%s\n", in, short)
		}

		if minLen == 0 || len(short) < minLen {
			minLen = len(short)
		}
		if len(short) > maxLen {
			maxLen = len(short)
		}
	}
	fmt.Printf("In %d shortening, minLen=%d, maxLen=%d\n", b.N, minLen, maxLen)
}
