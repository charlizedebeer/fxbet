package ids

import (
	"fmt"
	"github.com/oklog/ulid/v2"
	"math"
	"math/big"
	"math/rand"
	"sort"
	"strings"
	"time"
)

// NewID produces a new, best-effort-unique identifier in raw bytes form, ready for
// use as entity or message identifier in microservices.
// Note: This is NOT safe for concurrent use. Microservices are ideally single-threaded
// using ZeroMQ, NodeJS-style.
func NewID() string {
	seed := time.Now().UnixNano()
	entropy := rand.New(rand.NewSource(seed))
	timestamp := ulid.Timestamp(time.Now())
	id := ulid.MustNew(timestamp, entropy)
	return id.String()
}

// parse this ulid to confirm it is a valid ulid
// returns false for invalid ids
func IsValidULID(id string) bool {
	_, err := ulid.Parse(id)
	if err != nil {
		return false
	}
	return true
}

// ToShortID takes a canonical ID produced with `NewID`, and converts
// it to a shorter representation, such that it is convertable back to
// its original form using `FromShortID`. Some attributes, e.g. lexicographic
// sort-ability in the case of ULIDs, may be lost on the short ID, but never
// uniqueness.
func ToShortID(id string) (string, error) {
	enc := &base57{newAlphabet(defaultAlphabet)} // TODO: Do once only, not every time
	u, err := ulid.Parse(id)
	if err != nil {
		return "", err
	}
	return enc.encode(u), nil
}

// FromShortID takes the output of `ToShortID` and returns it
// back to the original value provided as input.
func FromShortID(shortID string) (string, error) {
	enc := &base57{newAlphabet(defaultAlphabet)} // TODO: Do once only, not every time
	u, err := enc.decode(shortID)
	if err != nil {
		return "", err
	}
	return u.String(), nil
}

// ===== Support for base57 encoding of short IDs ====================

const defaultAlphabet = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

type alphabet struct {
	chars [57]string
	len   int64
}

func newAlphabet(s string) alphabet {
	abc := dedupe(strings.Split(s, ""))

	if len(abc) != 57 {
		panic("encoding alphabet is not 57-bytes long")
	}

	sort.Strings(abc)
	a := alphabet{
		len: int64(len(abc)),
	}
	copy(a.chars[:], abc)
	return a
}

func (a *alphabet) Length() int64 {
	return a.len
}

// Index returns the index of the first instance of t in the alphabet, or an error if t is not present.
func (a *alphabet) Index(t string) (int64, error) {
	for i, char := range a.chars {
		if char == t {
			return int64(i), nil
		}
	}
	return 0, fmt.Errorf("Element '%v' is not part of the alphabet", t)
}

// dudupe removes duplicate characters from s.
func dedupe(s []string) []string {
	var out []string
	m := make(map[string]bool)

	for _, char := range s {
		if _, ok := m[char]; !ok {
			m[char] = true
			out = append(out, char)
		}
	}

	return out
}

type base57 struct {
	// alphabet is the character set to construct the UUID from.
	alphabet alphabet
}

func (b base57) encode(u ulid.ULID) string {
	var num big.Int
	bytes, _ := u.MarshalBinary()
	num.SetBytes(bytes)

	// Calculate encoded length.
	factor := math.Log(float64(25)) / math.Log(float64(b.alphabet.Length()))
	length := math.Ceil(factor * float64(len(u)))

	return b.numToString(&num, int(length))
}

func (b base57) decode(u string) (id ulid.ULID, err error) {
	n := big.NewInt(0)

	for i := len(u) - 1; i >= 0; i-- {
		n.Mul(n, big.NewInt(b.alphabet.Length()))

		index, err := b.alphabet.Index(string(u[i]))
		if err != nil {
			return id, err
		}

		n.Add(n, big.NewInt(index))
	}

	err = id.UnmarshalBinary(n.Bytes())
	return
}

func (b *base57) numToString(number *big.Int, padToLen int) string {
	var (
		out   string
		digit *big.Int
	)

	for number.Uint64() > 0 {
		number, digit = new(big.Int).DivMod(number, big.NewInt(b.alphabet.Length()), new(big.Int))
		out += b.alphabet.chars[digit.Int64()]
	}

	if padToLen > 0 {
		remainder := math.Max(float64(padToLen-len(out)), 0)
		out = out + strings.Repeat(b.alphabet.chars[0], int(remainder))
	}

	return out
}
