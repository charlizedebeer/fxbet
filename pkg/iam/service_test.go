package iam

import (
	"reflect"
	"stream/fxbet/spatial/events/storage/memory"
	"testing"
)

func Test_CreateUser(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	want := User{
		ID:        user.ID,
		CreatedAt: user.CreatedAt,
		CredentialByStrategy: map[LoginStrategy]LoginCredential{
			LoginStrategyPassword: {
				Password:     user.CredentialByStrategy[LoginStrategyPassword].Password,
				IDInStrategy: "test@fxbet.co",
			},
		},
	}
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user.ID == "" {
		t.Fatalf("wanted valid ID, got blank")
	}
	if user.CredentialByStrategy[LoginStrategyPassword].Password == "" {
		t.Fatalf("wanted hashed password, got blank")
	}
	if !reflect.DeepEqual(want, user) {
		t.Fatalf("\n  wanted: %v\n  got:    %v", want, user)
	}
}

func Test_EnableUser(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user.Enabled {
		t.Fatalf("user should be disabled when newly created")
	}

	err = s.EnableUser(user.ID)
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user, ok := s.userByID[user.ID]; ok && !user.Enabled {
		t.Fatalf("want: %v, got: %v", true, user.Enabled)
	}
}

func Test_DisableUser(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user.Enabled {
		t.Fatalf("user should be disabled when newly created")
	}

	err = s.EnableUser(user.ID)
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user, ok := s.userByID[user.ID]; ok && !user.Enabled {
		t.Fatalf("want: %v, got: %v", true, user.Enabled)
	}

	err = s.DisableUser(user.ID)
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user, ok := s.userByID[user.ID]; ok && user.Enabled {
		t.Fatalf("want: %v, got: %v", false, user.Enabled)
	}
}

func Test_AddRole(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if roles, ok := s.rolesByUserID[user.ID]; ok && !reflect.DeepEqual([]ContextRole{}, roles) {
		t.Fatalf("want: %v, got: %v", []ContextRole{}, roles)
	}

	role := ContextRole{Role: "some-role", ContextID: "context-id", ContextType: "context-type"}
	err = s.AddRole(user.ID, role)
	if err != nil {
		t.Fatalf("adding role, %v", err)
	}
	if roles, ok := s.rolesByUserID[user.ID]; ok && !reflect.DeepEqual([]ContextRole{role}, roles) {
		t.Fatalf("want: %v, got %v", []ContextRole{role}, roles)
	}
}

func Test_RemoveRole(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	want := []ContextRole{
		{Role: "some-role1", ContextID: "context-id1", ContextType: "context-type1"},
		{Role: "some-role2", ContextID: "context-id2", ContextType: "context-type2"},
		{Role: "some-role3", ContextID: "context-id3", ContextType: "context-type3"},
	}
	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	for _, w := range want {
		s.AddRole(user.ID, w)
	}
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if roles, ok := s.rolesByUserID[user.ID]; ok && !reflect.DeepEqual(want, roles) {
		t.Fatalf("want: %v, got: %v", want, roles)
	}

	role := ContextRole{Role: "some-role2", ContextID: "context-id2", ContextType: "context-type2"}
	err = s.RemoveRole(user.ID, role)
	if err != nil {
		t.Fatalf("adding role, %v", err)
	}
	want = []ContextRole{
		{Role: "some-role1", ContextID: "context-id1", ContextType: "context-type1"},
		{Role: "some-role3", ContextID: "context-id3", ContextType: "context-type3"},
	}
	if roles, ok := s.rolesByUserID[user.ID]; ok && !reflect.DeepEqual(want, roles) {
		t.Fatalf("want: %v, got: %v", want, roles)
	}
}

func Test_UserByStrategyAndIDInStrategy(t *testing.T) {
	s := New(memory.NewEventStorage("iam"))

	go s.Run()
	defer s.Shutdown()

	user, err := s.CreateUser(LoginStrategyPassword, "test@fxbet.co", "SomePassword")
	if err != nil {
		t.Fatalf("creating user, %v", err)
	}
	if user.Enabled {
		t.Fatalf("user should be disabled when newly created")
	}

	u, found := s.UserByStrategyAndIDInStrategy(LoginStrategyPassword, "test@fxbet.co")
	if !found {
		t.Fatalf("user not found")
	}
	if !reflect.DeepEqual(u.ID, user.ID) {
		t.Fatalf("want: %v, got: %v", user, u)
	}
}
