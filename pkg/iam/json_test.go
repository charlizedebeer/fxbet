package iam

import (
	"reflect"
	"testing"
	"time"
)

func TestEmail_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    Email
		wantErr bool
	}{
		{name: "lowercase", args: args{b: []byte(`"TEST@SOMEEMAIL.COM"`)}, want: Email("test@someemail.com"), wantErr: false},
		{name: "trimmed", args: args{b: []byte(`"  TEST@SOMEEMAIL.COM  "`)}, want: Email("test@someemail.com"), wantErr: false},
		{name: "valid format", args: args{b: []byte(`"@SOMEEMAIL.COM"`)}, want: Email(""), wantErr: true},
		{name: "valid format", args: args{b: []byte(`"test@"`)}, want: Email(""), wantErr: true},
		{name: "valid format", args: args{b: []byte(`"test44$@gmail.com"`)}, want: Email(""), wantErr: true},
		{name: "valid format", args: args{b: []byte(`"test-email.com"`)}, want: Email(""), wantErr: true},
		{name: "valid format", args: args{b: []byte(`"bad-email"`)}, want: Email(""), wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Email
			err := got.UnmarshalJSON(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("Email.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil && got != tt.want {
				t.Errorf("Email.UnmarshalJSON() got %v, wanted %v", got, tt.want)
			}
		})
	}
}

func TestDateOfBirth_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	epoch := time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name    string
		args    args
		want    DateOfBirth
		wantErr bool
	}{
		{name: "2 January 2006", args: args{b: []byte(`"2006-01-02"`)}, want: DateOfBirth(time.Date(2006, time.January, 2, 0, 0, 0, 0, time.UTC)), wantErr: false},
		{name: "invalid year", args: args{b: []byte(`"99-01-02"`)}, want: DateOfBirth(epoch), wantErr: true},
		{name: "invalid month", args: args{b: []byte(`"2000-13-02"`)}, want: DateOfBirth(epoch), wantErr: true},
		{name: "invalid day", args: args{b: []byte(`"2000-01-62"`)}, want: DateOfBirth(epoch), wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got DateOfBirth
			err := got.UnmarshalJSON(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("Email.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil && got != tt.want {
				t.Errorf("Email.UnmarshalJSON() got %v, wanted %v", got, tt.want)
			}
		})
	}
}

func TestCountry_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    Country
		wantErr bool
	}{
		{name: "south africa", args: args{b: []byte(`"ZA"`)}, want: Country("ZA"), wantErr: false},
		{name: "UK", args: args{b: []byte(`"UK"`)}, want: Country("UK"), wantErr: false},
		{name: "more than 2", args: args{b: []byte(`"ASD"`)}, want: Country(""), wantErr: true},
		{name: "less than 2", args: args{b: []byte(`"A"`)}, want: Country(""), wantErr: true},
		{name: "invalid", args: args{b: []byte(`""`)}, want: Country(""), wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Country
			err := got.UnmarshalJSON(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("Email.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil && got != tt.want {
				t.Errorf("Email.UnmarshalJSON() got %v, wanted %v", got, tt.want)
			}
		})
	}
}

func TestName_UnmarshalJSON(t *testing.T) {
	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		args    args
		want    Name
		wantErr bool
	}{
		{name: "ok", args: args{b: []byte(`{"first":"First","last":"Last"}`)}, want: Name{FirstName: "First", LastName: "Last"}, wantErr: false},
		{name: "no blank first", args: args{b: []byte(`{"first":"","last":"Last"}`)}, want: Name{FirstName: "", LastName: "Last"}, wantErr: true},
		{name: "no blank last", args: args{b: []byte(`{"first":"First","last":""}`)}, want: Name{FirstName: "First", LastName: ""}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Name
			err := got.UnmarshalJSON(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("Email.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil && !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Email.UnmarshalJSON() got %v, wanted %v", got, tt.want)
			}
		})
	}
}
