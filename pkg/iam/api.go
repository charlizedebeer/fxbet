package iam

import (
	"fmt"
	"stream/fxbet/pkg/errors"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// User ...
type User struct {
	ID                   string
	EntityID             string
	CreatedAt            time.Time
	Enabled              bool
	CredentialByStrategy map[LoginStrategy]LoginCredential
}

// ContextRole indicates a role and the context in which it applies
type ContextRole struct {
	Role        string `json:"r,required" example:"back-office"`
	ContextType string `json:"t,omitempty" description:"The type of entity or context that this role is bound to" example:"MerchantID"`
	ContextID   string `json:"id,omitempty" description:"The identity of the entity, in respect of which the user has this role (e.g. a particular merchant)." example:"87ASDR4AW8E7RAS87FD5"`
}

// LoginCredential ...
type LoginCredential struct {
	Password     string
	IDInStrategy string
}

// Error codes
const (
	UserNotFound errors.Code = 1000
)

// Email represents a valid email address.
type Email string

func (e Email) String() string {
	return string(e)
}

// Name represents the full name of a person.
type Name struct {
	FirstName   string `json:"first,required" description:"First name" example:"John"`
	MiddleNames string `json:"middle,omitempty" description:"Middle name(s)" example:"Saint Piere"`
	LastName    string `json:"last,required" description:"Last name" example:"Smith"`
	Prefix      string `json:"prefix,omitempty" description:"Prefix / Title" example:"Mr"`
	Suffix      string `json:"suffix,omitempty" description:"Suffix" example:"Jr"`
}

// Country represents a 2-digit ISO country code
type Country string

// Gender represents the different genders
type Gender string

// Values used for the different genders, extend by incrementing
const (
	GenderMale   Gender = "male"
	GenderFemale Gender = "female"
	GenderOther  Gender = "other"
)

// ToEnum represents gender as a smaller footprint byte
func (g Gender) ToEnum() byte {
	switch g {
	case GenderMale:
		return 0
	case GenderFemale:
		return 1
	case GenderOther:
		return 2
	}
	return 2
}

// GenderFromEnum returns the gender based on the smaller footprint byte
func GenderFromEnum(b byte) Gender {
	switch b {
	case 0:
		return GenderMale
	case 1:
		return GenderFemale
	default:
		return GenderOther
	}
}

// DateOfBirth represents when a person is born.
type DateOfBirth time.Time

// LoginStrategy enum
type LoginStrategy byte

// Values for login strategies
const (
	LoginStrategyPassword LoginStrategy = 1
)

// Age returns the current age.
func Age(d DateOfBirth) int {
	return time.Now().Year() - time.Time(d).Year()
}

// SignedToken takes in jwt.Claims and returns a signed token using a configured secret
func (s Service) SignedToken(jwtClaims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims)
	return token.SignedString([]byte(s.AuthJwtSecret))
}

// IsTokenValid checks if the token has a valid signature
func (s Service) IsTokenValid(tokenString string) bool {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method")
		}
		return []byte(s.AuthJwtSecret), nil
	})
	if err != nil {
		return false
	}
	return token.Valid
}

// TokenSubject returns the subject claim from a token
func (s Service) TokenSubject(tokenString string) (string, bool) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(s.AuthJwtSecret), nil
	})
	if err != nil {
		return "", false
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if sub, ok := claims["sub"].(string); ok {
			return sub, true
		}
	}
	return "", false
}
