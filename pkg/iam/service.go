package iam

import (
	"stream/fxbet/pkg/ids"
	"stream/fxbet/spatial/atomic/state"
	"stream/fxbet/spatial/events/storage"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

//go:generate flatc --go --gen-onefile --gen-object-api --go-namespace iam events.fbs

// Service ...
type Service struct {
	state.Atomic
	AuthJwtSecret                 string
	LoginExpiresIn                time.Duration
	userByID                      map[string]*User
	userByStrategyAndIDInStrategy map[LoginStrategy]map[string]*User
	rolesByUserID                 map[string][]ContextRole
}

// New instantiates a new wallet API
func New(evtStore storage.EventStorage) Service {
	s := Service{
		AuthJwtSecret:                 "my_secret_key",
		LoginExpiresIn:                7 * 24 * time.Hour,
		userByID:                      make(map[string]*User),
		userByStrategyAndIDInStrategy: make(map[LoginStrategy]map[string]*User),
		rolesByUserID:                 make(map[string][]ContextRole),
	}
	s.Atomic = state.New(s, evtStore)
	return s
}

// CreateUser creates a new user
func (s Service) CreateUser(strategy LoginStrategy, idInStrategy string, password string) (User, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return User{}, err
	}
	userID := ids.NewID()
	err = s.Dispatch(uint16(EvtTypeEvtUserCreated), &EvtUserCreatedT{
		UserID:       userID,
		Strategy:     byte(LoginStrategyPassword),
		Password:     string(hash),
		IDInStrategy: idInStrategy,
		At:           time.Now().UTC().UnixNano(),
	})
	user, ok := s.UserByID(userID)
	if !ok {
		return User{}, UserNotFound
	}
	return user, nil
}

// UpdateUser ...
func (s Service) UpdateUser(user User) (User, error) {
	found, ok := s.UserByID(user.ID)
	if !ok {
		return user, UserNotFound
	}
	if found.Enabled != user.Enabled && user.Enabled {
		err := s.Dispatch(uint16(EvtTypeEvtUserEnabled), &EvtUserEnabledT{
			UserID: user.ID,
			At:     time.Now().UTC().UnixNano(),
		})
		if err != nil {
			return user, err
		}
	}
	if found.Enabled != user.Enabled && !user.Enabled {
		err := s.Dispatch(uint16(EvtTypeEvtUserDisabled), &EvtUserDisabledT{
			UserID: user.ID,
			At:     time.Now().UTC().UnixNano(),
		})
		if err != nil {
			return user, err
		}
	}
	return user, nil
}

// EnableUser ...
func (s Service) EnableUser(userID string) error {
	return s.Dispatch(uint16(EvtTypeEvtUserEnabled), &EvtUserEnabledT{
		UserID: userID,
		At:     time.Now().UTC().UnixNano(),
	})
}

// DisableUser ...
func (s Service) DisableUser(userID string) error {
	return s.Dispatch(uint16(EvtTypeEvtUserDisabled), &EvtUserDisabledT{
		UserID: userID,
		At:     time.Now().UTC().UnixNano(),
	})
}

// AddRole ...
func (s Service) AddRole(userID string, role ContextRole) error {
	return s.Dispatch(uint16(EvtTypeEvtUserRoleAdded), &EvtUserRoleAddedT{
		UserID:      userID,
		Role:        role.Role,
		ContextType: role.ContextType,
		ContextID:   role.ContextID,
	})
}

// RemoveRole ...
func (s Service) RemoveRole(userID string, role ContextRole) error {
	return s.Dispatch(uint16(EvtTypeEvtUserRoleRemoved), &EvtUserRoleRemovedT{
		UserID:      userID,
		Role:        role.Role,
		ContextType: role.ContextType,
		ContextID:   role.ContextID,
	})
}

// ApplyEvent updates state according to the event. This is already called atomically.
func (s Service) ApplyEvent(evtType uint16, payload []byte) {
	switch EvtType(evtType) {
	case EvtTypeEvtUserCreated:
		evt := GetRootAsEvtUserCreated(payload, 0)

		userID := string(evt.UserID())
		if userID == "" {
			return
		}

		user := &User{
			ID:        userID,
			EntityID:  string(evt.EntityID()),
			CreatedAt: time.Unix(0, evt.At()),
			Enabled:   false,
			CredentialByStrategy: map[LoginStrategy]LoginCredential{
				LoginStrategyPassword: {
					Password:     string(evt.Password()),
					IDInStrategy: string(evt.IDInStrategy()),
				},
			},
		}

		s.userByID[userID] = user
		if evt.Password() != nil && evt.IDInStrategy() != nil && evt.Strategy() != 0 {
			strat, ok := s.userByStrategyAndIDInStrategy[LoginStrategy(evt.Strategy())]
			if !ok {
				strat = map[string]*User{}
			}
			strat[string(evt.IDInStrategy())] = user
			s.userByStrategyAndIDInStrategy[LoginStrategy(evt.Strategy())] = strat
		}
	case EvtTypeEvtUserEnabled:
		evt := GetRootAsEvtUserEnabled(payload, 0)
		user, ok := s.userByID[string(evt.UserID())]
		if user != nil && ok {
			user.Enabled = true
		}
	case EvtTypeEvtUserDisabled:
		evt := GetRootAsEvtUserDisabled(payload, 0)
		user, ok := s.userByID[string(evt.UserID())]
		if user != nil && ok {
			user.Enabled = false
		}
	case EvtTypeEvtUserRoleAdded:
		evt := GetRootAsEvtUserRoleAdded(payload, 0)
		roles, ok := s.rolesByUserID[string(evt.UserID())]
		if !ok {
			roles = make([]ContextRole, 0)
		}
		for _, role := range roles {
			if role.Role == string(evt.Role()) {
				return
			}
		}
		roles = append(roles, ContextRole{
			Role:        string(evt.Role()),
			ContextType: string(evt.ContextType()),
			ContextID:   string(evt.ContextID()),
		})
		s.rolesByUserID[string(evt.UserID())] = roles
	case EvtTypeEvtUserRoleRemoved:
		evt := GetRootAsEvtUserRoleRemoved(payload, 0)
		roles, ok := s.rolesByUserID[string(evt.UserID())]
		if !ok {
			return
		}
		for i, role := range roles {
			if role.Role == string(evt.Role()) && role.ContextID == string(evt.ContextID()) && role.ContextType == string(evt.ContextType()) {
				roles = append(roles[:i], roles[i+1:]...)
				break
			}
		}
		s.rolesByUserID[string(evt.UserID())] = roles
	}
}

// UserByID ...
func (s Service) UserByID(userID string) (User, bool) {
	var user User
	var found bool
	s.Atomically(func() {
		if userPtr := s.userByID[userID]; userPtr != nil {
			user = *userPtr
			found = true
		}
	})
	return user, found
}

// UserByStrategyAndIDInStrategy ...
func (s Service) UserByStrategyAndIDInStrategy(stratgy LoginStrategy, idInStrategy string) (User, bool) {
	var user User
	var found bool
	s.Atomically(func() {
		if userPtr := s.userByStrategyAndIDInStrategy[stratgy][idInStrategy]; userPtr != nil {
			user = *userPtr
			found = true
		}
	})
	return user, found
}

// UserExists if the ID in the given strategy already exists.
func (s Service) UserExists(strategy LoginStrategy, idInStrategy string) bool {
	_, ok := s.UserByStrategyAndIDInStrategy(strategy, idInStrategy)
	if ok {
		return true
	}
	return false
}

// AddUserRoles ...
func (s Service) AddUserRoles(userID string, roles ...ContextRole) error {
	user, ok := s.UserByID(userID)
	if !ok {
		return UserNotFound
	}
	for _, role := range roles {
		err := s.AddRole(user.ID, role)
		if err != nil {
			return err
		}
	}
	return nil
}

// SetStatus ...
func (s Service) SetStatus(userID string, enabled bool) error {
	if enabled {
		s.EnableUser(userID)
	}
	return s.DisableUser(userID)
}

// UserFromCredentials returns a user with the given credentials
func (s Service) UserFromCredentials(strategy LoginStrategy, idInStrategy string, password string) (User, bool) {
	user, ok := s.UserByStrategyAndIDInStrategy(strategy, idInStrategy)
	if !ok {
		return User{}, false
	}
	cred, ok := user.CredentialByStrategy[strategy]
	if !ok {
		return User{}, false
	}
	err := bcrypt.CompareHashAndPassword([]byte(cred.Password), []byte(password))
	if err != nil {
		return User{}, false
	}
	return user, true
}

// AuthToken returns a auth token
func (s Service) AuthToken(userID string) (string, error) {
	roles := s.UserRoles(userID)
	expiryTimeInSeconds := time.Now().UTC().Add(s.LoginExpiresIn).Unix()
	// TODO this a hack to set a long expiry for anyone with a scoring role (machine to machine api)
	for _, role := range roles {
		if role.Role == "scoring" {
			expiryTimeInSeconds = time.Now().UTC().Add(24 * 365 * time.Hour).Unix() // one year
		}
	}

	var jwtClaims jwt.MapClaims = map[string]interface{}{
		"sub":   userID,
		"roles": roles,
		"exp":   expiryTimeInSeconds,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims)
	tokenString, err := token.SignedString([]byte(s.AuthJwtSecret))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// UserByStrategy finds a user based on the strategy details.
func (s Service) UserByStrategy(strategy LoginStrategy, idInStrategy string) (User, bool) {
	return s.UserByStrategyAndIDInStrategy(strategy, idInStrategy)
}

// UserRoles returns the roles assigned to the user.
func (s Service) UserRoles(userID string) []ContextRole {
	var roles []ContextRole
	s.Atomically(func() {
		if foundRoles, ok := s.rolesByUserID[userID]; ok {
			roles = foundRoles
		}
	})
	return roles
}
