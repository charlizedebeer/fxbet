package iam

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"
)

var emailRegex *regexp.Regexp

func init() {
	emailRegex = regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
}

// UnmarshalJSON extenal input
func (c *Country) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	if len(s) != 2 {
		return fmt.Errorf("%v is not a valid ISO country code", s)
	}
	*c = Country(strings.ToUpper(s))
	return nil
}

// UnmarshalJSON extenal input
func (e *Email) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	trimmedLowercase := strings.Trim(strings.ToLower(s), " ")
	if !emailRegex.MatchString(trimmedLowercase) {
		return fmt.Errorf("%v is not a valid email format", s)
	}
	*e = Email(trimmedLowercase)
	return nil
}

// UnmarshalJSON extenal input
func (n *Name) UnmarshalJSON(b []byte) error {
	var j map[string]interface{}
	if err := json.Unmarshal(b, &j); err != nil {
		return err
	}
	first, ok := j["first"].(string)
	if !ok || first == "" {
		return fmt.Errorf("\"%v\" is not a invalid first name", j["first"])
	}
	last, ok := j["last"].(string)
	if !ok || last == "" {
		return fmt.Errorf("\"%v\" is not a invalid first name", j["first"])
	}
	if middle, ok := j["middle"].(string); ok {
		n.MiddleNames = middle
	}
	if prefix, ok := j["prefix"].(string); ok {
		n.MiddleNames = prefix
	}
	if suffix, ok := j["suffix"].(string); ok {
		n.MiddleNames = suffix
	}
	n.FirstName = first
	n.LastName = last
	return nil
}

// UnmarshalJSON extenal input
func (d *DateOfBirth) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	dob, err := time.Parse("2006-01-02", s)
	if err != nil {
		return err
	}
	*d = DateOfBirth(dob)
	return nil
}
