package games

import "sort"

// MostPopular sorts games by their rating
func MostPopular(games []GameEntity) *Sorter {
	return &Sorter{
		games: games,
		by: func(g1, g2 *GameEntity) bool {
			return g1.Rating > g2.Rating
		},
	}
}

// Sorter ...
type Sorter struct {
	games []GameEntity
	by    func(p1, p2 *GameEntity) bool
}

// Sort sorts the array by the given sort function
func (s *Sorter) Sort() {
	sort.Sort(s)
}

func (s *Sorter) Len() int {
	return len(s.games)
}

func (s *Sorter) Swap(i, j int) {
	s.games[i], s.games[j] = s.games[j], s.games[i]
}

func (s *Sorter) Less(i, j int) bool {
	return s.by(&s.games[i], &s.games[j])
}
