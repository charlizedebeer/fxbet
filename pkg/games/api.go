package games

import (
	"stream/fxbet/pkg/errors"
	"stream/fxbet/pkg/forex/currency"
)

// Game is the details of the game without the ID
type Game struct {
	Feed string   `json:"feed" description:"Feed the games uses" example:"USD/EUR"`
	Tags []string `json:"tags" description:"tags for the game" example:"fast"`
	// * 0 = Rises.
	// * 1 = Touches.
	Market MarketType `json:"market"`
	// * 0 = Disabled.
	// * 1 = Enabled.
	Status        GameStatus        `json:"status"`
	MinimumStake  currency.Currency `json:"minimum_stake" description:"minimum stake allowed"`
	MaximumStake  currency.Currency `json:"maximum_stake" description:"maximum stake allowed"`
	MinimumOdds   float64           `json:"minimum_odds" description:"minimum odds allowed"`
	MaximumOdds   float64           `json:"maximum_odds" description:"maximum odds allowed"`
	OddsTolerance float64           `json:"odds_tolerance" description:"maximum allowed odds tolerance"`
	Rating        float32           `json:"rating" description:"rating of the game" example:"4.5"`
}

// GameEntity ...
type GameEntity struct {
	ID string `json:"id" description:"Unique identifier" example:"01FB4HYR9AG6AQQ1FD2CSW3W9M"`
	Game
}

// MarketStatus ...
type MarketStatus byte

// MarketStatus values
const (
	MarketStatusClosed MarketStatus = 0
	MarketStatusOpen   MarketStatus = 1
)

// GameStatus ...
type GameStatus byte

// GameStatus values
const (
	GameStatusDisabled GameStatus = 0
	GameStatusEnabled  GameStatus = 1
)

// MarketType enum to define the market. A market has to be programatically added so this is safe to do.
// Markets are binary and defined in terms of one of the pair, so we dont say Rise/Fall we say Rise and you can bet for or against Rise
type MarketType byte

// MarketType values
const (
	MarketTypeRises   MarketType = 1
	MarketTypeTouches MarketType = 2
)

// Branding ...
type Branding struct {
	Title         string `json:"title" description:"Title of the game" example:"Hong Kong Kicks"`
	Description   string `json:"description" description:"Short description of the game"`
	ImageURL      string `json:"image" description:"URL pointing to the image" example:"https://fxbet.co/images/img.png"`
	IconURL       string `json:"icon" description:"URL pointing to the icon" example:"https://fxbet.co/images/img.png"`
	ActionIconURL string `json:"action_icon" description:"URL pointing to the action icon" example:"https://fxbet.co/images/img.png"`
	BackgroundURL string `json:"background" description:"URL pointing to the background" example:"https://fxbet.co/images/img.png"`
	Colour        string `json:"colour" description:"Primary for the game" example:"#ff0000"`
}

// Error codes
const (
	GameNotFound errors.Code = 1000
)
