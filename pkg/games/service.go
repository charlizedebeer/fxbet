package games

import (
	"stream/fxbet/pkg/forex/currency"
	"stream/fxbet/pkg/ids"
	"stream/fxbet/spatial/atomic/state"
	"stream/fxbet/spatial/events/storage"
)

//go:generate flatc --go --gen-onefile --gen-object-api --go-namespace games events.fbs

// Service describes state operations
type Service struct {
	state.Atomic
	gameByID         map[string]GameEntity
	brandingByGameID map[string]Branding
}

// New returns a games service
func New(evtStore storage.EventStorage) Service {
	svc := Service{
		gameByID:         make(map[string]GameEntity),
		brandingByGameID: make(map[string]Branding),
	}
	svc.Atomic = state.New(svc, evtStore)
	return svc
}

// CreateGame ...
func (s Service) CreateGame(game Game) (string, error) {
	id := ids.NewID()
	err := s.Dispatch(uint16(EvtTypeEvtGameCreated), &EvtGameCreatedT{
		ID:     id,
		Feed:   game.Feed,
		Tags:   game.Tags,
		Market: byte(game.Market),
		MinimumStake: &CurrencyAmountT{
			Value: game.MinimumStake.AmountBankers(),
			CCY:   game.MinimumStake.CCY,
		},
		MaximumStake: &CurrencyAmountT{
			Value: game.MaximumStake.AmountBankers(),
			CCY:   game.MaximumStake.CCY,
		},
		MinimumOdds:   game.MinimumOdds,
		MaximumOdds:   game.MaximumOdds,
		OddsTolerance: game.OddsTolerance,
		Rating:        game.Rating,
	})
	if err != nil {
		return "", err
	}
	return id, nil
}

// BrandGame ...
func (s Service) BrandGame(gameID string, brand Branding) error {
	_, ok := s.Game(gameID)
	if !ok {
		return GameNotFound
	}
	return s.Dispatch(uint16(EvtTypeEvtBrandingCreated), &EvtBrandingCreatedT{
		GameID:           gameID,
		Title:            brand.Title,
		ShortDescription: brand.Description,
		ImageURL:         brand.ImageURL,
		IconURL:          brand.IconURL,
		ActionIconURL:    brand.ActionIconURL,
		BackgroundURL:    brand.BackgroundURL,
		Colour:           brand.Colour,
	})
}

// Enable ...
func (s Service) Enable(gameID string) error {
	_, ok := s.Game(gameID)
	if !ok {
		return GameNotFound
	}
	return s.Dispatch(uint16(EvtTypeEvtEnableGame), &EvtGameEnabledT{
		GameID: gameID,
	})
}

// Disable ...
func (s Service) Disable(gameID string) error {
	_, ok := s.Game(gameID)
	if !ok {
		return GameNotFound
	}
	return s.Dispatch(uint16(EvtTypeEvtDisableGame), &EvtGameDisabledT{
		GameID: gameID,
	})
}

// ApplyEvent updates state based on events.
func (s Service) ApplyEvent(evtType uint16, payload []byte) {
	switch EvtType(evtType) {
	case EvtTypeEvtGameCreated:
		evt := GetRootAsEvtGameCreated(payload, 0)
		var tags []string
		for i := 0; i < evt.TagsLength(); i++ {
			tags = append(tags, string(evt.Tags(i)))
		}
		s.gameByID[string(evt.ID())] = GameEntity{
			ID: string(evt.ID()),
			Game: Game{Feed: string(evt.Feed()),
				Tags:   tags,
				Market: MarketType(evt.Market()),
				Status: GameStatusDisabled,
				MinimumStake: currency.Currency{
					Amount: currency.FromBankers(evt.MinimumStake(nil).Value()),
					CCY:    string(evt.MinimumStake(nil).CCY()),
				},
				MaximumStake: currency.Currency{
					Amount: currency.FromBankers(evt.MaximumStake(nil).Value()),
					CCY:    string(evt.MaximumStake(nil).CCY()),
				},
				MinimumOdds:   evt.MinimumOdds(),
				MaximumOdds:   evt.MaximumOdds(),
				OddsTolerance: evt.OddsTolerance(),
				Rating:        evt.Rating(),
			},
		}
	case EvtTypeEvtBrandingCreated:
		evt := GetRootAsEvtBrandingCreated(payload, 0)
		s.brandingByGameID[string(evt.GameID())] = Branding{
			Title:         string(evt.Title()),
			Description:   string(evt.ShortDescription()),
			ImageURL:      string(evt.ImageURL()),
			IconURL:       string(evt.IconURL()),
			ActionIconURL: string(evt.ActionIconURL()),
			BackgroundURL: string(evt.BackgroundURL()),
			Colour:        string(evt.Colour()),
		}
	}
}

// Games returns all the games in state
func (s Service) Games() []GameEntity {
	var gg []GameEntity
	s.Atomically(func() {
		for _, game := range s.gameByID {
			gg = append(gg, game)
		}
	})
	MostPopular(gg).Sort()
	return gg
}

// Game returns a game given an ID
func (s Service) Game(gameID string) (GameEntity, bool) {
	var game GameEntity
	var found bool
	s.Atomically(func() {
		game, found = s.gameByID[gameID]
	})
	return game, found
}

// Branding returns a game's branding by the gameID
func (s Service) Branding(gameID string) (Branding, bool) {
	var branding Branding
	var found bool
	s.Atomically(func() {
		branding, found = s.brandingByGameID[gameID]
	})
	return branding, found
}
