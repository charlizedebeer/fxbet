/// Defines persistent data structures for the FXBet users.
namespace fbs;

enum EvtType : uint16 {
	EvtGameCreated = 1000,
	EvtBrandingCreated = 1001,
	EvtEnableGame = 1002,
	EvtDisableGame = 1003
}

table CurrencyAmount {
	/// 6 decimal places, e.g. 42500000 for 42.50
	Value: int64;
	/// The ISO currency code
	CCY: string;
}

table EvtGameCreated {
	/// Unique identifier, typically ULID
	ID: string;
	/// A game has a Market which identifes the type of game, while the
	/// feed identifes the context of the market for example a currency pair feed would be 'EUR/USD'
	Feed: string;
	/// Human readable tags to power game discovery, grouping, etc
	Tags: [string];
	/// The nature of the game e.g. Rise, Touches, Range, etc
	Market: ubyte;
	/// Minimum Stake
	MinimumStake: CurrencyAmount;
	/// Maximum Stake
	MaximumStake: CurrencyAmount;
	/// Minimum odds, expressed as a fraction e.g. 1.75
	MinimumOdds: float64;
	/// Maximum odds, expressed as a fraction e.g. 1.75
	MaximumOdds: float64;
	// The tolerance, expressed as a factor (floating point number), e.g. 0.01 for 1%, that
	// incoming wagers and the odds that they are requesting, are allowed to deviate from the
	// actual game odds at the instant the wager is received.
	OddsTolerance: float64;
	/// Rating
	Rating: float32;
}

table EvtBrandingCreated {
    /// GameID of the game, typically ULID
    GameID: string;
	/// Title of the game
	Title: string;
	/// Short description of the game. 
	ShortDescription: string;
	/// URL to the image of the game
	ImageURL: string;
	/// URL to the icon of the game
	IconURL: string;
	/// URL to the action icon of the game
	ActionIconURL: string;
	/// URL to the background image of the game
	BackgroundURL: string;
	/// Primary colour of the gamer
	Colour: string;
}

table EvtGameEnabled {
    /// GameID of the game, typically ULID
    GameID: string;
}

table EvtGameDisabled {
    /// GameID of the game, typically ULID
    GameID: string;
}
