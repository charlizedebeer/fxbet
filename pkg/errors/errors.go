package errors

import (
	"encoding/json"
	"fmt"
)

// Coded errors give a code instead of text
type Coded interface {
	Code() int
}

// Code defines an error code based error
type Code int

// Code returns the code of the error
func (e Code) Code() int {
	return int(e)
}

func (e Code) Error() string {
	return fmt.Sprintf("refused: Code %d", e)
}

// UnmarshalJSON decodes json into an error
func (e *Code) UnmarshalJSON(b []byte) error {
	var errorCode struct {
		Code int `json:"code"`
	}
	if err := json.Unmarshal(b, &errorCode); err != nil {
		return err
	}
	*e = Code(errorCode.Code)
	return nil
}

// MarshalJSON encodes and error to json
func (e Code) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Code int `json:"code"`
	}{
		Code: int(e),
	})
}

// Detailed adds a detail message to an error code
type Detailed struct {
	Code   int    `json:"code" example:"1000"`
	Detail string `json:"detail" example:"OPTIONAL message details on the error"`
}

func (d Detailed) Error() string {
	return d.Detail
}

// UnmarshalJSON decodes json into an error
func (d *Detailed) UnmarshalJSON(b []byte) error {
	var errorCode struct {
		Code   int    `json:"code"`
		Detail string `json:"detail"`
	}
	if err := json.Unmarshal(b, &errorCode); err != nil {
		return err
	}
	d.Code = errorCode.Code
	d.Detail = errorCode.Detail
	return nil
}

// MarshalJSON encodes and error to json
func (d Detailed) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Code   int    `json:"code"`
		Detail string `json:"detail"`
	}{
		Code:   int(d.Code),
		Detail: d.Detail,
	})
}
