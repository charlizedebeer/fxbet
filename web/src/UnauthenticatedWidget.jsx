import React from 'react'
import { Link } from 'react-router-dom'

/** UnauthenticatedWidget displays top-level "widget" controls for unauthenticated
 * users. */
const UnauthenticatedWidget = () => {
  return (
    <div className='widget user unauthenticated'>
      <Link to='/sign-up'>Sign up</Link>
      <Link to='/sign-in'>Sign in</Link>
    </div>
  )
}

export default UnauthenticatedWidget
