import React, { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import { ping } from './api'
//import styled from 'styled-components'
import Header from './Header'


/** Landing page (public view) */
const Home = () => {
  // This is simple demonstration of an API call
  // State: Contains status and data for this call
  const [pingPong, setPingPong] = useState({ loading: false, failed: null, data: null })

  /** Calls the /ping endpoint, and manages UI status around it. */
  const callPing = async () => {
    setPingPong({ loading: true })
    try {
      const data = await ping()
      setPingPong({ loading: false, data })
    } catch (failed) {
      setPingPong({ loading: false, failed })
    }
  }

  // We call the above once, at time of component mount
  useEffect(() => {
    callPing()
  }, [])

  return (
    <div>

<Header/>
  
      <h1>Home</h1>

      <NavLink activeClassName="active" to="/sign-in">
      <button className="Login">Login</button>  
      </NavLink>

      <NavLink activeClassName="active" to="/sign-up">
      <button className="SignUp"> Sign Up</button>
      </NavLink>
 
      <pre>{JSON.stringify(pingPong, null, ' ')}</pre>
    </Wrapper>
  )
}
export default Home
