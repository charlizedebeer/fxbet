import React from 'react'
import { render } from 'react-dom'
import App from './App'

// Mount the app
render(<App />, document.getElementById('root'))
