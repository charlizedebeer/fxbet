import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { GlobalStyle } from './style'
import SignUp from './sign-up/SignUp'
import TermsByID from './userServices/TermsByID'
import SignUpVerify from './sign-up/SignUpVerify'
import SignIn from './userServices/SignIn'
import Home from './Home'

const App = () =>
  

 (

  <Router>
    <GlobalStyle />
    <Route exact path='/' component={Home} />
    <Route exact path='/user/:user_id/terms/accept' component={TermsByID} />
    <Route exact path='/sign-up' component={SignUp} />
    <Route exact path='/sign-up/verify/:token' component={SignUpVerify} />
    <Route path='/sign-in' component={SignIn} />
  </Router>
)

export default App
