import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import api from '../api'
/** A component which, when displayed to the user, takes care of the user-side
 * logic of implicitly agreeing to the terms required for this action, as per
 * the `/api/agreements/terms-required-for/{actionName}` API endpoint.
 *
 * This component is typically embedded within the component that kicks of
 * the user action, such as a registration form.
 */
const ImplicitAgreement = ({ actionName, prefix = 'By continuing, you accept our', receiveTermIDs }) => {
  // Data this component manages
  const [toAgree, setToAgree] = useState()
  const [loading, setLoading] = useState(false)
  const [failed, setFailed] = useState()

  // Given a TermSummariesForUserAction, calls back the
  // provided callback with a list of term IDs
  const callBackWith = ({ term_summaries = [] }) => {
    if (receiveTermIDs != null) {
      receiveTermIDs(term_summaries.map(({ id }) => id))
    }
  }

  // Fetch data based on provided props
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      try {
        const data = await api.queryTermsForUserAction(actionName)
        setToAgree(data)
        callBackWith(data)
      } catch (e) {
        setFailed(e)
      }
      setLoading(false)
    }
    fetchData().catch((err) => {
      console.error(err)
    })
  }, [])

  return (
    <div className='agreement implicit'>
      {loading && <div>Loading</div>}
      {failed && <div>Failed</div>}
      {toAgree && toAgree.term_summaries && (
        <span className='summary'>
          {prefix}{' '}
          {toAgree.term_summaries.map(({ id, title }, idx) => (
            <span key={id}>
              <Link to={`/agreements/terms/${id}`} target='_blank'>
                {title}
              </Link>
              {idx < toAgree.term_summaries.length - 1 && (
                <span className='sep'>{idx == toAgree.term_summaries.length - 2 ? ' and ' : ', '}</span>
              )}
            </span>
          ))}
          .
        </span>
      )}
    </div>
  )
}

ImplicitAgreement.propTypes = {
  // Logical description of the user action, e.g. 'user.signup'
  actionName: PropTypes.string.isRequired,
  // Callback function to tell the parent component _which_ terms
  // the user is agreeing to, of type:
  // [TermID] -> ()
  receiveTermIDs: PropTypes.func,
  // Optionally, customise the text that is displayed before the linked
  // set of terms document titles / links.
  prefix: PropTypes.string,
}

export default ImplicitAgreement
