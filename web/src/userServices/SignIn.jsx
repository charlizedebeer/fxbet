import LayoutPublic from '../ui/layout/LayoutPublic'
import ContentSmallCentered from '../ui/layout/ContentSmallCentered'
import { Heading, Spacer } from '../ui/Typography'
import { Input, InputRow } from '../ui/Forms'
import PublicButtonNav from '../ui/buttons/PublicButtonNav'
import PublicActionButton from '../ui/buttons/PublicActionButton'
import {SignInWrapper, TitleArea, FormButtons} from '../ui/Forms'
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Link, useHistory } from 'react-router-dom'
import { authenticateUser } from '../api'
import { useAuth } from '../auth'

/** SignIn requests user authentication, and after authentication, routes
  the user to a previously-recorded destination - or to the "home" view if
  none were set. */
  
const SignIn = ({ location }) => {
  const history = useHistory()
  const [email, setEmail] = useState(undefined)
  const [password, setPassword] = useState(undefined)
  const [loginError, setLoginError] = useState('')
  const { setToken } = useAuth()
  const { fromURL } = location.state || { fromURL: '/' }

  const handleSubmit = () => {
    if (email && password) {
      setLoginError(null)
      authenticateUser(email, password)
        .then((res) => {
          if (!res) {
            setLoginError('That did not work, please try again')
          } else {
            setToken(res.token)
            history.push(fromURL)
          }
        })
        .catch((err) => {
          console.error(err)
        })
    } else {
      setLoginError('Please provide both email and password')
    }
  }

  const handleKeyPressed = (e) => {
    if (e.key === 'Enter') {
      handleSubmit()
    }
  }

  return (
    // TODO: See if `AuthenticatedRoute` set a user destination, and if so,
    // route user there upon login.
    <LayoutPublic>
      <ContentSmallCentered>
      <TitleArea>
<Heading title > Log into your FXBET account </Heading>
</TitleArea>
<SignInWrapper>
       <InputRow suffixLabel='The email address linked to your account'>
          <Input
            type='email'
            placeholder='lexi@llamamail.com'
            id='email'
            onChange={(e) => setEmail(e.target.value)}
            onKeyPress={handleKeyPressed}
            autoFocus
          /></InputRow>

        <InputRow suffixLabel='Enter your account password to log in'>
          <Input
            type='password'
            id='password'
            onChange={(e) => setPassword(e.target.value)}
            onKeyPress={handleKeyPressed}
          /></InputRow>
    
        <FormButtons>

          <PublicButtonNav className='login-button' onClick={handleSubmit}>  
            <span className="text">Login</span> 
            {loginError !== '' && <div className='error'>{loginError}</div>}
`         </PublicButtonNav>
      <Link to='/password-reset'>Forgot your <br/> password? Reset it <a href="#">here</a> </Link>

        </FormButtons>
        
          
</SignInWrapper>

<Spacer vertical />
<Heading primary>Don&apos;t have an FXBET account?</Heading>

<PublicActionButton >
   Sign up for an account 
</PublicActionButton>

        </ContentSmallCentered>
</LayoutPublic>
  )
}
SignIn.propTypes = {
  location: PropTypes.object.isRequired,
}

export default SignIn



