import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useAuth } from '../auth'
import { getUser } from '../api'
import { pick } from 'ramda'
import UserContext from './UsersContext'

// DEPRECATED - use `org-scope` instead
const UserProvider = ({ children }) => {
  const { token } = useAuth()
  const [User, setUser] = useState()

  useEffect(() => {
    async function fetchMyAPI() {
      const response = await getUser(token)
      if (response) {
        setUser(response.map(pick(['id', 'description'])))
      }
    }
    fetchMyAPI()
  }, [token])

  const { Provider } = UserContext
  return <Provider value={User}>{children}</Provider>
}

UserProvider.propTypes = {
  children: PropTypes.any,
}

export default UserProvider
