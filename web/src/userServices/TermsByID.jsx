import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'
import { Helmet } from 'react-helmet'
import { useAuth } from '../auth'

/** Renders the content of an identified terms document. Supports both
 * an explicit player_id prop, or react-router match.*/
const TermsByID = ({ player_id, match }) => {
  const termsID = player_id || (match && match.params ? match.params.player_id : undefined)
  // Data this component manages
  const [terms, setTerms] = useState()
  const [loading, setLoading] = useState(false)
  const [failed, setFailed] = useState()

  // Fetch data based on player_id prop
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      try {
        const data = await useAuth.queryTermsByID(termsID)
        setTerms(data)
      } catch (e) {
        setFailed(e)
      }
      setLoading(false)
    }
    fetchData()
  }, [termsID])

  return (
    <div className='terms content'>
      {loading && <div>Loading</div>}
      {failed && <div>Failed</div>}
      {terms && (
        <>
          <Helmet>
            <title>{`${terms.title} - FXBet`}</title>
          </Helmet>
          <h1>{terms.title}</h1>
          <div className='body'>
            <ReactMarkdown source={terms.body} />
          </div>
        </>
      )}
    </div>
  )
}

TermsByID.propTypes = {
  // The raw player_id of the terms document to display. Takes precedence
  player_id: PropTypes.string,
  // react-router match, assumes match.param.player_id
  match: PropTypes.object,
}

export default TermsByID
