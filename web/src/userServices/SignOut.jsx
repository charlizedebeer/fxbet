import React from 'react'
import { useAuth } from '../auth'
import { Redirect } from 'react-router-dom'
import ContentSmallCentered from './ui/layout/ContentSmallCentered'

const SignOut = () => {
  const { setToken } = useAuth()
  // Kill user's active token
  setToken(null)
  return (
      <ContentSmallCentered>
     
          <Redirect to='/' />
          </ContentSmallCentered>
 
  )} 

export default SignOut
