// Simple hack: If we're running on localhost, hit the known port of the back-end
// in development mode, otherwise whatever is currently serving the UI. This is because
// the back-end is a single server that also serves the React web app itself.
const port = window.location.hostname === 'localhost' ? ':8090' : ''

/** Base URL for REST API calls */
export const API_URL = `${window.location.protocol}//${window.location.hostname}${port}/api`
/** Base URL for web socket connections */
//const WS_URL = `wss://${window.location.hostname}${port}/api`

/** Given a `fetch` response, handles redirecting the
 * user to the sign-in page if server sends a 401 unauthorized,
 * otherwise simply returns the given response. */
export const handleSignIn = (response) => {
  if (!response.ok) {
    // Trap 401 and route user to login page
    if (response.status === 401) {
      if (console && console.warn) {
        console.warn('Redirecting user to login page because 401 received: ', response)
      }
      // TODO: Should we do this (which could also be useful in triggering
      // a page load, updating e.g. stale app code) or should we use the
      // history API (react-router) ?
      window.location.href = '/sign-in'
    }
    // Pass-through any other error
    if (console && console.warn) {
      console.warn('Error response received: ', response)
    }
    throw Error(response.statusText)
  }
  return response
}


/** Sends a request to initiate player signup to the server.
 * After this, the user has to confirm by clicking a link
 * in an email, and then complete user signing using this token,
 * as well as setting a password. */
export const initiatePlayerSignUp = (req) =>
  fetch(`${API_URL}/user/registrations`, {
    mode: 'cors',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(req),
  }).then((r) => r.json())

/** Sends a request to complete player signup to the server.
 * Includes a token received via email, and sets user password.
 * Returns a token, i.e. user is effectively signed in. */
export const completePlayerSignUp = () =>
  fetch(`${API_URL}/user/verifications`, {
    mode: 'cors',
    method: 'POST',
    headers: {
      'Accept': 'application/json', 
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify({
      password: '',
      token: ''
    }),
  }).then((r) => r.json())

// TODO: Authenticate (log in)
// TODO: Determine user's country, to pre-populate country in signup form
export const acceptTerms = (req) =>
  fetch(`${API_URL}/user/:player_id/terms/accept`, {
    mode: 'cors',
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(req),
  }).then((r) => r.json())

  export const AcceptTermsRequest = (req) =>
  fetch(`${API_URL}/user/:player_id/terms/accept`, {
    mode: 'cors',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify(req),
  }).then((r) => r.json())

 export const authenticateUser = (response) =>
 fetch(`${API_URL}/user/auth`, {
   mode: 'cors',
   method: 'POST',
   headers: {
     'Content-Type': 'application/json;charset=utf-8',
   },
   body: JSON.stringify(response),
 }).then((r) => r.json())

/** Sends a simple PING request, useful to check if the API is reachable
 * and alive. Returns a Promise<String>. */
 export const ping = () =>
 fetch(`${API_URL}/ping`, {
   mode: 'cors',
   method: 'GET',
 })
   .then(handleSignIn) // <- Only do this for other calls that require an auth token
   .then((r) => r.text())
