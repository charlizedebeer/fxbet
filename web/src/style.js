import { createGlobalStyle } from 'styled-components'

import QuicksandWoff from 'url:../node_modules/@fontsource/quicksand/files/quicksand-latin-400-normal.woff'
import QuicksandWoffBold from 'url:../node_modules/@fontsource/quicksand/files/quicksand-latin-700-normal.woff'
import QuicksandWoff2 from 'url:../node_modules/@fontsource/quicksand/files/quicksand-latin-400-normal.woff2'
import QuicksandWoff2Bold from 'url:../node_modules/@fontsource/quicksand/files/quicksand-latin-700-normal.woff2'

/** Special global styling, to style the HTML container that the app runs in.
 * All other styling is applied to individual components in isolation. */
export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'Quicksand';
    src: url(${QuicksandWoff}) format('woff2'), url(${QuicksandWoff2}) format('woff');
  }
  @font-face {
    font-family: 'Quicksand Bold';
    src: url(${QuicksandWoffBold}) format('woff2'), url(${QuicksandWoff2Bold}) format('woff');
  }

  html, body {
    font-family: 'Quicksand', sans-serif;
    font-size: 16px;
    margin: 0;
    padding: 0;
    /*background-color: #0960b0;*/
    background-color: #333333;
    color: white;
  }

  a {
    color: white;
  }

  * {
    box-sizing: border-box;
  }
`
