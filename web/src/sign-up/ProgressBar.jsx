import React from "react"
import PropTypes from "prop-types"
import styled from 'styled-components'

const Container = styled.div`
text-align: -webkit-center;

progress[value]{
     min-width: 17rem;
     justify-content: center;
  appearance: none;
  ::-webkit-progress-bar {
    height:0.625rem;
    border-radius: 0.4rem;
    border-color: #000000;
    border-width: 2px;
    text-align: -webkit-left;
    background-color:#F7F6F6;
 

  }

  ::-webkit-progress-value{
    height:0.625rem;
    text-align: -webkit-left;
    border-radius: 0.4rem;
    background-color: #69b934;
  }
}
`


const ProgressBar = props => {
  const {value, max, width, step, totalStep } = props;

  return (
  <Container>
  <progress value={value} max={max} step={step} totalStep={totalStep} width={width} />
  <span>  STEP {step} / {totalStep}</span>
  </Container>
  );

};

ProgressBar.PropTypes={
  value: PropTypes.number.isRequired,
  max: PropTypes.number,
  width: PropTypes.string,
  step: PropTypes.number,
  totalStep: PropTypes.number,
}
ProgressBar.defaultProps={
  max:100,
  width: '18rem',
  totalStep: 4,
};

export default ProgressBar;