import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Step from './Step'
import { Heading, Spacer } from '../ui/Typography'
import { Input, InputRow } from '../ui/Forms'
import PublicButtonNav from '../ui/buttons/PublicButtonNav'
import ProgressBar from './ProgressBar'
import SignupBtnWrapper from './SignupBtnWrapper'

const Step1 = ({ req = {}, setReq = () => null }) => {
  const [agreesTo, setAgreesTo] = useState(req?.agreesTo)
  const [email, setEmail] = useState(req?.email)
  const onNext = () => setReq({ ...req, email: email, accepted_term_ids: agreesTo })
  
  return (
    <Step>
      <Heading primary>Excellent choice!</Heading>
      <Heading tertiary>Sign up is quick and easy</Heading>
      <Spacer vertical />
     

      <Heading primary>
        First, we will need your email to get started...
        </Heading>

<Spacer vertical />

      <InputRow suffixLabel='A verification email will be sent to this email address'>
        <Input
          type='email'
          placeholder='lexi@llamamail.com'
          name='email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoFocus
        />
      </InputRow>
      <Spacer vertical />

  
      <ProgressBar value={25} step= {1}/>
      <Spacer secodary />
<SignupBtnWrapper>
      <PublicButtonNav value={agreesTo} className="StepBtn" onClick={onNext} onSubmit={setAgreesTo}>
        <span className="text">Get Started</span>
        </PublicButtonNav>
        </SignupBtnWrapper>
    </Step>
  )
}

Step1.propTypes = {
  req: PropTypes.object.isRequired,
  setReq: PropTypes.func.isRequired,
}

export default Step1
