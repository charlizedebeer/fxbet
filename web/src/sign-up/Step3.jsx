import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { countries } from 'country-data'
import PublicButtonNav from '../ui/buttons/PublicButtonNav'
import ProgressBar from './ProgressBar'
import SignupBtnWrapper from './SignupBtnWrapper'
import Step from './Step'
import { Heading, Spacer } from '../ui/Typography'
import { InputRow, Select } from '../ui/Forms'
import styled from 'styled-components'


const StyledName = styled.text`
color: #7fe1ad;
`
const sortedCountries = countries.all
  .filter(({ alpha2 }) => alpha2)
  .sort((a, b) => {
    var nameA = a.name.toUpperCase()
    var nameB = b.name.toUpperCase()
    return nameA < nameB ? -1 : nameA > nameB ? 1 : 0
  })

const Step3 = ({ req = {}, setReq = () => null }) => {
  const [info, setInfo] = useState({ ip: '' })
  useEffect(() => {
    fetch('https://ip.nf/me.json', { method: 'get' })
      .then((response) => response.json())
      .then((data) => {
        setInfo({ ...data })
      })
  }, [])
  const defaultCountryCode = info.ip.country_code
  //const countryFlag = CountryData.countries[infop.ip.country_code].emoji
  const defaultCountry = info.ip.country + ' ' + info.ip.country_code

  const [country, setCountry] = useState(req?.country)

  const onNext = () => setReq({ ...req, country: country })

  return (
    /** A self-contained country selector, using locally-packaged country data.
     * Adds about 100kb to the bundled app size ;-)
     */

    /**TODO 1. Make this variable in the text display the player's name  
        2. Conditional logic for country selcector  */

  <Step>
      <Heading primary>Thanks <StyledName> {req?.name?.first},</StyledName> what is your country of residence... </Heading >
     
     <Spacer vertical />
    

      <InputRow suffixLabel="    We currently support players from all EU and EFTA countries. Your permanent address will need to be verified
        later when paying out your winnings"> 
      <Select
        name='country'
        value={country}
        onChange={setCountry != null ? (e) => setCountry(e.target.value) : undefined}
      >
        {
          <option
            name='default'
            value={defaultCountryCode}
            label={defaultCountry}
            onLoadedData={setCountry}
            //Add country flag countries[info.ip.country_code].emoji
          >
            {' '}
            Select Country:
          </option> /* 
        On reload - flag dissapears when using country-data
        {countries[defaultCountryCode].emoji} 
       Default Option*/
        }

        {sortedCountries.map(({ alpha2, name, emoji }) => (
          <option name='country' value={alpha2} key={`${alpha2}-${name}`}>
            {emoji} {name} {alpha2}
          </option>
        ))}
      </Select>
      </InputRow>
    <Spacer secondary />
     

    
<Spacer vertical />

 
      <ProgressBar value={75} step={3} />
      <Spacer vertical />
      <SignupBtnWrapper>
      <PublicButtonNav className="StepBtn" onClick={onNext}>
        <span className="text">Next</span>
        </PublicButtonNav>
        </SignupBtnWrapper>
       
   
        </Step>
  )
}

Step3.propTypes = {
  req: PropTypes.object.isRequired,
  setReq: PropTypes.func.isRequired,
}

export default Step3
