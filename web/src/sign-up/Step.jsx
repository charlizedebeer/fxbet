import styled from 'styled-components'

/** Step is a wrapper for a single sign-up 'step' */
const Step = styled.div`
  width: 100%;
  max-width: 25rem;

`

export default Step
