import React, { useState } from 'react'
import { initiatePlayerSignUp } from '../api'
import { validateEmail, validateName } from '../validations'
import SignUpVerify from './SignUpVerify'
//Use Luxon for Date handling

import Step1 from './Step1'
import Step2 from './Step2'
import Step3 from './Step3'
import Step4 from './Step4'

import Header from '../Header'


import styled from 'styled-components'

const Wrapper = styled.section
`
  padding: 0.5em;
  position: fixed;
  z-index: 10;
  background:linear-gradient(180deg, #000000 0%,#4787ff 50%,#0b5eab 100% );
  min-width: 100%;
  min-height: 100%;
  `

  const Content = styled.section
  `
  display: flex;
  justify-content: center;
  align-text: center;
  `




/** Given the state of a request, indicates the UI step that must be shown. Step 1-4. */
const calcStep = ({ email, name, country, dateOfBirth }) =>
  !validateEmail(email) ? 1 : !validateName(name.first) || !validateName(name.last) ? 2 : 
  !country.length ? 3 : !dateOfBirth.length   ? 4 : 5 

/** Gathers basic user info in multiple steps, one at a time,
 * before initiating signup. A user then clicking the verify link in an
 * email will them verify and complete signup (SignupVerify). */

const SignUp = () => {
  // The user signup details being populated
  const [req, setReq] = useState({
    name: {
      first: '',
      last: '',
    },
    email: '',
    country: '',
    dateOfBirth: '',
    acceptTerms: ''


  })

  // Based on what's already populated, show step
  const inputStep = calcStep(req)

  const updateReq = (req) => {
    setReq(req)

    if (calcStep(req) === 5) {
      initiatePlayerSignUp(req)
        .then((res) => {
          console.log('signupid', res.sign_up_id)
        })
        .catch((err) => console.error('error', err))
    }
  }

  return (
    <LayoutPublic>
      <ContentSmallCentered>
        {inputStep === 1 && <Step1 req={req} setReq={updateReq} />}
        {inputStep === 2 && <Step2 req={req} setReq={updateReq} />}
        {inputStep === 3 && <Step3 req={req} setReq={updateReq} />}
        {inputStep === 4 && <Step4 req={req} setReq={updateReq} />}
        {inputStep === 5 && <SignUpVerify />}

      </ContentSmallCentered>
    </LayoutPublic>
  )
}

export default SignUp
