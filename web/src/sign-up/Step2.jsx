import PropTypes from 'prop-types'
import React, { useState } from 'react'
import Step from './Step'
import { Heading, Spacer} from '../ui/Typography'
import { Input, InputRow } from '../ui/Forms'
import PublicButtonNav from '../ui/buttons/PublicButtonNav'
import ProgressBar from './ProgressBar'
import SignupBtnWrapper from './SignupBtnWrapper'

const Step2 = ({ req = {}, setReq = () => null }) => {
  const [firstName, setFirstName] = useState(req?.name.first)
  const [lastName, setLastName] = useState(req?.name.last)
  const onNext = () => setReq({ ...req, name: { first: firstName, last: lastName } })

  return (
    <Step>
      <Heading>Next, please enter your first and last name...</Heading>
      <Spacer vertical />
      <InputRow suffixLabel='First and last name as it appears on your ID'>
        <Input name='firstName' value={firstName} onChange={(e) => setFirstName(e.target.value)} />
        <Input name='lastName' value={lastName} onChange={(e) => setLastName(e.target.value)} />
      </InputRow>
      <Spacer vertical />
      <ProgressBar value={50} step={2} />
      <Spacer vertical />
      <SignupBtnWrapper>
      <PublicButtonNav className="StepBtn" onClick={onNext}>
        <span className="text">Next</span>
        </PublicButtonNav>
        </SignupBtnWrapper>

    </Step>
  )
}

Step2.propTypes = {
  req: PropTypes.object.isRequired,
  setReq: PropTypes.func.isRequired,
}

export default Step2
