import React from 'react'
import styled from 'styled-components'


const ButtonFSContainer = styled.div`
min-width: 100%;
display: flex;
flex-direction: row-reverse;
margin-top: 1rem;
`

const SignupBtnWrapper = ({children}) => {
  return (
    <ButtonFSContainer>
      {children} 
    </ButtonFSContainer>
  )
}

export default SignupBtnWrapper
