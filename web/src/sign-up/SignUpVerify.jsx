import React, { useState } from 'react'
import { completePlayerSignUp } from '../api'

//TODO get these values correctly

//TODO Email confirmation + set passwd here

const SignUpVerify = () => {
  const [password, setPassword] = useState('')
  const [token, setToken] = useState()

  const createPassword = () => {
    if (password.length > 7 && token.length > 7) {
      completePlayerSignUp({ password, token })
        .then((res) => {
          console.log('player_id', res.player_id)
        })
        .catch((err) => console.error('error', err))
    }
  }

  return (
    <div>
      <p>
        Create Password: <input name='password' value={password} type="password" onChange={(e) => setPassword(e.target.value)} />
      </p>      
      <p>
        Email Token: <input name='token' value={token} onChange={(e) => setToken(e.target.value)} />
      </p>
      <p> Dear </p>,
      <p>
        {' '}
        FXBET wants to make your account opening process as smooth as possible. Please clink the link below to verify your
        email address:
      </p>
      <br />
      <button onClick={createPassword}>Click to Confirm Email</button>
      <br />
      <p>
        {' '}
        {/**TODO Text Component Structures */}
        Should you have further questions, we are always available to help with every step of your gaming experience.{' '}
        <br />
        We look forward to having you onboard at FXBET!
        <br />
        <br />
        <br />
        Kind regards,
        <br />
        <br />
        <br />
        <b>FXBET Team</b>
        {/**TODO Remove random inline styling */}
        <span>Phone: +123 123 123 1234 Email: support@fxbet.co</span>
      </p>
    </div>
  )
}

export default SignUpVerify
