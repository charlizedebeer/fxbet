import { DateTime } from 'luxon'
import PropTypes from 'prop-types'
import React, { useState } from 'react'
import { validateAge } from '../validations'
import PublicButtonNav from '../ui/buttons/PublicButtonNav'
import ProgressBar from './ProgressBar'
import SignupBtnWrapper from './SignupBtnWrapper'
import Step from './Step'
import {  Spacer } from '../ui/Typography'
import { Input, InputRow, Select } from '../ui/Forms'


const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

/** Gathers the birthdate of the user (mandatory) */
const Step4 = ({ req = {}, setReq = () => null }) => {
  // State of three individual inputs
  const [day, setDay] = useState('') // TODO: Init from req
  const [month, setMonth] = useState('') // 1 for Jan
  const [year, setYear] = useState('') // TODO: Init from req
  const now = new Date()

  // If all fields set, construct a date object
  // We use this date to calculate validity, and to produce the final ISO date. Time is unimportant.
  const birthdate = day != '' && month != '' && year != '' ? DateTime.fromObject({ day, month, year }) : null

  const onDayChange = (e) => {
    const val = e.target.value
    if (val <= 31) {
      setDay(e.target.value)
    }
  }
  const onYearChange = (e) => {
    const val = e.target.value
    if (val <= now.getFullYear()) {
      setYear(e.target.value)
    }
  }

  // Is date invalid? (either under-age, or e.g. 31 Feb)
  const isValid = () => validateAge(birthdate.toISODate())
  // TODO: implement//validateAge

  const onNext = () => {
    if (isValid) {
      setReq({ ...req, dateOfBirth: birthdate.toISODate() })
    }
  }

  return (
    // Array of months
    <Step>
    <div className='dateOfBirth'>
    <InputRow suffixLabel='The day you took your first breath'>
      <Input type='number' min='1' max='31' placeholder='DD' onChange={onDayChange} value={day} />
      <Select onChange={(e) => setMonth(e.target.value)} value={month}>
        {!month && <option value=''>Month</option>}
        {months.map((month, index) => (
          <option value={index + 1} key={index + 1}>
            {month}
          </option>
        ))}
      </Select>
      <Input value={year} type='number' min='1900' max={now.getFullYear()} placeholder='YYYY' onChange={onYearChange} />
      </InputRow>
 <Spacer vertical />
      <ProgressBar value={100} step={4} />


      <Spacer vertical />
      <SignupBtnWrapper>
      <PublicButtonNav className="StepBtn" onClick={onNext}>
        <span className="text">Next</span>
        </PublicButtonNav>
        </SignupBtnWrapper>

    </div>
    </Step>
  )
}

Step4.propTypes = {
  req: PropTypes.object.isRequired,
  setReq: PropTypes.func.isRequired,
}

export default Step4
