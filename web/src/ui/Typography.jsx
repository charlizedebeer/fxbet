import styled from 'styled-components'
import PropTypes from 'prop-types'

/** Heading for a page or section */
export const Heading = styled.div`
  font-family: 'Quicksand Bold', sans-serif;
  font-size: ${(props) => (props.tertiary ? '1.15rem': props.title? '2rem' : props.secondary ? '1.5rem' : '1.75rem')};
  text-align: center;
  padding-bottom: 0.75rem;
`
Heading.propTypes = {
  children: PropTypes.any,
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  title: PropTypes.bool,
  tertiary: PropTypes.bool,
}

/**  SpacerVert is a utility to explicitly render a blank vertical space.
 * Usually strongly discouraged, and useful in very few scenarios. */
export const Spacer = styled.div`
  display: block;
  width: 100%;
  height: ${(props) => (!props.vertical ? '0' : props.tertiary ? '1.15rem' : props.secondary ? '1.5rem' : '1.75rem')};
  width: ${(props) =>
    !props.horizontal ? '100%' : props.tertiary ? '1.15rem' : props.secondary ? '1.5rem' : '1.75rem'};
`
Spacer.propTypes = {
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  tertiary: PropTypes.bool,
  horizontal: PropTypes.bool,
  vertical: PropTypes.bool,
}
