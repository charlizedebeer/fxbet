import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

/** A simple form input, e.g text, email, number. */
export const Input = styled.input`
  font-family: 'Quicksand', sans-serif;
  font-size: 1rem;
  border: none;
  color: white;
  background-color: rgba(0, 0, 0, 0.5);
  height: 2.5rem;
  border-radius: 0.5rem;
  margin: 0.25rem;
  padding: 0.25rem 0.5rem;
  outline: 0;
  text-align: center;
`

/** A top-level 'row' in a form, with one or more inputs rendered filling the entire row. */
export const InputRow = ({ children, suffixLabel }) => (
  <InputRowWrapper>
    <InputRowItemsWrapper>{children}</InputRowItemsWrapper>
    {suffixLabel && <InputRowItemsSuffixWrapper>{suffixLabel}</InputRowItemsSuffixWrapper>}
  </InputRowWrapper>
)
InputRow.propTypes = {
  /** One or more input elements to render in the input row */
  children: PropTypes.any,
  /** Optional small suffix label applied underneath */
  suffixLabel: PropTypes.any,
}

const InputRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
  width: 100%;
  padding-bottom: 0.75rem;
`
const InputRowItemsWrapper = styled.div`
  flex: auto;
  display: flex;
  ${Input} {
    flex: 1;
  }
`
const InputRowItemsSuffixWrapper = styled.div`
  font-size: 0.75rem;
  text-align: center;
  padding: 0.5rem 0;
`

export const Select = styled.select
`
font-family: 'Quicksand', sans-serif;
font-size: 1rem;
border: none;
color: white;
background-color: rgba(0, 0, 0, 0.5);
height: 2.5rem;
border-radius: 0.5rem;
margin: 0.25rem;
padding: 0.25rem 0.5rem;
outline: none;
text-align: center;

  option {
    color: white;
    background: #034e76;
    display: flex;
    min-height: 20px;
    outline: none;
    padding: 0.25rem 0.5rem;
  }
`
export const SignInWrapper = styled.div`
  width: 100%;
  max-width: 25rem;
  padding: 0rem 1rem;
 
`
export const TitleArea = styled.section`
max-height: 7rem;
position:top;
padding: 0rem 2rem;
margin-bottom:1rem;
`

export const FormButtons = styled.div`
font-family: "Quicksand";
color: #ffffff;
display: grid; 
grid-template-columns: 1fr 35%; 
grid-template-rows: 1fr; 
align-content: end; 
gap: 0px 0px; 
grid-template-areas: 
  "ForgotPassword LoginButton"; 

}
.login-button { grid-area: LoginButton; }
.ForgotPassword { grid-area: ForgotPassword; }
`

export const forgotPassword = styled.div`
display: inline;
font-weight: bolder;
font-size: 1.3em;
text-align: left;

>* {
  text-decoration: none;
}

`