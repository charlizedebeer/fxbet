import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import LogoFull from 'url:./fxbet-logo-full.svg'
import LogoShort from 'url:./fxbet-logo-short.svg'

const Wrapper = styled.div`
  text-indent: -9999px;
  background-image: url(${(props) => (props.compact ? LogoShort : LogoFull)});
  background-size: contain;
  background-position: left center;
  background-repeat: no-repeat;
  min-height: 2em;
  margin: 0.7rem;
  grid-column-start: 1;
`
/** Renders an FXBet logo - a div which is 'skinned' with either a full-size, or compact,
 * fxbet logo. */
const FXBetLogo = (props) => <Wrapper {...props}>FXbet</Wrapper>

FXBetLogo.propTypes = {
  compact: PropTypes.bool,
}

export default FXBetLogo
