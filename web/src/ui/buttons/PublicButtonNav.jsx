import React from 'react'
import styled from 'styled-components'
import { FaChevronRight } from 'react-icons/fa'



const PrimaryBtn = styled.button`
color: #FFFFFF;
border-radius: 1.8em;
border: 0px #3866a3 solid;
background: linear-gradient(0deg, #009244 0%, #dae021 100%);
cursor: pointer;
align-text: center;
display: inline-flex;
align-items: center;
float: right;
font-family: 'Quicksand',sans-serif ;
font-weight:bolder;
min-height: 1.75rem ;
}

&:hover {
background:linear-gradient(180deg, #d6d6d6 0%,#7c797d 76%,#605c62 100% );
}
&:active {
background:linear-gradient(180deg, #d6d6d6 0%,#7c797d 76%,#605c62 100% );
}
.text {
padding: 0.8rem;
font-size: 1.3em;

}
svg {
position: relative;
margin-right: 0.5em;
font-size: 20px;

}
.text span{
display: inline;
position: relative;
top: 0px;

}
`

const PublicButtonNav = ({ children, ...props }) => {
  return (
    <PrimaryBtn {...props}>
      {children} <FaChevronRight />
    </PrimaryBtn>
  )
}

export default PublicButtonNav
