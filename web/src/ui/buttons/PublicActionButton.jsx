import React from 'react'
import styled from 'styled-components'
import { FaUserAlt } from 'react-icons/fa'

const ActionButton = styled.button`

background:linear-gradient(180deg, #e276f8 1%,#4e0bab 87% );
width: 100%;
max-width: 27rem;
color: #FFFFFF;
padding: 0rem 1rem;
max-width: 24rem;
border: none;
outline: none;
border-radius: 1.8em;
cursor: pointer;
align-text: center;
display: inline-block;
align-items: center;
font-family: 'Quicksand',sans-serif ;
font-weight:bolder;
min-height: 2rem ;
margin: 1.75em ;
font-size: 1em;
}
&:hover {
background:linear-gradient(180deg, #d6d6d6 0%,#7c797d 76%,#605c62 100% );
}
&:active {
background:linear-gradient(180deg, #d6d6d6 0%,#7c797d 76%,#605c62 100% );

svg {
position: end;
margin-left: 1rem;
font-size: 20px;

}
`

const PublicActionButton = ({ children }) => {
  return (
    <ActionButton>
      {' '}
      <p>
        {' '}
        {children} <FaUserAlt />{' '}
      </p>{' '}
    </ActionButton>
  )
}

export default PublicActionButton
