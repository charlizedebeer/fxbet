import React from 'react'
//import PropTypes from 'prop-types'
import styled from 'styled-components'
import Logo from '../logo/Logo'
import HeaderButton from './HeaderButton';
import { NavLink } from 'react-router-dom'
import LoginIcon from 'url:./icon-login.png'
import { useAuth } from '../../auth'





const ButtonWrapper = styled.div`
display: inline-flex;
flex-direction: row;
justify-content: flex-end;
margin-right:0.5rem;

img {
  max-width: 1.5rem;
  margin-left: 1rem;
  display: inline-flex;

}

> * {
  text-decoration: none;
}
`
const Wrapper = styled.header`
font-weight:bolder;
  display: inline-grid; 
  grid-template-rows: 1fr;
  grid-template-columns: 25% 60% 15%;
  justify-content: end;
  background-color: none;
  min-width: 100%;
  margin-top: 1rem;
  padding: 0.5rem 1rem;

  
    .Logo { 
      grid-column-start: 1;
      justify-content: end;
      box-sizing:content-box;
    }

    .ctabuttons {   
      grid-column-start: 3;
      align: right;
      box-sizing: content-box;
    }
`


const HeaderTopPublic = () => {
  const { token } = useAuth()
  
  return(

  <Wrapper>

    <NavLink to='/'>
      <Logo className='Logo' />
</NavLink>


     <ButtonWrapper className="ctabuttons">
{!token  && (<NavLink to='/sign-up'>
        <HeaderButton className='Signup'inputColor='#88C32C'> Sign Up</HeaderButton>
       </NavLink>)}
 
 {!token  && (<NavLink  to='/sign-in'>   
        <HeaderButton className='Login'>
           Login <img src={LoginIcon}></img>
  </HeaderButton>    
      </NavLink>)}

      {token  && (<NavLink  to='/sign-out'>   
        <HeaderButton className='Login'>
           Sign Out <img src={LoginIcon}></img>
  </HeaderButton>    
      </NavLink>)}

      </ButtonWrapper>
  </Wrapper>
)}
export default HeaderTopPublic
