import React from "react";
import styled from "styled-components";
import PropTypes from 'prop-types'


const StyledButton = styled.button`
padding: 1rem, 1.5rem;
font-size: 1.8rem;
color: ${props => props.inputColor || "#00CAFB"};
outline: none;
border: none;
background-color: rgba(0,0,0,0);
width: 5em;
height: 20px;
font-family: "Quicksand";
font-weight: bold;
font-size: 16px;
line-height: 1.3;
display: flex;
padding: 1.2em 0em;
align-items: end;
.Signup{
    width: 5rem
}
.login{
    width: 5rem

}



`

const HeaderButton = ({inputColor, children}) => {
    return (
        <StyledButton inputColor={inputColor}>{children}</StyledButton>
        
    );


}

HeaderButton.propTypes = {
    inputColor: PropTypes.any,
    children: PropTypes.any
  }

export default HeaderButton