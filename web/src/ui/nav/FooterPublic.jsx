import React from 'react';
import styled  from 'styled-components';


const FooterMenu = styled.footer`
min-width: 100%;
min-height: 15vh;
font-family: "Quicksand";
font-weight: 400;
text-align: center;
display: flex;
flex-direction: column;
  overflow-y: auto;
  overflow-x: hidden;
  padding: 1rem
`
const FooterNav = styled.div`
margin: 0.75rem 0;
color: #ffffff;
font-size: 0.8rem;
`
const LegalMenu =styled.div`
color: #d6d6d6;
font-size: 0.7rem;
`

const FooterPublic = () => (


<FooterMenu>
  <FooterNav>
   Terms of Use  |  Privacy Policy  |  AML Policy  |  Payment Policy 
    Compliance & Arbitration  |  Responsible Gaming
    </FooterNav>
    <LegalMenu>  Copyright © FXBET, 2021 
    FXBET and its logos are trademarks of FXBET Ltd.</LegalMenu>
  </FooterMenu>

)


export default FooterPublic;
