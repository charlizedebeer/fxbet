import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import bgChart from 'url:./bg-purple-chart-fs8.png' // Compressed with pngquant
import FooterPublic from '../nav/FooterPublic'
import HeaderTopPublic from '../nav/HeaderTopPublic'

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  background: #0960b0;
  background: url(${bgChart}) no-repeat right top,
    linear-gradient(180deg, rgba(2, 0, 36, 1) 0%, rgba(4, 161, 240, 1) 41%, rgba(9, 96, 176, 1) 100%);
  background-size: contain;
`

/** High-level app layout for public (non-logged-in) users */
const LayoutPublic = ({ children }) => (
  <Wrapper>
    <HeaderTopPublic />
    {children}
    <FooterPublic/>
  </Wrapper>
)

LayoutPublic.propTypes = {
  children: PropTypes.any,
}

export default LayoutPublic
