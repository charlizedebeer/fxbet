import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
`

const Content = styled.div`
  max-width: 1600px;
`

/** A content wrapper that renders content "full screen" - as wide as possible (up to a limit),
 * top-down. */
const ContentFullPage = ({ children }) => (
  <Wrapper>
    <Content>{children}</Content>
  </Wrapper>
)
ContentFullPage.propTypes = {
  children: PropTypes.any,
}

export default ContentFullPage
