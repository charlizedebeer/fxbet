import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 75%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  justify-items: center;
  overflow-y: auto;  <FooterPublic/>
  overflow-x: hidden;
  padding: 1rem;
`

/** A content wrapper that floats the content in the middle of the screen
 * and keeps it as small as possible. */
const ContentSmallCentered = ({ children }) => <Wrapper>{children}</Wrapper>
ContentSmallCentered.propTypes = {
  children: PropTypes.any,
}

export default ContentSmallCentered
